//
//  ValidateError.swift
//  MyAppChat
//
//  Created by CPU11814 on 8/30/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

public class ValidateError : Error {
    let message: String
    public init(message: String) {
        self.message = message
    }
}
