//
//  RxBindings.swift
//  MyAppChat
//
//  Created by CPU11814 on 8/30/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

infix operator <->

public func <-> <T>(property: ControlProperty<T>, variable: BehaviorRelay<T>) -> Disposable {
    let bindToUIDisposable = variable.asObservable().bind(to: property)
    
    let bindToVariableDisposable = property.subscribe(onNext: { n in
        variable.accept(n)
    }, onCompleted: {
        bindToUIDisposable.dispose()
    })
    return Disposables.create(bindToUIDisposable, bindToVariableDisposable)
}
