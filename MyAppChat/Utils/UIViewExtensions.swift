//
//  UiViewExtensions.swift
//  MyAppChat
//
//  Created by CPU11814 on 11/7/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import UIKit

extension UIView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat, width: CGFloat, height: CGFloat) {
        let path = UIBezierPath(roundedRect: CGRect(x: 0.0, y: 0.0, width: width, height: height), byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}
