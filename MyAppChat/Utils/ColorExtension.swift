//
//  ColorExtension.swift
//  MyAppChat
//
//  Created by CPU11814 on 8/30/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import UIKit
extension UIColor {
    public class var textFieldBlue: UIColor {
        return UIColor(named: "TextFieldBlue")!
    }
    
    public class var loginUserChat: UIColor {
        return UIColor(named: "LoginUserChat")!
    }
    
    public class var chatBackgroundGrey: UIColor {
        return UIColor(named: "ChatBackgroundGrey")!
    }
    
    public class var progressBackground: UIColor {
        return UIColor(named: "ProgressBackground")!
    }
}
