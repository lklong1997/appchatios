//
//  Converter.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/27/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import Foundation
import UIKit

class Converter {
    public static func calculateLatestMessageTimeString(latestMessageTime: CLong) -> String {
        let currTime = NSDate().timeIntervalSince1970
        let diffInSeconds = CLong(currTime) - latestMessageTime
        
        let days = diffInSeconds/60/60/24
        if days < 1 {
            let hours = diffInSeconds/60/60
            if hours < 1 {
                var minutes = diffInSeconds/60
                if minutes < 1 {
                    minutes = 1
                }
                return ("\(minutes) phút")
            }
            else {
                return ("\(hours) giờ")
            }
        }
        else {
            return ("\(days) ngày")
        }
    }
    
    // Helper function inserted by Swift 4.2 migrator.
    public static func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
        return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
    }
    
    // Helper function inserted by Swift 4.2 migrator.
    public static func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
        return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
    }

    // Helper function inserted by Swift 4.2 migrator.
    public static func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
        return input.rawValue
    }
}
