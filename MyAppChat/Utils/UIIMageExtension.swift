//
//  UIIMageExtension.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/4/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import UIKit

extension UIImage {
    public class var imageUser: UIImage {
        return UIImage(named: "User")!
    }
    
    public class var imagePassword: UIImage {
        return UIImage(named: "Password")!
    }
    
    public class var imageCamera: UIImage {
        return UIImage(named: "Camera")!
    }
    
    public class var imageAvatar: UIImage {
        return UIImage(named: "Avatar")!
    }
    
    public class var imageSend: UIImage {
        return UIImage(named: "Send")!
    }
    
    public class var imageConversation: UIImage {
        return UIImage(named: "Conversation")!
    }
    
    public class var imageSetting: UIImage {
        return UIImage(named: "Setting")!
    }
    
    public class var imageSettingAvatar: UIImage {
        return UIImage(named: "SettingAvatar")!
    }
    
    public class var imageMessageImage: UIImage {
        return UIImage(named: "MessageImage")!
    }
    
    public class var imageGallery: UIImage {
        return UIImage(named: "Gallery")!
    }
    
    public class var imageDownload: UIImage {
        return UIImage(named: "Download")!
    }
    
    public class var imageFile: UIImage {
        return UIImage(named: "File")!
    }
    
    public class var imageLocation: UIImage {
        return UIImage(named: "Location")!
    }
    
    public class var imageEmoji: UIImage {
        return UIImage(named: "Emoji")!
    }
    
    public class var imageDownArrow: UIImage {
        return UIImage(named: "DownArrow")!
    }
    
    public class var imagePlay: UIImage {
        return UIImage(named: "Play")!
    }
    
    public class var imagePause: UIImage {
        return UIImage(named: "Pause")!
    }
    
    public class var imagePlayVideoChat: UIImage {
        return UIImage(named: "PlayVideoChat")!
    }
}
