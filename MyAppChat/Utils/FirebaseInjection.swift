//
//  Firebase.swift
//  MyAppChat
//
//  Created by CPU11814 on 8/31/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import Firebase
import FirebaseDatabase
import FirebaseStorage

class FirebaseInjection {

    // Firebase
    public static let databaseRef = Database.database().reference()
    public static let storageRef = Storage.storage().reference()
    public static let type = "type"
    public static let atTime = "atTime"
    
    // Storage
    public static let storageImage = "Image"
    public static let storageFile = "File"
    public static let storageAudio = "Audio"
    public static let storageVideo = "Video"
    public static let storageLocation = "Location"
    
    // User
    public static let user = "User"
    
    public static let name = "name"
    
    public static let password = "password"
    
    public static let avatarUrl = "avatarURL"
    
    public static let conversationIDList = "conversationIDList"
    
    // Conversation
    public static let conversation = "Conversation"

    public static let conversationName = "conversationName"    
        
    public static let privateType = "private"
    
    public static let groupType = "group"
    
    public static let userIDList = "userIDList"
    
    public static let latestMessage = "latestMessage"
    
    public static let latestMessageType = "latestMessageType"
    
    // Message
    public static let message = "Message"
    
    public static let textType = "text"
    
    public static let imageType = "image"
    
    public static let videoType = "video"
    
    public static let audioType = "audio"
    
    public static let fileType = "file"
    
    public static let locationType = "location"
    
    public static let content = "content"
    
    public static let sender = "sender"
    
    public static let nameOfContent = "nameOfContent"
}
