//
//  UILabelExtension.swift
//  MyAppChat
//
//  Created by CPU11814 on 10/16/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import UIKit
import Foundation

extension UILabel {
    func setHtmlFromString(text: String) {
        let modifiedFont = NSString(format:"<span style=\"font-family: '-apple-system', 'HelveticaNeue'; font-size: \(self.font!.pointSize)\">%@</span>" as NSString, text)
        
        
        let attrStr = try! NSAttributedString(data: modifiedFont.data(using: String.Encoding.unicode.rawValue, allowLossyConversion: true)!, options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
            
        self.attributedText = attrStr
    }
}
