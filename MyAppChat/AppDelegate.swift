//
//  AppDelegate.swift
//  MyAppChat
//
//  Created by CPU11814 on 8/22/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import UIKit
import Firebase
import RealmSwift
import GoogleMaps
import GooglePlaces

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate { 
    
    var window: UIWindow?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        Database.database().reference().keepSynced(false)
        
        // For easy access to the Realm database file when coding!!
        if TARGET_OS_SIMULATOR != 0 {
            Realm.Configuration.defaultConfiguration.fileURL = URL(fileURLWithPath: "/Users/cpu11814/Desktop/RealmDB/ChatApp.realm")
        }
        
        GMSServices.provideAPIKey("AIzaSyCvFqxbtuQ1-k5mQwITNWoGNyn2dJTdwiw")
        GMSPlacesClient.provideAPIKey("AIzaSyCvFqxbtuQ1-k5mQwITNWoGNyn2dJTdwiw")
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
}

