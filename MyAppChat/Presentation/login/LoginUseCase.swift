//
//  LoginUseCase.swift
//  MyAppChat
//
//  Created by CPU11814 on 8/28/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift

class LoginUseCase : UseCase {
    private let repository : UserRepository = UserRepositoryFactory.sharedInstance
    
    public typealias Request = LoginRequest
    public typealias Response = User
    
    public func execute(request: LoginRequest) -> Observable<User> {
        return repository.login(request: request)
    }
}
