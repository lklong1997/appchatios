//
//  ViewController.swift
//  MyAppChat
//
//  Created by CPU11814 on 8/22/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import UIKit
import PureLayout
import RxSwift
import RxCocoa

protocol LoginDisplayLogic : class {
    func hideKeyboard()
    func goToMain(user: User)
    func goToSignUp()
}

class LoginViewController: BaseViewController {
    @IBOutlet private weak var textFieldUsername : UITextField!
    @IBOutlet private weak var textFieldPassword: UITextField!
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var labelNoAccount: UILabel!
    @IBOutlet private weak var buttonSignIn: UIButton!
    @IBOutlet private weak var buttonSignUp: UIButton!
    @IBOutlet private weak var imageViewUser: UIImageView!
    @IBOutlet private weak var imageViewPassword: UIImageView!
    
    private var viewModel : LoginViewModel!
    private let disposeBag = DisposeBag()

    class func newInstance() -> UIViewController {
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        return vc
    }
    

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        viewModel = LoginViewModel(displayLogic: self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        setupUI()
        setupLayoutConstraints()
        bindViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Hide the navigation bar (as we are using a navigation controller to control the navigation from story board)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Unhide the navigation bar
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    private func setupUI() {
        let color = UIColor.textFieldBlue
        
        imageViewUser.image = UIImage.imageUser
        imageViewUser.autoSetDimensions(to: CGSize(width: 32.0, height: 32.0))
        
        imageViewPassword.image = UIImage.imagePassword
        imageViewPassword.autoSetDimensions(to: CGSize(width: 32.0, height: 32.0))
        
        textFieldUsername.layer.borderWidth = 1.0
        textFieldUsername.layer.cornerRadius = 8.0
        textFieldUsername.layer.borderColor = color.cgColor
        textFieldUsername.autoSetDimensions(to: CGSize(width: 256.0, height: 32.0))
        
        textFieldPassword.layer.borderWidth = 1.0
        textFieldPassword.layer.cornerRadius = 8.0
        textFieldPassword.layer.borderColor = color.cgColor
        textFieldPassword.autoSetDimensions(to: CGSize(width: 256.0, height: 32.0))
        
        buttonSignIn.contentEdgeInsets = UIEdgeInsets(top: 8.0, left: 8.0, bottom: 8.0, right: 8.0)
        buttonSignIn.layer.cornerRadius = 4.0
        buttonSignIn.layer.borderColor = UIColor.lightGray.cgColor
        buttonSignIn.layer.borderWidth = 1.0
        buttonSignIn.clipsToBounds = true
        buttonSignIn.backgroundColor = color
        buttonSignIn.setTitleColor(UIColor.white, for: .normal)
        
        buttonSignUp.contentEdgeInsets = UIEdgeInsets(top: 8.0, left: 8.0, bottom: 8.0, right: 8.0)
        buttonSignUp.layer.cornerRadius = 4.0
        buttonSignUp.layer.borderColor = UIColor.lightGray.cgColor
        buttonSignUp.layer.borderWidth = 1.0
        buttonSignUp.clipsToBounds = true
        buttonSignUp.backgroundColor = color
        buttonSignUp.setTitleColor(UIColor.white, for: .normal)
    }
    
    private func setupLayoutConstraints() {
        containerView.autoCenterInSuperview()
        
        textFieldUsername.autoPinEdge(toSuperviewEdge: .right)
        textFieldUsername.autoPinEdge(toSuperviewEdge: .top)
        
        imageViewUser.autoAlignAxis(.horizontal, toSameAxisOf: textFieldUsername)
        imageViewUser.autoPinEdge(toSuperviewEdge: .left)
        imageViewUser.autoPinEdge(.right, to: .left, of: textFieldUsername, withOffset: -8.0)
        
        
        textFieldPassword.autoPinEdge(.top, to: .bottom, of: textFieldUsername, withOffset: 8.0)
        textFieldPassword.autoPinEdge(toSuperviewEdge: .right)
        textFieldPassword.autoPinEdge(toSuperviewEdge: .bottom)

        imageViewPassword.autoAlignAxis(.horizontal, toSameAxisOf: textFieldPassword)
        imageViewPassword.autoPinEdge(toSuperviewEdge: .left)
        imageViewPassword.autoPinEdge(.right, to: .left, of: textFieldPassword, withOffset: -8.0)
        
        buttonSignIn.autoAlignAxis(toSuperviewAxis: .vertical)
        buttonSignIn.autoPinEdge(.top, to: .bottom, of: textFieldPassword, withOffset: 8.0)
        
        labelNoAccount.autoAlignAxis(toSuperviewAxis: .vertical)
        labelNoAccount.autoPinEdge(.top, to: .bottom, of: buttonSignIn, withOffset: 16.0)
        
        buttonSignUp.autoAlignAxis(toSuperviewAxis: .vertical)
        buttonSignUp.autoPinEdge(.top, to: .bottom, of: labelNoAccount, withOffset: 16.0)
    }
    
    private func bindViewModel() {
        let viewWillAppear = self.rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        
        let input = LoginViewModel.Input(trigger: viewWillAppear,
                                         loginTrigger: buttonSignIn.rx.tap.asDriver(),
                                         signupTrigger: buttonSignUp.rx.tap.asDriver(),
                                         username: textFieldUsername.rx.text.orEmpty,
                                         password: textFieldPassword.rx.text.orEmpty)
        
        let output = viewModel.transform(input: input)
        
        output.fetching.drive(onNext: { [unowned self] (show) in
            self.showLoading(withStatus: show)
        }).disposed(by: disposeBag)
        
        output.error.drive(onNext: { [unowned self] (error) in
            self.showError(error: error)
        }).disposed(by: disposeBag)
    }
}

extension LoginViewController: LoginDisplayLogic {
    func hideKeyboard() {
        self.view.resignFirstResponder()
    }
    
    func goToMain(user: User) {
        let vc = MainViewController.newInstance(user: user)
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    func goToSignUp() {
        let vc = SignUpViewController.newInstance()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

