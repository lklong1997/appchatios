//
//  LoginViewModel.swift
//  MyAppChat
//
//  Created by CPU11814 on 8/28/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift
import RxCocoa

final class LoginViewModel : ViewModelDelegate {
    private let username = BehaviorRelay<String>(value: "")
    private let password = BehaviorRelay<String>(value: "")
    private let isValid : Observable<Bool>
    private weak var displayLogic : LoginDisplayLogic?
    private var disposeBag = DisposeBag()
    private let loginUseCase = LoginUseCase()
    private let loadUsersUseCase = LoadUsersUseCase()
    private let loadUserFromLocalUseCase = LoadUserFromLocalUseCase()
    private var loginUser: User!
    
    init(displayLogic: LoginDisplayLogic) {
        self.displayLogic = displayLogic
        
        isValid = Observable.combineLatest(username, password) {
            (username, password) in
            return username.count > 0 && password.count > 0
        }
    }
    
    func transform(input: Input) -> Output {
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        
        (input.username <-> self.username)
            .disposed(by: disposeBag)
        (input.password <-> self.password)
            .disposed(by: disposeBag)
        
        input.trigger
            .flatMap { [unowned self] _ -> Driver<User> in
                return Observable.deferred {
                    return Observable.just(LoadUserFromLocalRequest())
                    }
                    .flatMap { [unowned self] request -> Observable<User> in
                        return self.loadUserFromLocalUseCase
                            .execute(request: request)
                    }
                    .do(onNext: { [unowned self] user in
                        self.displayLogic?.goToMain(user: user)
                    })
                    .asDriverOnErrorJustComplete()
            }
            .drive()
            .disposed(by: disposeBag)
        
        
        input.loginTrigger
            .flatMap { [unowned self] () -> Driver<User>  in
                self.displayLogic?.hideKeyboard()
                return Observable.deferred { [unowned self]  in
                    guard !self.username.value.isEmpty else {
                        return Observable.error(ValidateError(message: "Please enter username"))
                    }
                    guard !self.password.value.isEmpty else {
                        return Observable.error(ValidateError(message: "Please enter password"))
                    }
                    
                    return Observable.just(LoginRequest(username: self.username.value, password: self.password.value))
                    }
                    .flatMap{ [unowned self] (request) -> Observable<User> in
                        return self.loginUseCase
                            .execute(request: request)
                            .do(onNext: { [unowned self] (user) in
                                self.displayLogic?.goToMain(user: user)
                            })
                    }
                    .trackActivity(activityIndicator)
                    .trackError(errorTracker)
                    .asDriverOnErrorJustComplete()
            }
            .drive()
            .disposed(by: disposeBag)
        input.signupTrigger
            .drive(onNext: { [unowned self] in
                self.displayLogic?.goToSignUp()                
            })
            .disposed(by: disposeBag)
        
        return Output(fetching: activityIndicator.asDriver(), error: errorTracker.asDriver())
    }
}

extension LoginViewModel {
    public struct Input {
        let trigger: Driver<Void>
        let loginTrigger: Driver<Void>
        let signupTrigger: Driver<Void>
        let username: ControlProperty<String>
        let password: ControlProperty<String>
    }
    public struct Output {
        let fetching : Driver<Bool>
        let error: Driver<Error>
    }
}
