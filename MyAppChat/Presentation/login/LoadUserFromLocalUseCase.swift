//
//  LoadUserFromLocalUseCase.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/25/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift

class LoadUserFromLocalUseCase: UseCase {
    private let repository: UserRepository = UserRepositoryFactory.sharedInstance
    
    public typealias Request = LoadUserFromLocalRequest
    public typealias Response = User
    
    func execute(request: LoadUserFromLocalRequest) -> Observable<User> {
        return repository.loadUserFromLocal(request: request)
    }
}
