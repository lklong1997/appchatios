//
//  SendMessageUseCase.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/12/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift

class SendMessageUseCase: UseCase {
    private let repository: MessageRepository = MessageRepositoryFactory.sharedInstance
    
    public typealias Request = SendMessageRequest
    public typealias Response = Message
    
    func execute(request: SendMessageRequest) -> Observable<Message> {
        return repository.sendMessage(request: request)
    }
}
