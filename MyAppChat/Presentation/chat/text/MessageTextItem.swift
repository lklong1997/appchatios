//
//  MessageTextItem.swift
//  MyAppChat
//
//  Created by CPU11814 on 11/7/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

class MessageTextItem: MessageItem {
    override init(messageItemType: MessageItemType, id: String!, atTime: String!, type: String!, userContent: String!, userName: String!, avatarUrl: String!, isTimeVisible: Bool) {
        super.init(messageItemType: messageItemType, id: id, atTime: atTime, type: type, userContent: userContent, userName: userName, avatarUrl: avatarUrl, isTimeVisible: isTimeVisible)
    }
    
    init(item: MessageTextItem) {
        super.init(item: item)
    }
}

extension Hashable where Self: MessageTextItem {
    static func == (lhs: Self, rhs: Self) -> Bool {        
        return lhs.id == rhs.id &&
            lhs.atTime == rhs.atTime &&
            lhs.type == rhs.type &&
            lhs.userContent == rhs.userContent &&
            lhs.userName == rhs.userName &&
            lhs.isTimeVisible == rhs.isTimeVisible &&
            lhs.isAvatarVisible == rhs.isAvatarVisible &&
            lhs.isSent == rhs.isSent
    }
}
