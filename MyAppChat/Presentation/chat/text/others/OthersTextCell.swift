//
//  OthersTextCell.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/13/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import UIKit
import PureLayout
import Kingfisher

class OthersTextCell: BaseMessageCell {
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var avatar: UIImageView!
    @IBOutlet private weak var senderLabel: UILabel!
    @IBOutlet private weak var contentLabel: UILabel!
    @IBOutlet private weak var atTimeLabel: UILabel!
    @IBOutlet private weak var stackView: UIStackView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func setupUI() {
        super.setupUI()
        
        self.avatar.autoSetDimensions(to: CGSize(width: 44.0, height: 44.0))
        self.avatar.autoPinEdge(toSuperviewEdge: .left, withInset: 8.0)
        self.avatar.autoPinEdge(toSuperviewEdge: .top, withInset: 8.0)
        self.avatar.layer.cornerRadius = 22.0
        self.avatar.layer.borderColor = UIColor.lightGray.cgColor
        self.avatar.layer.borderWidth = 1.0
        self.avatar.contentMode = .scaleToFill
        self.avatar.clipsToBounds = true
        
        self.containerView.layer.borderColor = UIColor.lightGray.cgColor
        self.containerView.layer.borderWidth = 1.0
        self.containerView.layer.cornerRadius = 8.0
        self.containerView.backgroundColor = UIColor.white
        self.containerView.autoPinEdge(toSuperviewEdge: .top, withInset: 8.0)
        self.containerView.autoPinEdge(.left, to: .right, of: avatar, withOffset: 8.0)
        self.containerView.autoPinEdge(toSuperviewEdge: .bottom, withInset: 8.0)
        self.containerView.autoPinEdge(.right, to: .right, of: containerView.superview!, withOffset: -64.0, relation: .lessThanOrEqual)
        
        self.stackView.alignment = .leading
        self.stackView.axis = .vertical
        self.stackView.spacing = 8.0
        self.stackView.autoPinEdge(toSuperviewEdge: .top, withInset: 8.0)
        self.stackView.autoPinEdge(toSuperviewEdge: .bottom, withInset: 8.0)
        self.stackView.autoPinEdge(toSuperviewEdge: .left, withInset: 8.0)
        self.stackView.autoPinEdge(toSuperviewEdge: .right, withInset: 8.0)
        
        self.senderLabel.numberOfLines = 1
        self.senderLabel.font = UIFont.systemFont(ofSize: 17.0)
        self.senderLabel.textColor = UIColor.purple
        
        self.contentLabel.lineBreakMode = .byWordWrapping
        self.contentLabel.numberOfLines = 0
        self.contentLabel.textAlignment = .left
        self.contentLabel.font = UIFont.systemFont(ofSize: 17.0)
        self.contentLabel.textColor = UIColor.black
        
        self.atTimeLabel.textAlignment = .right
        self.atTimeLabel.font = UIFont.systemFont(ofSize: 11.0)
        self.atTimeLabel.textColor = UIColor.lightGray
    }

    override func bind(item: MessageItem, listener: Listener) {
        super.bind(item: item, listener: listener)

        let textItem = item as! MessageTextItem
        if !textItem.isTimeVisible {
            self.stackView.removeArrangedSubview(atTimeLabel)
            self.atTimeLabel.isHidden = true

        } else {
            self.atTimeLabel.isHidden = false
            self.stackView.addArrangedSubview(atTimeLabel)

            self.atTimeLabel.text = textItem.atTime
        }

        if textItem.isAvatarVisible {
            self.avatar.layer.borderColor = UIColor.lightGray.cgColor
            let processor = BlurImageProcessor(blurRadius: 4) >> RoundCornerImageProcessor(cornerRadius: 20)
            self.avatar.kf.setImage(with: URL(string: textItem.avatarUrl),
                                    placeholder: UIImage.imageAvatar,
                                    options: [.processor(processor)],
                                    progressBlock: nil,
                                    completionHandler: { (image, error, cacheType, imageURL) in

            })
        }
        else {
            self.avatar.layer.borderColor = UIColor.clear.cgColor
            self.avatar.image = nil
        }

        self.senderLabel.text = textItem.userName
        self.contentLabel.text = textItem.userContent
    }
}
