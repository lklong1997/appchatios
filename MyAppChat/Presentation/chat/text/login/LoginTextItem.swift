//
//  LoginTextItem.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/13/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import UIKit

class LoginTextItem: Hashable, ConfigurableItem {
    var hashValue: Int {
        return id.hashValue
    }
    
    static func == (lhs: LoginTextItem, rhs: LoginTextItem) -> Bool {
        return lhs.id == rhs.id &&
            lhs.atTime == rhs.atTime &&
            lhs.type == rhs.type &&
            lhs.userContent == rhs.userContent &&
            lhs.userName == rhs.userName &&
            lhs.isTimeVisible == rhs.isTimeVisible &&
            lhs.isSent == rhs.isSent
    }
    
    func hideTimeLabel() {
        isTimeVisible = false
    }
    
    func showTimeLabel() {
        isTimeVisible = true
    }
    
    func hideAvatar() {
        
    }
    
    func showAvatar() {
        
    }
    
    func setIsSent() {
        isSent = true
    }
    
    func changeNewContent(newContent: String) {
        
    }
    
    let id: String!
    let atTime: String!
    let type: String!
    let userContent: String!
    let userName: String!
    let avatarUrl: String!
    var isTimeVisible: Bool = false
    var isSent: Bool = false
    init(id: String!, atTime: String!, type: String!, userContent: String!, userName: String!, avatarUrl: String!) {
        self.id = id
        self.atTime = atTime
        self.type = type
        self.userName = userName
        self.userContent = userContent
        self.avatarUrl = avatarUrl
    }
    
    init(id: String!, atTime: String!, type: String!, userContent: String!, userName: String!, avatarUrl: String!, isTimeVisible: Bool) {
        self.id = id
        self.atTime = atTime
        self.type = type
        self.userName = userName
        self.userContent = userContent
        self.avatarUrl = avatarUrl
        self.isTimeVisible = isTimeVisible
    }
    
    init(item: LoginTextItem) {
        self.id = item.id
        self.atTime = item.atTime
        self.type = item.type
        self.userName = item.userName
        self.userContent = item.userContent
        self.avatarUrl = item.avatarUrl
        self.isTimeVisible = item.isTimeVisible
        self.isSent = item.isSent
    }
}
