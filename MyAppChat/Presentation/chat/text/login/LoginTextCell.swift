//
//  LoginTextCell.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/7/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import UIKit
import PureLayout

class LoginTextCell: BaseMessageCell {
    @IBOutlet private weak var contentLabel: UILabel!
    @IBOutlet private weak var atTimeLabel: UILabel!
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var stackView: UIStackView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func bind(item: MessageItem, listener: Listener) {
        super.bind(item: item, listener: listener)

        let textItem = item as! MessageTextItem
        if textItem.isSent {
            self.containerView.backgroundColor = UIColor.loginUserChat
        } else {
            self.containerView.backgroundColor = UIColor.red
        }
        
        if !textItem.isTimeVisible {
            self.stackView.removeArrangedSubview(atTimeLabel)
            atTimeLabel.isHidden = true
        }
        else {
            atTimeLabel.isHidden = false
            self.stackView.addArrangedSubview(atTimeLabel)
            
            self.atTimeLabel.text = textItem.atTime
        }
        self.contentLabel.setHtmlFromString(text: textItem.userContent)
    }
    
    override func setupUI() {
        super.setupUI()
        
        self.containerView.layer.borderColor = UIColor.lightGray.cgColor
        self.containerView.layer.borderWidth = 1.0
        self.containerView.layer.cornerRadius = 8.0
        self.containerView.backgroundColor = UIColor.loginUserChat
        self.containerView.autoPinEdge(toSuperviewEdge: .top, withInset: 8.0)
        self.containerView.autoPinEdge(toSuperviewEdge: .right, withInset: 8.0)
        self.containerView.autoPinEdge(toSuperviewEdge: .bottom, withInset: 8.0)
        self.containerView.autoPinEdge(.left, to: .left, of: containerView.superview!, withOffset: 64.0, relation: .greaterThanOrEqual)
        
        self.stackView.alignment = .leading
        self.stackView.axis = .vertical
        self.stackView.spacing = 8.0
        self.stackView.autoPinEdge(toSuperviewEdge: .top, withInset: 8.0)
        self.stackView.autoPinEdge(toSuperviewEdge: .bottom, withInset: 8.0)
        self.stackView.autoPinEdge(toSuperviewEdge: .left, withInset: 8.0)
        self.stackView.autoPinEdge(toSuperviewEdge: .right, withInset: 8.0)
        
        self.contentLabel.lineBreakMode = .byWordWrapping
        self.contentLabel.numberOfLines = 0
        self.contentLabel.textAlignment = .left
        self.contentLabel.font = UIFont.systemFont(ofSize: 17.0)
        self.contentLabel.textColor = UIColor.black
        
        self.atTimeLabel.textAlignment = .right
        self.atTimeLabel.font = UIFont.systemFont(ofSize: 11.0)
        self.atTimeLabel.textColor = UIColor.lightGray
    }
}
