//
//  LoadNewMessageUseCase.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/20/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift

class LoadNewMessageUseCase: UseCase {
    private let repository: MessageRepository = MessageRepositoryFactory.sharedInstance
    
    public typealias Request = LoadNewMessageRequest
    
    public typealias Response = Message
    
    func execute(request: LoadNewMessageRequest) -> Observable<Message> {
        return repository.loadNewMessage(request: request)
    }
}
