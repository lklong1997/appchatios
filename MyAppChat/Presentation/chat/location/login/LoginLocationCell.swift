//
//  LoginLocationCell.swift
//  MyAppChat
//
//  Created by CPU11814 on 11/6/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import Kingfisher
import UIKit
import PureLayout

class LoginLocationCell: BaseMessageCell {
    @IBOutlet private weak var containerView: UIView!
    
    @IBOutlet private weak var timeLabelContainer: UIView!
    @IBOutlet private weak var atTimeLabel: UILabel!
    
    @IBOutlet private weak var stackView: UIStackView!
    @IBOutlet private weak var locationContainer: UIView!
    @IBOutlet private weak var locationSnapshot: UIImageView!
    @IBOutlet private weak var locationNameContainer: UIView!
    @IBOutlet private weak var locationName: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func setupUI() {
        super.setupUI()
        
        self.containerView.autoPinEdge(toSuperviewEdge: .top, withInset: 8.0)
        self.containerView.autoPinEdge(toSuperviewEdge: .right, withInset: 8.0)
        self.containerView.autoPinEdge(toSuperviewEdge: .bottom, withInset: 8.0)
        self.containerView.autoPinEdge(.left, to: .left, of: containerView.superview!, withOffset: 64.0, relation: .greaterThanOrEqual)
        
        self.stackView.backgroundColor = UIColor.clear
        self.stackView.alignment = .leading
        self.stackView.axis = .vertical
        self.stackView.spacing = 8.0
        self.stackView.autoPinEdge(toSuperviewEdge: .top, withInset: 5.0)
        self.stackView.autoPinEdge(toSuperviewEdge: .bottom, withInset: 5.0)
        self.stackView.autoPinEdge(toSuperviewEdge: .right, withInset: 0.0)
        
        self.locationContainer.backgroundColor = UIColor.clear
        
        self.locationSnapshot.autoSetDimensions(to: CGSize(width: 200.0, height: 150.0))
        self.locationSnapshot.autoPinEdge(toSuperviewEdge: .left)
        self.locationSnapshot.autoPinEdge(toSuperviewEdge: .top)
        self.locationSnapshot.autoPinEdge(toSuperviewEdge: .right)
        self.locationSnapshot.roundCorners(corners: UIRectCorner(arrayLiteral: .topLeft, .topRight), radius: 16.0, width: 200.0, height: 150.0)
        self.locationSnapshot.layer.borderWidth = 0.0
        self.locationSnapshot.clipsToBounds = true
        self.locationSnapshot.contentMode = .scaleAspectFill
        
        self.locationNameContainer.autoPinEdge(.top, to: .bottom, of: self.locationSnapshot)
        self.locationNameContainer.autoPinEdge(toSuperviewEdge: .right)
        self.locationNameContainer.autoPinEdge(toSuperviewEdge: .left)
        self.locationNameContainer.autoPinEdge(toSuperviewEdge: .bottom)
        
        self.locationName.font = UIFont.systemFont(ofSize: 11.0)
        self.locationName.numberOfLines = 1
        self.locationName.textAlignment = .left
        self.locationName.lineBreakMode = .byWordWrapping
        self.locationName.autoPinEdge(toSuperviewEdge: .top, withInset: 5.0)
        self.locationName.autoPinEdge(toSuperviewEdge: .left, withInset: 5.0)
        self.locationName.autoPinEdge(toSuperviewEdge: .right, withInset: 5.0)
        self.locationName.autoPinEdge(toSuperviewEdge: .bottom, withInset: 5.0)

        self.timeLabelContainer.backgroundColor = UIColor.loginUserChat
        self.timeLabelContainer.layer.cornerRadius = 8.0
        self.timeLabelContainer.layer.borderColor = UIColor.lightGray.cgColor
        self.timeLabelContainer.layer.borderWidth = 1.0
        self.timeLabelContainer.clipsToBounds = true
        
        self.atTimeLabel.textAlignment = .right
        self.atTimeLabel.font = UIFont.systemFont(ofSize: 11.0)
        self.atTimeLabel.textColor = UIColor.lightGray
        self.atTimeLabel.autoPinEdge(toSuperviewEdge: .top, withInset: 5.0)
        self.atTimeLabel.autoPinEdge(toSuperviewEdge: .bottom, withInset: 5.0)
        self.atTimeLabel.autoPinEdge(toSuperviewEdge: .left, withInset: 5.0)
        self.atTimeLabel.autoPinEdge(toSuperviewEdge: .right, withInset: 5.0)
    }
    
    override func bind(item: MessageItem, listener: Listener) {
        super.bind(item: item, listener: listener)
        
        let locationItem = item as! MessageLocationItem
        let address = locationItem.address
        if !address.isEmpty {
            self.locationName.text = locationItem.address
        } else {
            self.locationName.text = locationItem.latitude + " " + locationItem.longitude
        }
        
        if locationItem.isSent {
            self.timeLabelContainer.backgroundColor = UIColor.loginUserChat
        } else {
            self.timeLabelContainer.backgroundColor = UIColor.red
        }
        
        if !locationItem.isTimeVisible {
            self.stackView.removeArrangedSubview(timeLabelContainer)
            timeLabelContainer.isHidden = true
        } else {
            timeLabelContainer.isHidden = false
            self.stackView.addArrangedSubview(timeLabelContainer)
            
            atTimeLabel.text = locationItem.atTime
        }
        self.locationSnapshot.kf.setImage(with: URL(string: locationItem.userContent),
                                      placeholder: UIImage.imageMessageImage,
                                      options: nil,
                                      progressBlock: nil,
                                      completionHandler: nil)
    }
}
