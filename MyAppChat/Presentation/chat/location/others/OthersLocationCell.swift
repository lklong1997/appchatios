//
//  OthersLocationCell.swift
//  MyAppChat
//
//  Created by CPU11814 on 11/7/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import Kingfisher
import UIKit
import PureLayout

class OthersLocationCell: BaseMessageCell {
    @IBOutlet private weak var containerView: UIView!
    
    @IBOutlet private weak var avatar: UIImageView!

    @IBOutlet private weak var stackView: UIStackView!
    
    @IBOutlet private weak var senderLabel: UILabel!
    @IBOutlet private weak var senderLabelContainer: UIView!

    @IBOutlet private weak var locationContainer: UIView!
    @IBOutlet private weak var locationSnapshot: UIImageView!
    @IBOutlet private weak var locationNameContainer: UIView!
    @IBOutlet private weak var locationName: UILabel!
    
    @IBOutlet private weak var timeLabelContainer: UIView!
    @IBOutlet private weak var atTimeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func setupUI() {
        super.setupUI()
        
        self.avatar.autoSetDimensions(to: CGSize(width: 44.0, height: 44.0))
        self.avatar.autoPinEdge(toSuperviewEdge: .left, withInset: 8.0)
        self.avatar.autoPinEdge(toSuperviewEdge: .top, withInset: 8.0)
        self.avatar.layer.cornerRadius = 22.0
        self.avatar.layer.borderColor = UIColor.lightGray.cgColor
        self.avatar.layer.borderWidth = 1.0
        self.avatar.contentMode = .scaleToFill
        self.avatar.clipsToBounds = true
        
        self.containerView.backgroundColor = UIColor.clear
        self.containerView.autoPinEdge(toSuperviewEdge: .top, withInset: 8.0)
        self.containerView.autoPinEdge(.left, to: .right, of: avatar, withOffset: 8.0)
        self.containerView.autoPinEdge(toSuperviewEdge: .bottom, withInset: 8.0)
        self.containerView.autoPinEdge(.right, to: .right, of: containerView.superview!, withOffset: -64.0, relation: .lessThanOrEqual)
        
        self.stackView.alignment = .leading
        self.stackView.axis = .vertical
        self.stackView.spacing = 8.0
        self.stackView.autoPinEdge(toSuperviewEdge: .top, withInset: 5.0)
        self.stackView.autoPinEdge(toSuperviewEdge: .bottom, withInset: 5.0)
        self.stackView.autoPinEdge(toSuperviewEdge: .left, withInset: 0.0)
        self.stackView.autoPinEdge(toSuperviewEdge: .right, withInset: 0.0)
        
        self.senderLabelContainer.layer.cornerRadius = 8.0
        self.senderLabelContainer.backgroundColor = UIColor.white
        self.senderLabelContainer.clipsToBounds = true
        
        self.senderLabel.autoPinEdge(toSuperviewEdge: .top, withInset: 5.0)
        self.senderLabel.autoPinEdge(toSuperviewEdge: .bottom, withInset: 5.0)
        self.senderLabel.autoPinEdge(toSuperviewEdge: .left, withInset: 8.0)
        self.senderLabel.autoPinEdge(toSuperviewEdge: .right, withInset: 8.0)
        self.senderLabel.numberOfLines = 1
        self.senderLabel.font = UIFont.systemFont(ofSize: 17.0)
        self.senderLabel.textColor = UIColor.purple
        
        self.locationContainer.backgroundColor = UIColor.clear
        
        self.locationSnapshot.autoSetDimensions(to: CGSize(width: 200.0, height: 150.0))
        self.locationSnapshot.autoPinEdge(toSuperviewEdge: .left)
        self.locationSnapshot.autoPinEdge(toSuperviewEdge: .top)
        self.locationSnapshot.autoPinEdge(toSuperviewEdge: .right)
        self.locationSnapshot.roundCorners(corners: UIRectCorner(arrayLiteral: .topLeft, .topRight), radius: 16.0, width: 200.0, height: 150.0)
        self.locationSnapshot.layer.borderWidth = 0.0
        self.locationSnapshot.clipsToBounds = true
        self.locationSnapshot.contentMode = .scaleAspectFill
        
        self.locationNameContainer.backgroundColor = UIColor.white
        self.locationNameContainer.autoPinEdge(.top, to: .bottom, of: self.locationSnapshot)
        self.locationNameContainer.autoPinEdge(toSuperviewEdge: .right)
        self.locationNameContainer.autoPinEdge(toSuperviewEdge: .left)
        self.locationNameContainer.autoPinEdge(toSuperviewEdge: .bottom)
        
        self.locationName.font = UIFont.systemFont(ofSize: 11.0)
        self.locationName.numberOfLines = 1
        self.locationName.textAlignment = .left
        self.locationName.lineBreakMode = .byWordWrapping
        self.locationName.autoPinEdge(toSuperviewEdge: .top, withInset: 5.0)
        self.locationName.autoPinEdge(toSuperviewEdge: .left, withInset: 5.0)
        self.locationName.autoPinEdge(toSuperviewEdge: .right, withInset: 5.0)
        self.locationName.autoPinEdge(toSuperviewEdge: .bottom, withInset: 5.0)
        
        self.timeLabelContainer.layer.cornerRadius = 8.0
        self.timeLabelContainer.backgroundColor = UIColor.lightGray
        self.timeLabelContainer.clipsToBounds = true
        
        self.atTimeLabel.textAlignment = .right
        self.atTimeLabel.font = UIFont.systemFont(ofSize: 11.0)
        self.atTimeLabel.textColor = UIColor.white
        self.atTimeLabel.autoPinEdge(toSuperviewEdge: .top, withInset: 5.0)
        self.atTimeLabel.autoPinEdge(toSuperviewEdge: .bottom, withInset: 5.0)
        self.atTimeLabel.autoPinEdge(toSuperviewEdge: .left, withInset: 5.0)
        self.atTimeLabel.autoPinEdge(toSuperviewEdge: .right, withInset: 5.0)
    }
    
    override func bind(item: MessageItem, listener: Listener) {
        super.bind(item: item, listener: listener)
        
        let locationItem = item as! MessageLocationItem
        
        if locationItem.isAvatarVisible {
            self.avatar.layer.borderColor = UIColor.lightGray.cgColor
            self.avatar.kf.setImage(with: URL(string: locationItem.avatarUrl),
                                    placeholder: UIImage.imageAvatar,
                                    options: nil,
                                    progressBlock: nil,
                                    completionHandler: { (image, error, cacheType, imageURL) in
                                        
            })
        }
        else {
            self.avatar.layer.borderColor = UIColor.clear.cgColor
            self.avatar.image = nil
        }
        
        self.senderLabel.text = item.userName
        
        self.locationSnapshot.kf.setImage(with: URL(string: locationItem.userContent),
                                          placeholder: UIImage.imageMessageImage,
                                          options: nil,
                                          progressBlock: nil,
                                          completionHandler: nil)
        
        let address = locationItem.address
        if !address.isEmpty {
            self.locationName.text = locationItem.address
        } else {
            self.locationName.text = locationItem.latitude + " " + locationItem.longitude
        }
        
        if !locationItem.isTimeVisible {
            self.stackView.removeArrangedSubview(timeLabelContainer)
            timeLabelContainer.isHidden = true
        } else {
            timeLabelContainer.isHidden = false
            self.stackView.addArrangedSubview(timeLabelContainer)
            
            atTimeLabel.text = locationItem.atTime
        }
    }
}
