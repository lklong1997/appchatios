//
//  MessageAudioItem.swift
//  MyAppChat
//
//  Created by CPU11814 on 11/8/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

class MessageAudioItem: MessageItem {
    var duration: String!
    
    init(messageItemType: MessageItemType, id: String!, atTime: String!, type: String!, userContent: String!, userName: String!, avatarUrl: String!, isTimeVisible: Bool, duration: String) {
        super.init(messageItemType: messageItemType, id: id, atTime: atTime, type: type, userContent: userContent, userName: userName, avatarUrl: avatarUrl, isTimeVisible: isTimeVisible)
        
        self.duration = duration
    }
    
    init(item: MessageAudioItem) {
        super.init(item: item)
        
        self.duration = item.duration
    }
}

extension Hashable where Self: MessageAudioItem {
    static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.id == rhs.id &&
            lhs.atTime == rhs.atTime &&
            lhs.type == rhs.type &&
            lhs.userContent == rhs.userContent &&
            lhs.userName == rhs.userName &&
            lhs.isTimeVisible == rhs.isTimeVisible &&
            lhs.isAvatarVisible == rhs.isAvatarVisible &&
            lhs.isSent == rhs.isSent &&
            lhs.duration == rhs.duration
    }
}
