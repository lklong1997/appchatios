//
//  MessageLocationItem.swift
//  MyAppChat
//
//  Created by CPU11814 on 11/7/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

//
//  MessageFileItem.swift
//  MyAppChat
//
//  Created by CPU11814 on 11/7/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import UIKit
import MapKit

class MessageLocationItem: MessageItem {
    var address: String = ""
    var latitude: String!
    var longitude: String!
    
    init(messageItemType: MessageItemType, id: String!, atTime: String!, type: String!, userContent: String!, userName: String!, avatarUrl: String!, isTimeVisible: Bool, address: String, latitude: String!, longitude: String!) {
        super.init(messageItemType: messageItemType, id: id, atTime: atTime, type: type, userContent: userContent, userName: userName, avatarUrl: avatarUrl, isTimeVisible: isTimeVisible)
        
        self.address = address
        self.latitude = latitude
        self.longitude = longitude
    }
    
    init(item: MessageLocationItem) {
        super.init(item: item)
        
        self.address = item.address
        self.latitude = item.latitude
        self.longitude = item.longitude
    }
}

extension Hashable where Self: MessageLocationItem {
    static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.id == rhs.id &&
            lhs.atTime == rhs.atTime &&
            lhs.type == rhs.type &&
            lhs.userContent == rhs.userContent &&
            lhs.userName == rhs.userName &&
            lhs.isTimeVisible == rhs.isTimeVisible &&
            lhs.isAvatarVisible == rhs.isAvatarVisible &&
            lhs.isSent == rhs.isSent &&
            lhs.address == rhs.address &&
            lhs.latitude == rhs.latitude &&
            lhs.longitude == rhs.longitude
    }
}
