//
//  SendLocationUseCase.swift
//  MyAppChat
//
//  Created by CPU11814 on 11/5/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift

class SendLocationUseCase: UseCase {
    private let repository: MessageRepository = MessageRepositoryFactory.sharedInstance
    
    public typealias Request = SendLocationRequest
    public typealias Response = Message
    
    public func execute(request: SendLocationRequest) -> Observable<Message> {
        return repository.sendMessageLocation(request: request)
    }
}
