//
//  GetUnSentMessagesUseCase.swift
//  MyAppChat
//
//  Created by CPU11814 on 10/9/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift

class GetUnSentMessagesUseCase: UseCase {
    private let repository: MessageRepository = MessageRepositoryFactory.sharedInstance
    
    public typealias Request = GetUnSentMessagesRequest
    public typealias Response = [Message]
    
    func execute(request: GetUnSentMessagesUseCase.Request) -> Observable<[Message]> {
        return repository.getUnSentMessages(request: request)
    }
}
