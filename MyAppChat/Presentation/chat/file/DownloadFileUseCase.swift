//
//  DownloadFileUseCase.swift
//  MyAppChat
//
//  Created by CPU11814 on 11/2/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import Foundation
import RxSwift

class DownloadFileUseCase: UseCase {
    private let repository: MessageRepository = MessageRepositoryFactory.sharedInstance
    
    public typealias Request = DownloadFileRequest
    public typealias Response = FileDownloadWrapper
    
    func execute(request: DownloadFileRequest) -> Observable<FileDownloadWrapper> {
        return self.repository.downloadFile(request: request)
    }
}
