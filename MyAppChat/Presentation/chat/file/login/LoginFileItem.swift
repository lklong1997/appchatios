//
//  LoginFileItem.swift
//  MyAppChat
//
//  Created by CPU11814 on 10/17/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import Foundation


class LoginFileItem: Hashable, ConfigurableItem {
    var hashValue: Int {
        return id.hashValue
    }
    
    static func == (lhs: LoginFileItem, rhs: LoginFileItem) -> Bool {
        return lhs.id == rhs.id &&
            lhs.atTime == rhs.atTime &&
            lhs.isTimeVisible == rhs.isTimeVisible &&
            lhs.isSent == rhs.isSent &&
            lhs.userName == rhs.userName &&
            lhs.userContent == rhs.userContent &&
            lhs.nameOfFile == rhs.nameOfFile &&
            lhs.type == rhs.type 
    }
    
    func hideTimeLabel() {
        isTimeVisible = false
    }
    
    func showTimeLabel() {
        isTimeVisible = true
    }
    
    func hideAvatar() {
        
    }
    
    func showAvatar() {
        
    }
    
    func setIsSent() {
        isSent = true
    }
    
    func changeNewContent(newContent: String) {
        self.userContent = newContent
    }
    
    let id: String!
    let atTime: String!
    let type: String!
    var userContent: String!
    let userName: String!
    let avatarUrl: String!
    let nameOfFile: String
    var isTimeVisible: Bool = false
    var isSent: Bool = false
    init(id: String!, atTime: String!, type: String!, userContent: String!, userName: String!, avatarUrl: String!, nameOfFile: String!) {
        self.id = id
        self.atTime = atTime
        self.type = type
        self.userName = userName
        self.userContent = userContent
        self.avatarUrl = avatarUrl
        self.nameOfFile = nameOfFile
    }
    
    init(id: String!, atTime: String!, type: String!, userContent: String!, userName: String!, avatarUrl: String!, nameOfFile: String!, isTimeVisible: Bool) {
        self.id = id
        self.atTime = atTime
        self.type = type
        self.userName = userName
        self.userContent = userContent
        self.avatarUrl = avatarUrl
        self.nameOfFile = nameOfFile
        self.isTimeVisible = isTimeVisible
    }
    
    init(item: LoginFileItem) {
        self.id = item.id
        self.atTime = item.atTime
        self.type = item.type
        self.userName = item.userName
        self.userContent = item.userContent
        self.avatarUrl = item.avatarUrl
        self.nameOfFile = item.nameOfFile
        self.isTimeVisible = item.isTimeVisible
        self.isSent = item.isSent
    }

    
}
