//
//  FileDownloadWrapper.swift
//  MyAppChat
//
//  Created by CPU11814 on 11/5/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import FirebaseStorage

// Wraps anything necessary for the process of downloading a file 
struct FileDownloadWrapper {
    let downloadTask: StorageDownloadTask
    let filePath: URL
}
