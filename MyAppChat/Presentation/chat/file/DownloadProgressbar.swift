//
//  DownloadProgressbar.swift
//  MyAppChat
//
//  Created by CPU11814 on 11/5/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import UIKit

class DownloadProgressbar: UIView {
    private var progressLayer = CAShapeLayer() // progress
    private var trackLayer = CAShapeLayer() // background 
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        createCircularPath()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        createCircularPath()
    }
    
    var progress: CGFloat = 0 {
        willSet(newValue) {
            progressLayer.strokeEnd = newValue
        }
    }
    
    var progressColor = UIColor.textFieldBlue {
        didSet {
            progressLayer.strokeColor = progressColor.cgColor
        }
    }
    
    var trackColor = UIColor.progressBackground {
        didSet {
            trackLayer.strokeColor = trackColor.cgColor
        }
    }
    
    private func createCircularPath() {
        self.backgroundColor = UIColor.clear
        self.layer.cornerRadius = self.frame.size.width / 2
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: frame.size.width/2, y: frame.size.height/2), radius: (frame.size.width - 1.5)/2, startAngle: CGFloat(-0.5 * .pi), endAngle: CGFloat(1.5 * .pi), clockwise: true)
        circlePath.close()
        
        // Background
        trackLayer.path = circlePath.cgPath
        trackLayer.fillColor = UIColor.clear.cgColor
        trackLayer.strokeColor = trackColor.cgColor
        trackLayer.lineWidth = 4.0
        trackLayer.strokeEnd = 1.0
        layer.addSublayer(trackLayer)
        
        progressLayer.path = circlePath.cgPath
        progressLayer.fillColor = UIColor.clear.cgColor
        progressLayer.strokeColor = progressColor.cgColor
        progressLayer.lineWidth = 4.0
        progressLayer.strokeEnd = progress
        layer.addSublayer(progressLayer)
    }
}
