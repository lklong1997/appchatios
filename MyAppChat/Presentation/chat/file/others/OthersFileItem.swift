//
//  OthersFileItem.swift
//  MyAppChat
//
//  Created by CPU11814 on 10/17/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

class OthersFileItem: Hashable, ConfigurableItem {
    var hashValue: Int {
        return id.hashValue
    }
    
    static func == (lhs: OthersFileItem, rhs: OthersFileItem) -> Bool {
        return lhs.id == rhs.id &&
            lhs.atTime == rhs.atTime &&
            lhs.type == rhs.type &&
            lhs.userContent == rhs.userContent &&
            lhs.userName == rhs.userName &&
            lhs.avatarUrl == rhs.avatarUrl &&
            lhs.nameOfFile == rhs.nameOfFile &&
            lhs.isTimeVisible == rhs.isTimeVisible &&
            lhs.isAvatarVisible == rhs.isAvatarVisible
    }
    
    func hideTimeLabel() {
        isTimeVisible = false
    }
    
    func showTimeLabel() {
        isTimeVisible = true
    }
    
    func hideAvatar() {
        isAvatarVisible = false
    }
    
    func showAvatar() {
        isAvatarVisible = true
    }
    
    func setIsSent() {
    }
    
    func changeNewContent(newContent: String) {
        self.userContent = newContent
    }
    
    let id: String!
    let atTime: String!
    let type: String!
    var userContent: String!
    let userName: String!
    let avatarUrl: String!
    let nameOfFile: String!
    var isTimeVisible = false
    var isAvatarVisible = true
    
    init(id: String!, atTime: String!, type: String!, userContent: String!, userName: String!, avatarUrl: String!, nameOfFile: String!) {
        self.id = id
        self.atTime = atTime
        self.type = type
        self.userName = userName
        self.userContent = userContent
        self.avatarUrl = avatarUrl
        self.nameOfFile = nameOfFile
    }
    
    init(id: String!, atTime: String!, type: String!, userContent: String!, userName: String!, avatarUrl: String!, nameOfFile: String!, isTimeVisible: Bool) {
        self.id = id
        self.atTime = atTime
        self.type = type
        self.userName = userName
        self.userContent = userContent
        self.avatarUrl = avatarUrl
        self.nameOfFile = nameOfFile
        self.isTimeVisible = isTimeVisible
    }
    
    init(item: OthersFileItem) {
        self.id = item.id
        self.atTime = item.atTime
        self.type = item.type
        self.userName = item.userName
        self.userContent = item.userContent
        self.avatarUrl = item.avatarUrl
        self.nameOfFile = item.nameOfFile
        self.isTimeVisible = item.isTimeVisible
    }
}
