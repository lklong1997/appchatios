//
//  OthersFileCell.swift
//  MyAppChat
//
//  Created by CPU11814 on 10/17/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import UIKit

class OthersFileCell: BaseMessageCell {
    @IBOutlet private weak var avatar: UIImageView!
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var stackView: UIStackView!
    
    @IBOutlet private weak var senderContainer: UIView!
    @IBOutlet private weak var senderLabel: UILabel!
    
    @IBOutlet private weak var fileContainer: UIView!
    @IBOutlet private weak var fileNameContainer: UIView!
    @IBOutlet private weak var fileNameLabel: UILabel!
    @IBOutlet private weak var downloadIcon: UIView!
    
    @IBOutlet private weak var timeLabelContainer: UIView!
    @IBOutlet private weak var timeLabel: UILabel!
    
    private var progressBar: DownloadProgressbar!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setupUI() {
        super.setupUI()
        
        self.avatar.autoSetDimensions(to: CGSize(width: 44.0, height: 44.0))
        self.avatar.autoPinEdge(toSuperviewEdge: .left, withInset: 8.0)
        self.avatar.autoPinEdge(toSuperviewEdge: .top, withInset: 8.0)
        self.avatar.layer.cornerRadius = 22.0
        self.avatar.layer.borderColor = UIColor.lightGray.cgColor
        self.avatar.layer.borderWidth = 1.0
        self.avatar.contentMode = .scaleToFill
        self.avatar.clipsToBounds = true
        
        self.containerView.backgroundColor = UIColor.clear
        self.containerView.autoPinEdge(toSuperviewEdge: .top, withInset: 8.0)
        self.containerView.autoPinEdge(.left, to: .right, of: self.avatar, withOffset: 8.0)
        self.containerView.autoPinEdge(toSuperviewEdge: .bottom, withInset: 8.0)
        self.containerView.autoPinEdge(toSuperviewEdge: .right, withInset: 64.0, relation: .greaterThanOrEqual)
        
        self.stackView.alignment = .leading
        self.stackView.axis = .vertical
        self.stackView.spacing = 8.0
        self.stackView.autoPinEdge(toSuperviewEdge: .top, withInset: 5.0)
        self.stackView.autoPinEdge(toSuperviewEdge: .bottom, withInset: 5.0)
        self.stackView.autoPinEdge(toSuperviewEdge: .left, withInset: 5.0)
        self.stackView.autoPinEdge(toSuperviewEdge: .right, withInset: 0.0)
        
        self.fileContainer.backgroundColor = UIColor.clear
        
        self.senderContainer.backgroundColor = UIColor.white
        self.senderContainer.layer.cornerRadius = 8.0
        self.senderContainer.clipsToBounds = true
        
        self.senderLabel.font = UIFont.systemFont(ofSize: 17.0)
        self.senderLabel.numberOfLines = 1
        self.senderLabel.lineBreakMode = .byCharWrapping
        self.senderLabel.autoPinEdge(toSuperviewEdge: .top, withInset: 5.0)
        self.senderLabel.autoPinEdge(toSuperviewEdge: .left, withInset: 5.0)
        self.senderLabel.autoPinEdge(toSuperviewEdge: .right, withInset: 5.0)
        self.senderLabel.autoPinEdge(toSuperviewEdge: .bottom, withInset: 5.0)
        
        self.downloadIcon.autoSetDimensions(to: CGSize(width: 44.0, height: 44.0))
        self.downloadIcon.autoPinEdge(toSuperviewEdge: .top, withInset: 0.0)
        self.downloadIcon.autoPinEdge(toSuperviewEdge: .right, withInset: 0.0)
        self.downloadIcon.autoPinEdge(toSuperviewEdge: .bottom, withInset: 0.0)
        self.downloadIcon.backgroundColor = UIColor.clear
        
        let image = UIImageView(frame: CGRect(x: 6.0, y: 6.0, width: 32.0, height: 32.0))
        image.image = UIImage.imageDownArrow
        image.contentMode = .scaleAspectFill
        downloadIcon.addSubview(image)
        
        progressBar = DownloadProgressbar(frame: CGRect(x: 6.0, y: 6.0, width: 32.0, height: 32.0))
        downloadIcon.addSubview(progressBar)
        
        self.fileNameContainer.isUserInteractionEnabled = true
        self.fileNameContainer.autoPinEdge(toSuperviewEdge: .left, withInset: 0.0)
        self.fileNameContainer.autoPinEdge(.right, to: .left, of: downloadIcon, withOffset: -5.0)
        self.fileNameContainer.autoAlignAxis(.horizontal, toSameAxisOf: downloadIcon)
        self.fileNameContainer.layer.cornerRadius = 16.0
        self.fileNameContainer.clipsToBounds = true
        
        self.fileNameLabel.font = UIFont.systemFont(ofSize: 17.0)
        self.fileNameLabel.textColor = UIColor.textFieldBlue
        self.fileNameLabel.textAlignment = .left
        self.fileNameLabel.lineBreakMode = .byTruncatingTail
        self.fileNameLabel.numberOfLines = 1
        self.fileNameLabel.autoPinEdge(toSuperviewEdge: .top, withInset: 8.0)
        self.fileNameLabel.autoPinEdge(toSuperviewEdge: .left, withInset: 8.0)
        self.fileNameLabel.autoPinEdge(toSuperviewEdge: .bottom, withInset: 8.0)
        self.fileNameLabel.autoPinEdge(toSuperviewEdge: .right, withInset: 8.0)
        
        self.timeLabelContainer.backgroundColor = UIColor.white
        self.timeLabelContainer.layer.cornerRadius = 8.0
        self.timeLabelContainer.layer.borderColor = UIColor.lightGray.cgColor
        self.timeLabelContainer.layer.borderWidth = 1.0
        self.timeLabelContainer.clipsToBounds = true
        
        self.timeLabel.textAlignment = .right
        self.timeLabel.font = UIFont.systemFont(ofSize: 11.0)
        self.timeLabel.textColor = UIColor.lightGray
        self.timeLabel.autoPinEdge(toSuperviewEdge: .top, withInset: 5.0)
        self.timeLabel.autoPinEdge(toSuperviewEdge: .bottom, withInset: 5.0)
        self.timeLabel.autoPinEdge(toSuperviewEdge: .left, withInset: 5.0)
        self.timeLabel.autoPinEdge(toSuperviewEdge: .right, withInset: 5.0)
        
        self.fileNameContainer.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onItemClick)))
    }
    
    @objc func onItemClick() {
        super.onClick()        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override func bind(item: MessageItem, listener: Listener) {
        super.bind(item: item, listener: listener)

        let fileItem = item as! MessageFileItem
        updateProgressBar()
        
        self.avatar.kf.setImage(with: URL(string: fileItem.avatarUrl),
                                placeholder: UIImage.imageAvatar,
                                options: nil,
                                progressBlock: nil,
                                completionHandler: nil)
        
        self.senderLabel.text = fileItem.userName
        
        let underlineAttribute = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue]
        let attributedString = NSAttributedString(string: fileItem.nameOfFile, attributes: underlineAttribute)
        self.fileNameLabel.attributedText = attributedString
        if fileItem.isTimeVisible {
            timeLabelContainer.isHidden = false
            self.stackView.addArrangedSubview(timeLabelContainer)
            
            self.timeLabel.text = fileItem.atTime
        } else {
            self.stackView.removeArrangedSubview(timeLabelContainer)
            timeLabelContainer.isHidden = true
        }
    }
    
    public func updateProgressBar() {
        self.progressBar.progress = (item as! MessageFileItem).progressPercent
    }

}
