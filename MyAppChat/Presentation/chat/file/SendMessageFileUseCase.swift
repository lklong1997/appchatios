//
//  SendMessageFileUseCase.swift
//  MyAppChat
//
//  Created by CPU11814 on 10/17/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift

class SendMessageFileUseCase: UseCase {
    private let repository: MessageRepository = MessageRepositoryFactory.sharedInstance
    
    public typealias Request = SendMessageFileRequest
    public typealias Response = Message
    
    func execute(request: SendMessageFileRequest) -> Observable<Message> {
        return repository.sendMessageFile(request: request)
    }
}
