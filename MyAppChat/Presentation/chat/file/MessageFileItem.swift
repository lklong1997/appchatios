//
//  MessageFileItem.swift
//  MyAppChat
//
//  Created by CPU11814 on 11/7/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import UIKit
class MessageFileItem: MessageItem {
    var nameOfFile: String = ""
    var progressPercent: CGFloat = 0.0
    init(messageItemType: MessageItemType, id: String!, atTime: String!, type: String!, userContent: String!, userName: String!, avatarUrl: String!, isTimeVisible: Bool, nameOfFile: String) {
        super.init(messageItemType: messageItemType, id: id, atTime: atTime, type: type, userContent: userContent, userName: userName, avatarUrl: avatarUrl, isTimeVisible: isTimeVisible)
        
        self.nameOfFile = nameOfFile
    }
    
    init(item: MessageFileItem) {
        super.init(item: item)
        
        self.nameOfFile = item.nameOfFile
        self.progressPercent = item.progressPercent
    }
}

extension Hashable where Self: MessageFileItem {
    static func == (lhs: Self, rhs: Self) -> Bool {        
        return lhs.id == rhs.id &&
            lhs.atTime == rhs.atTime &&
            lhs.type == rhs.type &&
            lhs.userContent == rhs.userContent &&
            lhs.userName == rhs.userName &&
            lhs.isTimeVisible == rhs.isTimeVisible &&
            lhs.isAvatarVisible == rhs.isAvatarVisible &&
            lhs.isSent == rhs.isSent &&
            lhs.nameOfFile == rhs.nameOfFile
    }
}
