//
//  ChatViewController.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/7/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift
import RxCocoa
import RxDataSources
import PureLayout
import DeepDiff
import Photos
import AVFoundation
import AVKit

protocol ChatDisplayLogic: class {
    func emptyChatField()
    
    func closeEmojiCollection()
    
    func downloadFileSuccessfully(url: URL)
    
    func updateTableView(newItems: [MessageItem])
    
    func update(newItems: [MessageItem])
    
    func updateSentMessage(newItems: [MessageItem], indexOfSentMessage: Int, newContent: String)
    
    func updateDownloadFileProgress(percent: CGFloat, messageID: String)
}

protocol LocationViewControllerDelegate: class {
    func didFinishLocationVC(snapshot: UIImage, location: CLLocationCoordinate2D, name: String)
}

protocol Listener: class  {
    func onClick(item: MessageItem)
}

class ChatViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var textFieldChat: UITextField!
    @IBOutlet private weak var buttonSend: UIButton!
    @IBOutlet private weak var buttonEmoji: UIButton!
    
    @IBOutlet private weak var buttonRowContainer: UIStackView!
    @IBOutlet private weak var buttonPickImage: UIButton!
    @IBOutlet private weak var buttonCapturePhoto: UIButton!
    @IBOutlet private weak var buttonSendFile: UIButton!
    @IBOutlet private weak var buttonLocation: UIButton!
    @IBOutlet private weak var buttonGoogleMap: UIButton!
    
    @IBOutlet private weak var emojiView: UICollectionView!
    private var emojiIcon = [String]()
    private let emojiPublishSubject = PublishSubject<String>()
    private var isEmojiCollectionShown = false
    
    private let disposeBag = DisposeBag()
    private var viewModel : ChatViewModel!
    
    private var conversationItemNavigation: ConversationItemNavigation!
    private var loginUser: User!
    
    private var baseOldItems = [MessageItem]()
    
    private var audioPlayer: AVAudioPlayer?
    private var playerItem: AVPlayerItem?
    private var avPlayer: AVPlayer?
    private var currAudioIndex = -1
    private var isPLaying = false
    
    private var currVideoIndex = -1
    
    private let publishSubject = PublishSubject<Bool>()
    private let pickedImagePublishSubject = PublishSubject<[String: Any]>()
    private let pickedFilePublishSubject = PublishSubject<[String: Any]>()
    private let reSendPublishSubject = PublishSubject<Bool>()
    private let downloadFilePublishSubject = PublishSubject<MessageItem>()
    private let sendLocationPublishSubject = PublishSubject<[String: Any]>()
    
    private var isSendingFile = false
    
    class func newInstance(conversationItemNavigation: ConversationItemNavigation, loginUser: User) -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        vc.conversationItemNavigation = conversationItemNavigation
        vc.loginUser = loginUser
        vc.initViewModel()
        return vc        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func initViewModel() {
        viewModel = ChatViewModel(displayLogic: self, conversationItemNavigation: self.conversationItemNavigation, loginUser: self.loginUser)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        setupUI()
        setupLayoutConstraints()
        bindViewModel()
        initEmoji()
    }
    
    private func initEmoji() {
        self.emojiIcon.append("😄")
        self.emojiIcon.append("😀")
        self.emojiIcon.append("😃")
        self.emojiIcon.append("😁")
        self.emojiIcon.append("😆")
        
        self.emojiIcon.append("😅")
        self.emojiIcon.append("😂")
        self.emojiIcon.append("🤣")
        self.emojiIcon.append("☺️")
        self.emojiIcon.append("😊")
        
        self.emojiIcon.append("😇")
        self.emojiIcon.append("🙂")
        self.emojiIcon.append("🙃")
        self.emojiIcon.append("😉")
        self.emojiIcon.append("😍")
        
        self.emojiView.reloadData()
        self.emojiView.performBatchUpdates({}, completion: {[unowned self] result in
            let width = self.emojiView.collectionViewLayout.collectionViewContentSize.width
            self.constraint?.autoRemove()
            self.constraint = self.emojiView.autoPinEdge(toSuperviewEdge: .left, withInset: (self.view.frame.width - width), relation: .lessThanOrEqual)
            self.emojiView.setNeedsLayout()
        })
    }
    
    private var constraint: NSLayoutConstraint?
    
    
    private func setupUI() {
        self.containerView.autoSetDimension(.height, toSize: 96.0)
        self.containerView.autoPinEdge(toSuperviewEdge: .left, withInset: 0.0)
        self.containerView.autoPinEdge(toSuperviewEdge: .right, withInset: 0.0)
        self.containerView.autoPinEdge(toSuperviewMargin: .bottom)
        
        self.buttonEmoji.isUserInteractionEnabled = true
        self.buttonEmoji.autoSetDimensions(to: CGSize(width: 44.0, height: 44.0))
        self.buttonEmoji.autoPinEdge(.left, to: .left, of: containerView, withOffset: 8.0)
        self.buttonEmoji.autoPinEdge(.bottom, to: .bottom, of: containerView, withOffset: -8.0)
        self.buttonEmoji.setImage(UIImage.imageEmoji, for: .normal)
        self.buttonEmoji.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showEmoji)))
        
        self.buttonSend.autoSetDimensions(to: CGSize(width: 44.0, height: 44.0))
        self.buttonSend.autoPinEdge(.right, to: .right, of: containerView, withOffset: -8.0)
        self.buttonSend.autoPinEdge(.bottom, to: .bottom, of: containerView, withOffset: -8.0)
        self.buttonSend.setImage(UIImage.imageSend, for: .normal)
        
        self.textFieldChat.autoSetDimension(.height, toSize: 44.0)
        self.textFieldChat.layer.cornerRadius = 8.0
        self.textFieldChat.autoPinEdge(.left, to: .right, of: buttonEmoji, withOffset: 8.0)
        self.textFieldChat.autoPinEdge(.bottom, to: .bottom, of: containerView, withOffset: -8.0)
        self.textFieldChat.autoPinEdge(.right, to: .left, of: buttonSend, withOffset: -8.0)
        self.textFieldChat.autocorrectionType = .no
        self.textFieldChat.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(textFieldTapped)))
        
        self.buttonRowContainer.autoPinEdge(toSuperviewEdge: .top, withInset: 8.0)
        self.buttonRowContainer.autoPinEdge(.left, to: .left, of: containerView, withOffset: 8.0)
        self.buttonRowContainer.autoPinEdge(.right, to: .right, of: containerView, withOffset: -8.0, relation: .lessThanOrEqual)
        self.buttonRowContainer.autoPinEdge(.bottom, to: .top, of: textFieldChat, withOffset: -8.0)
        self.buttonRowContainer.alignment = .leading
        self.buttonRowContainer.axis = .horizontal
        self.buttonRowContainer.spacing = 32.0
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(pickImage))
        self.buttonPickImage.isUserInteractionEnabled = true
        self.buttonPickImage.autoSetDimensions(to: CGSize(width: 32.0, height: 32.0))
        self.buttonPickImage.setBackgroundImage(UIImage.imageGallery, for: .normal)
        self.buttonPickImage.contentMode = .scaleAspectFill
        self.buttonPickImage.addGestureRecognizer(tap)
        
        
        self.buttonCapturePhoto.isUserInteractionEnabled = true
        self.buttonCapturePhoto.autoSetDimensions(to: CGSize(width: 32.0, height: 32.0))
        self.buttonCapturePhoto.setBackgroundImage(UIImage.imageCamera, for: .normal)
        self.buttonCapturePhoto.contentMode = .scaleAspectFill
        
        self.buttonSendFile.isUserInteractionEnabled = true
        self.buttonSendFile.autoSetDimensions(to: CGSize(width: 32.0, height: 32.0))
        self.buttonSendFile.setBackgroundImage(UIImage.imageFile, for: .normal)
        self.buttonSendFile.contentMode = .scaleAspectFill
        self.buttonSendFile.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(sendFile)))
        
        self.buttonLocation.isUserInteractionEnabled = true
        self.buttonLocation.autoSetDimensions(to: CGSize(width: 32.0, height: 32.0))
        self.buttonLocation.setBackgroundImage(UIImage.imageLocation, for: .normal)
        self.buttonLocation.contentMode = .scaleAspectFill
        self.buttonLocation.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(sendLocation)))
        
        self.buttonGoogleMap.isUserInteractionEnabled = true
        self.buttonGoogleMap.autoSetDimensions(to: CGSize(width: 32.0, height: 32.0))
        self.buttonGoogleMap.setBackgroundImage(UIImage.imageLocation, for: .normal)
        self.buttonGoogleMap.contentMode = .scaleAspectFill
        self.buttonGoogleMap.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(sendLocationGoogleMap)))
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.tableView.transform = CGAffineTransform(rotationAngle: -(CGFloat)(Double.pi))
        self.tableView.showsVerticalScrollIndicator = false
        self.tableView.autoPinEdge(toSuperviewEdge: .left)
        self.tableView.autoPinEdge(toSuperviewMargin: .top)
        self.tableView.autoPinEdge(toSuperviewEdge: .right)
        self.tableView.autoPinEdge(.bottom, to: .top, of: self.containerView)
        self.tableView.separatorStyle = .none
        self.tableView.backgroundColor = UIColor.chatBackgroundGrey
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 80
        
        self.tableView.rx.itemSelected.asDriver()
            .drive(onNext: { [unowned self] indexPath in
                self.tableView.deselectRow(at: indexPath, animated: false)
            })
            .disposed(by: disposeBag)
        
        
        self.emojiView.dataSource = self
        self.emojiView.delegate = self
        
        self.emojiView.autoSetDimension(.height, toSize: 128.0)
        constraint = self.emojiView.autoPinEdge(toSuperviewEdge: .left, withInset: 128.0, relation: .lessThanOrEqual)
        self.emojiView.autoPinEdge(toSuperviewEdge: .right)
        self.emojiView.autoPinEdge(.bottom, to: .top, of: self.containerView)
        self.emojiView.showsVerticalScrollIndicator = false
        self.emojiView.isHidden = true
        
        self.emojiView.rx.itemSelected.asDriver()
            .drive(onNext: { [unowned self] indexPath in
                self.emojiView.deselectItem(at: indexPath, animated: false)
                self.emojiPublishSubject.onNext(self.emojiIcon[indexPath.row])
            })
            .disposed(by: disposeBag)
    }
    
    private func setupLayoutConstraints() {
        
    }
    
    private func changeStateOfEmojiCollection() {
        self.emojiView.isHidden = !self.emojiView.isHidden
        self.isEmojiCollectionShown = !self.emojiView.isHidden
    }
    
    @objc func textFieldTapped() {
        self.textFieldChat.becomeFirstResponder()
        if (isEmojiCollectionShown) {
            changeStateOfEmojiCollection()
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
    
    @objc func pickImage() {
        if askForPermission() {
            isSendingFile = false
            
            let alertController = UIAlertController.init(title: "Choose image", message: "", preferredStyle: .actionSheet)
            alertController.addAction(UIAlertAction(title: "Camera", style: .default, handler: { action in
                if UIImagePickerController.isSourceTypeAvailable(.camera) {
                    let imagePicker = UIImagePickerController()
                    imagePicker.delegate = self
                    imagePicker.sourceType = .camera
                    self.present(imagePicker, animated: true, completion: nil)
                } else {
                    self.showToast(message: "No camera available.")
                }
            }))
            alertController.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { action in
                if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum) {
                    let imagePickerController = UIImagePickerController()
                    imagePickerController.delegate = self
                    imagePickerController.sourceType = .savedPhotosAlbum
                    self.present(imagePickerController, animated: true, completion: nil)
                } else {
                    self.showToast(message: "No gallery available.")
                }
            }))
            alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            alertController.popoverPresentationController?.sourceView = self.view
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    @objc func sendFile() {
        if askForPermission() {
            isSendingFile = true
            
            let alertController = UIAlertController.init(title: "Choose image", message: "", preferredStyle: .actionSheet)
            alertController.addAction(UIAlertAction(title: "Camera", style: .default, handler: { action in
                if UIImagePickerController.isSourceTypeAvailable(.camera) {
                    let imagePicker = UIImagePickerController()
                    imagePicker.delegate = self
                    imagePicker.sourceType = .camera
                    self.present(imagePicker, animated: true, completion: nil)
                } else {
                    self.showToast(message: "No camera available.")
                }
            }))
            alertController.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { action in
                if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum) {
                    let imagePickerController = UIImagePickerController()
                    imagePickerController.delegate = self
                    imagePickerController.sourceType = .savedPhotosAlbum
                    self.present(imagePickerController, animated: true, completion: nil)
                } else {
                    self.showToast(message: "No gallery available.")
                }
            }))
            alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            alertController.popoverPresentationController?.sourceView = self.view
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    @objc func sendLocation() {
        let vc = LocationViewController.newInstance(user: self.loginUser)
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func sendLocationGoogleMap() {
        let vc = GoogleMapController.newInstance(loginUser: self.loginUser) as! GoogleMapController
        vc.delegate = self
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func bindViewModel() {
        let viewWillAppear = self.rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        
        let input = ChatViewModel.Input(trigger: viewWillAppear,
                                        sendTrigger: buttonSend.rx.tap.asDriver(),
                                        chatField: textFieldChat.rx.text.orEmpty,
                                        publishSubject: self.publishSubject,
                                        pickedImagePublishSubject: self.pickedImagePublishSubject,
                                        pickedFilePublishSubject: self.pickedFilePublishSubject,
                                        reSendPublishSubject: self.reSendPublishSubject,
                                        emojiPublishSubject: self.emojiPublishSubject,
                                        downloadFilePublishSubject: self.downloadFilePublishSubject,
                                        sendLocationPublishSubject: self.sendLocationPublishSubject)
        
        let output = viewModel.transform(input: input)
        output.fetching.drive(onNext: { [unowned self] (show) in
            self.showLoading(withStatus: show)
        })
            .disposed(by: disposeBag)
        
        output.error.drive(onNext: { [unowned self] (error) in
            self.showError(error: error)
        })
            .disposed(by: disposeBag)
    }
    
    // TABLE VIEW
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = baseOldItems[indexPath.row]
        
        let cell: BaseMessageCell = tableView.dequeueReusableCell(withIdentifier: item.getReuseID()) as! BaseMessageCell
        cell.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
        cell.backgroundColor = UIColor.clear
        cell.bind(item: item, listener: self)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.baseOldItems.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    
    // COLLECTION VIEW
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.emojiIcon.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! EmojiCell
        
        cell.bind(emoji: self.emojiIcon[indexPath.row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    
    private func askForPermission() -> Bool {
        var isAllowed = false
        
        let photos = PHPhotoLibrary.authorizationStatus()
        
        if photos == .notDetermined {
            PHPhotoLibrary.requestAuthorization({ status in
                if status == .authorized {
                    isAllowed = true
                }
                else {
                    isAllowed = false
                }
            })
        }
        else if photos == .denied {
            let alert = UIAlertController(title: "Please grant permission", message: "", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Grant permission manually", style: .default, handler: {action in
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!,
                                          options: Converter.convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]),
                                          completionHandler: { success in
                                            isAllowed = true
                })
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
        }
        else {
            isAllowed = true
        }
        
        return isAllowed
    }
    
    @objc func showEmoji() {
        changeStateOfEmojiCollection()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        super.viewDidDisappear(animated)
    }
}

extension ChatViewController: ChatDisplayLogic {
    func emptyChatField() {
        self.textFieldChat.text = ""
    }
    
    func closeEmojiCollection() {
        if (isEmojiCollectionShown) {
            self.emojiView.isHidden = !self.emojiView.isHidden
            self.isEmojiCollectionShown = !self.emojiView.isHidden
        }
    }
    
    func downloadFileSuccessfully(url: URL) {
        self.showToast(message: StringUtils.DownloadFileSuccessfully + "\(url.path)")
    }
    
    // new child added
    func updateTableView(newItems: [MessageItem]) {
        let changes = diff(old: baseOldItems, new: newItems)
        // When new message is added to the firebase, in the ChatViewModel we have modified the time label appearance of the previous message
        // So we also have to modify that timeLabel appearance in this view controller
        if baseOldItems.count > 0 && newItems.count > 1 {
            baseOldItems[0].isTimeVisible = newItems[1].isTimeVisible
        }
        
        let item = newItems[0]
        if item is MessageTextItem {
            baseOldItems.insert(MessageTextItem(item: item as! MessageTextItem), at: 0)
        } else if item is MessageFileItem {
            baseOldItems.insert(MessageFileItem(item: item as! MessageFileItem), at: 0)
        } else if item is MessageLocationItem {
            baseOldItems.insert(MessageLocationItem(item: item as! MessageLocationItem), at: 0)
        } else if item is MessageImageItem {
            baseOldItems.insert(MessageImageItem(item: item as! MessageImageItem), at: 0)
        } else if item is MessageAudioItem {
            baseOldItems.insert(MessageAudioItem(item: item as! MessageAudioItem), at: 0)
        } else if item is MessageVideoItem {
            baseOldItems.insert(MessageVideoItem(item: item as! MessageVideoItem), at: 0)
        }
        
        self.tableView.reload(changes: changes, section: 0, insertionAnimation: .top, deletionAnimation: .fade, replacementAnimation: .fade, completion: { _ in
            
        })
    }
    
    // fetching
    func update(newItems: [MessageItem]) {
        if newItems.count == 0 {
            publishSubject.onNext(true)
            return
        }
        let changes = diff(old: baseOldItems, new: newItems)
        
        newItems.forEach { [unowned self] item in
            if item is MessageTextItem {
                self.baseOldItems.append(MessageTextItem(item: item as! MessageTextItem))
            } else if item is MessageFileItem {
                self.baseOldItems.append(MessageFileItem(item: item as! MessageFileItem))
            } else if item is MessageLocationItem {
                self.baseOldItems.append(MessageLocationItem(item: item as! MessageLocationItem))
            } else if item is MessageImageItem {
                self.baseOldItems.append(MessageImageItem(item: item as! MessageImageItem))
            } else if item is MessageAudioItem {
                self.baseOldItems.append(MessageAudioItem(item: item as! MessageAudioItem))
            } else if item is MessageVideoItem {
                self.baseOldItems.append(MessageVideoItem(item: item as! MessageVideoItem))
            }
        }
        
        self.tableView.reload(changes: changes, section: 0, insertionAnimation: .top, deletionAnimation: .fade, replacementAnimation: .top, completion: {
            [unowned self] _ in
            self.publishSubject.onNext(true)
        })
    }
    
    func updateSentMessage(newItems: [MessageItem], indexOfSentMessage: Int, newContent: String) {
        let changes = diff(old: baseOldItems, new: newItems)
        let changeItem = baseOldItems[indexOfSentMessage]
        changeItem.setIsSent()
        
        if case .loginFile = changeItem.messageItemType {
            baseOldItems[indexOfSentMessage].changeNewContent(newContent: newContent)
        }
        
        self.tableView.reload(changes: changes, section: 0, insertionAnimation: .top, deletionAnimation: .fade, replacementAnimation: .none, completion: { _ in
        })
    }
    
    func updateDownloadFileProgress(percent: CGFloat, messageID: String) {
        let index = self.baseOldItems.firstIndex(where: { messageItem in
            messageItem.id == messageID
        })
        
        let item = self.baseOldItems[index!] as! MessageFileItem
        item.progressPercent = percent
        
        if let cell = self.tableView.cellForRow(at: IndexPath(row: index!, section: 0)) as? BaseMessageCell {
            if let fileCell = cell as? LoginFileCell {
                fileCell.updateProgressBar()
            } else if let fileCell = cell as? OthersFileCell {
                fileCell.updateProgressBar()
            }
        }
        
    }
}

private let snapshotKey = "Snapshot"
private let locationKey = "Location"
private let nameKey = "Name"
extension ChatViewController: LocationViewControllerDelegate {
    func didFinishLocationVC(snapshot: UIImage, location: CLLocationCoordinate2D, name: String) {
        self.navigationController?.popViewController(animated: true)
        let dictionary = [snapshotKey: snapshot,
                          locationKey: location,
                          nameKey: name] as [String: Any]
        self.sendLocationPublishSubject.onNext(dictionary)
    }
}

extension ChatViewController: Listener {
    func onClick(item: MessageItem) {
        if item is MessageImageItem {
            onImageClick(item: item)
        } else if item is MessageFileItem {
            onFileClick(item: item)
        } else if item is MessageAudioItem {
            onAudioClick(item: item)
        } else if item is MessageVideoItem {
            onVideoClick(item: item)
        }
    }
    
    private func onImageClick(item: MessageItem) {
        let vc = ViewImageController.newInstance(messageID: item.id, url: item.userContent)
        vc.hidesBottomBarWhenPushed = true
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    private func onFileClick(item: MessageItem) {
        self.downloadFilePublishSubject.onNext(item)
    }
    
    private func onAudioClick(item: MessageItem) {
        let item = item as! MessageAudioItem
        
        let index = self.baseOldItems.firstIndex(where: { messageItem in
            messageItem.id == item.id
        })!
        
        if currAudioIndex == index {
            if let cell = self.tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? LoginAudioCell {
                if isPLaying {
                    isPLaying = false
                    self.avPlayer?.pause()
                    cell.stopPlaying()
                } else {
                    isPLaying = true
                    self.avPlayer?.play()
                    cell.startPlaying()
                }
            }
        }
        else {
            currAudioIndex = index
            let playerItem = AVPlayerItem(url: URL(string: item.userContent)!)
            
            self.avPlayer = AVPlayer(playerItem: playerItem)
            self.avPlayer?.volume = 1.0
            self.avPlayer?.addObserver(self, forKeyPath: "status", options: [.new], context: nil)
            self.avPlayer?.play()
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if ((object! as! AVPlayer) == self.avPlayer! && keyPath == "status") {            
            if avPlayer!.status == AVPlayer.Status.readyToPlay {
                if let cell = self.tableView.cellForRow(at: IndexPath(row: currAudioIndex, section: 0)) as? LoginAudioCell {
                    isPLaying = true
                    cell.startPlaying()
                }
            }
        }
    }
    
    private func onVideoClick(item: MessageItem) {
        let videoItem = item as! MessageVideoItem
        
        let index = self.baseOldItems.firstIndex(where: { messageItem in
            videoItem.id == messageItem.id
        })
        
        if index == currVideoIndex { // Current Video cell may be isPlaying, or not playing
            handleCurrentVideoCell(index: index!, videoItem: videoItem)
        } else { // Need to stop the previous video cell because that cell is maybe playing
            if let previousCell = self.tableView.cellForRow(at: IndexPath(row: currVideoIndex, section: 0)) { // previous cell maybe out of screen
                if let cell = previousCell as? LoginVideoCell {
                    cell.stopPlaying()
                }
            }
            
            currVideoIndex = index!
            handleCurrentVideoCell(index: index!, videoItem: videoItem)
        }
    }
    
    private func handleCurrentVideoCell(index: Int, videoItem: MessageVideoItem) {
        let cell = self.tableView.cellForRow(at: IndexPath(row: index, section: 0)) as! BaseMessageCell
        if let cell = cell as? LoginVideoCell {
            if cell.isPlaying() {
                cell.stopPlaying()
                
                let player = AVPlayer(url: URL(string: videoItem.userContent)!)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                self.present(playerViewController, animated: true, completion: {
                    playerViewController.player?.play()
                })
            } else { // it is pausing
                cell.startPlaying()
            }
            
        }
    }
}


extension ChatViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // Local variable inserted by Swift 4.2 migrator.
        let info = Converter.convertFromUIImagePickerControllerInfoKeyDictionary(info)
        if !isSendingFile {
            self.pickedImagePublishSubject.onNext(info)
        } else {
            self.pickedFilePublishSubject.onNext(info)
        }
        self.dismiss(animated: true, completion: nil)        
    }
}

extension ChatViewController: AVAudioPlayerDelegate {
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        if let cell = self.tableView.cellForRow(at: IndexPath(row: currAudioIndex, section: 0)) as? LoginAudioCell {
            cell.stopPlaying()
        }
    }
}
