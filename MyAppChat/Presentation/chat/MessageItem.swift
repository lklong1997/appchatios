//
//  MessageItem.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/7/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//
import UIKit

enum MessageItem: Hashable {
    case text(MessageTextItem)
    case image(MessageImageItem)
    case file(MessageFileItem)
    
    var hashValue: Int {
        switch self {
        case .text(let messageTextItem): return messageTextItem.hashValue
        case .image(let messageImageItem): return messageImageItem.hashValue
        case .file(let messageFileItem): return messageFileItem.hashValue
        }
    }
    
    static func == (lhs: MessageItem, rhs: MessageItem) -> Bool {
        switch (lhs, rhs) {
        case (.text(let lhsValue), .text(let rhsValue)):
            return lhsValue == rhsValue
        case (.image(let lhsValue), .image(let rhsValue)):
            return lhsValue == rhsValue
        case (.file(let lhsValue), .file(let rhsValue)):
            return lhsValue == rhsValue
        default:
            return false
        }
    }
    
    func hideTimeLabel() {
        switch self {
        case .text(let messageTextItem):
            messageTextItem.hideTimeLabel()
        case .image(let messageImageItem):
            messageImageItem.hideTimeLabel()
        case .file(let messageFileItem):
            messageFileItem.hideTimeLabel()
        }
    }
    
    func showTimeLabel() {
        switch self {
        case .text(let messageTextItem):
            messageTextItem.showTimeLabel()
        case .image(let messageImageItem):
            messageImageItem.showTimeLabel()
        case .file(let messageFileItem):
            messageFileItem.showTimeLabel()
        }
    }
    
    func hideAvatar() {
        switch self {
        case .text(let messageTextItem):
            messageTextItem.hideAvatar()
        case .image(let messageImageItem):
            messageImageItem.hideAvatar()
        case .file(let messageFileItem):
            messageFileItem.hideAvatar()
        }
    }
    
    func showAvatar() {
        switch self {
        case .text(let messageTextItem):
            messageTextItem.showAvatar()
        case .image(let messageImageItem):
            messageImageItem.showAvatar()
        case .file(let messageFileItem):
            messageFileItem.showAvatar()
        }
    }
    
    func sender() -> String {
        switch self {
        case .text(let messageTextItem):
            return messageTextItem.sender()
        case .image(let messageImageItem):
            return messageImageItem.sender()
        case .file(let messageFileItem):
            return messageFileItem.sender()
        }
    }
    
    func content() -> String {
        switch self {
        case .text(let messageTextItem):
            return messageTextItem.content()
        case .image(let messageImageItem):
            return messageImageItem.content()
        case .file(let messageFileItem):
            return messageFileItem.content()
        }
    }
    
    func id() -> String {
        switch self {
        case .text(let messageTextItem):
            return messageTextItem.id()
        case .image(let messageImageItem):
            return messageImageItem.id()
        case .file(let messageFileItem):
            return messageFileItem.id()
        }
    }
    
    func setIsSent() {
        switch self {
        case .text(let messageTextItem):
            messageTextItem.setIsSent()
        case .image(let messageImageItem):
            messageImageItem.setIsSent()
        case .file(let messageFileItem):
            messageFileItem.setIsSent()
        }
    }
    
    func changeNewContent(newContent: String) {
        switch self {
        case .text(let messageTextItem):
            messageTextItem.changeNewContent(newContent: newContent)
        case .image(let messageImageItem):
            messageImageItem.changeNewContent(newContent: newContent)
        case .file(let messageFileItem):
            messageFileItem.changeNewContent(newContent: newContent)
        }
    }
    
    func atTime() -> String {
        switch self {
        case .text(let messageTextItem):
            return messageTextItem.atTime()
        default:
            return ""
            
        }
    }
}
