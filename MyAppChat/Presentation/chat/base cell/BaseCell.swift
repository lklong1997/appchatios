//
//  BaseCell.swift
//  MyAppChat
//
//  Created by CPU11814 on 10/19/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import UIKit

class BaseCell<M>: UITableViewCell {
    var item: M!
    internal weak var listener: Listener?
    
    func bind(item: M, listener: Listener) {
        self.item = item
        self.listener = listener
    }
}
