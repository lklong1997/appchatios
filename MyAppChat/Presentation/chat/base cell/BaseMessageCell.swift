//
//  BaseMessageCell.swift
//  MyAppChat
//
//  Created by CPU11814 on 10/19/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import UIKit

class BaseMessageCell: BaseCell<MessageItem> {
    override func awakeFromNib() {
        super.awakeFromNib()

        setupUI()
    }
    
    func setupUI() {
        selectionStyle = .none       
    }
        
    override func bind(item: MessageItem, listener: Listener) {
        super.bind(item: item,  listener: listener)
    }
    
    func onClick() {
        listener?.onClick(item: item)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
