//
//  BaseItem.swift
//  MyAppChat
//
//  Created by CPU11814 on 10/19/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import Foundation
import UIKit

class MessageItem {    
    func changeNewContent(newContent: String) {
        self.userContent = newContent
    }
    
    func setIsSent() {
        self.isSent = true
    }
    
    func showTimeLabel() {
        self.isTimeVisible = true
    }
    
    func hideTimeLabel() {
        self.isTimeVisible = false
    }
    
    func showAvatar() {
        self.isAvatarVisible = true
    }
    
    func hideAvatar() {
        self.isAvatarVisible = false
    }

    let messageItemType: MessageItemType
    
    let id: String!
    let type: String!
    var userContent: String!
    let userName: String!
    let atTime: String!
    let avatarUrl: String!
    var isTimeVisible: Bool = false
    var isSent: Bool = false
    var isAvatarVisible: Bool = false
    
    init(messageItemType: MessageItemType, id: String!, atTime: String!, type: String!, userContent: String!, userName: String!, avatarUrl: String!, isTimeVisible: Bool) {
        self.messageItemType = messageItemType
        self.id = id
        self.atTime = atTime
        self.type = type
        self.userName = userName
        self.userContent = userContent
        self.avatarUrl = avatarUrl
        self.isTimeVisible = isTimeVisible
    }
    
    init(item: MessageItem) {
        self.messageItemType = item.messageItemType
        self.id = item.id
        self.atTime = item.atTime
        self.type = item.type
        self.userName = item.userName
        self.userContent = item.userContent
        self.avatarUrl = item.avatarUrl
        self.isTimeVisible = item.isTimeVisible
        self.isSent = item.isSent
    }
    
    public func getReuseID() -> String {
        switch self.messageItemType {
        case .loginText:
            return String(describing: LoginTextCell.self)
        case .othersText:
            return String(describing: OthersTextCell.self)
        case .loginImage:
            return String(describing: LoginImageCell.self)
        case .othersImage:
            return String(describing: OthersImageCell.self)
        case .loginFile:
            return String(describing: LoginFileCell.self)
        case .othersFile:
            return String(describing: OthersFileCell.self)
        case .loginLocation:
            return String(describing: LoginLocationCell.self)
        case .othersLocation:
            return String(describing: OthersLocationCell.self)
        case .loginVideo:
            return String(describing: LoginVideoCell.self)
//        case .othersVideo:
//
        case .loginAudio:
            return String(describing: LoginAudioCell.self)
//        case .othersAudio:
//
        default:
            return ""
        }
    }
}

enum MessageItemType {
    case loginText
    case othersText
    
    case loginImage
    case othersImage
    
    case loginFile
    case othersFile
    
    case loginVideo
    case othersVideo
    
    case loginAudio
    case othersAudio
    
    case loginLocation
    case othersLocation
}

extension Hashable where Self: MessageItem {
    var hashValue: Int {
        return id.hashValue
    }
    
    static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.id == rhs.id &&
            lhs.atTime == rhs.atTime &&
            lhs.type == rhs.type &&
            lhs.userContent == rhs.userContent &&
            lhs.userName == rhs.userName &&
            lhs.isTimeVisible == rhs.isTimeVisible &&
            lhs.isAvatarVisible == rhs.isAvatarVisible &&
            lhs.isSent == rhs.isSent
    }
}

extension MessageItem: Hashable {
    
}
