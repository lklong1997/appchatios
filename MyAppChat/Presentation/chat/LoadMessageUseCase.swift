//
//  LoadMessageUseCase.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/12/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift

class LoadMessageUseCase: UseCase {
    private let repository : MessageRepository = MessageRepositoryFactory.sharedInstance
    
    public typealias Request = LoadMessagesRequest
    public typealias Response = [Message]
    
    func execute(request: LoadMessagesRequest) -> Observable<[Message]> {
        return repository.loadMessage(request: request)
    }
}
