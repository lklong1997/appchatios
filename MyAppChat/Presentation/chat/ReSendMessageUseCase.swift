//
//  ReSendMessageUseCase.swift
//  MyAppChat
//
//  Created by CPU11814 on 10/8/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift

class ReSendMessageUseCase: UseCase {
    private let repository: MessageRepository = MessageRepositoryFactory.sharedInstance
    
    public typealias Request = ReSendMessageRequest
    public typealias Response = Message
    
    func execute(request: ReSendMessageRequest) -> Observable<Message> {
        return repository.reSendMessage(request: request)
    }
}
