//
//  LoginVideoCell.swift
//  MyAppChat
//
//  Created by CPU11814 on 11/13/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import MapKit
import AVFoundation

class LoginVideoCell: BaseMessageCell {
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var stackView: UIStackView!
    
    @IBOutlet private weak var videoContainer: UIView!
    
    @IBOutlet private weak var timeLabelContainer: UIView!
    @IBOutlet private weak var atTime: UILabel!
    
    private var player: AVPlayer?
    private var playPauseButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        stopPlaying()
        super.prepareForReuse()
    }
    
    override func setupUI() {
        super.setupUI()
        
        self.containerView.backgroundColor = UIColor.clear
        self.containerView.autoPinEdge(toSuperviewEdge: .top, withInset: 8.0)
        self.containerView.autoPinEdge(toSuperviewEdge: .left, withInset: 64.0, relation: .greaterThanOrEqual)
        self.containerView.autoPinEdge(toSuperviewEdge: .right, withInset: 8.0)
        self.containerView.autoPinEdge(toSuperviewEdge: .bottom, withInset: 8.0)
        
        self.stackView.alignment = .leading
        self.stackView.axis = .vertical
        self.stackView.spacing = 8.0
        self.stackView.autoPinEdge(toSuperviewEdge: .top, withInset: 5.0)
        self.stackView.autoPinEdge(toSuperviewEdge: .bottom, withInset: 5.0)
        self.stackView.autoPinEdge(toSuperviewEdge: .left, withInset: 5.0)
        self.stackView.autoPinEdge(toSuperviewEdge: .right, withInset: 0.0)
        
        self.videoContainer.autoSetDimensions(to: CGSize(width: 256.0, height: 144.0))
        self.videoContainer.layer.cornerRadius = 16.0
        self.videoContainer.layer.borderWidth = 0.0
        self.videoContainer.clipsToBounds = true
        self.videoContainer.isUserInteractionEnabled = true
        self.videoContainer.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onItemClick)))
        
        playPauseButton = UIButton(frame: CGRect(x: 0.0, y: 0.0, width: 44.0, height: 44.0))
        playPauseButton.setImage(UIImage.imagePlayVideoChat, for: .normal)
        playPauseButton.isUserInteractionEnabled = true
        playPauseButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onItemClick)))
        
        player = AVPlayer(playerItem: nil)
        let playerLayer = AVPlayerLayer(player: player)
        //playerLayer.frame = self.videoContainer.bounds
        playerLayer.frame = CGRect(x: 0.0, y: 0.0, width: 256.0, height: 144.0) // As we are changing the size of videoContainer, at this point the layout hasn't been re-drawn yet 
        self.videoContainer.layer.addSublayer(playerLayer)
        self.videoContainer.addSubview(playPauseButton)
        
        playPauseButton.autoAlignAxis(.horizontal, toSameAxisOf: self.videoContainer)
        playPauseButton.autoAlignAxis(.vertical, toSameAxisOf: self.videoContainer)
        
        
        self.timeLabelContainer.backgroundColor = UIColor.loginUserChat
        self.timeLabelContainer.layer.cornerRadius = 8.0
        self.timeLabelContainer.layer.borderWidth = 1.0
        self.timeLabelContainer.layer.borderColor = UIColor.lightGray.cgColor
        self.timeLabelContainer.clipsToBounds = true
        
        self.atTime.textAlignment = .right
        self.atTime.font = UIFont.systemFont(ofSize: 11.0)
        self.atTime.textColor = UIColor.lightGray
        self.atTime.autoPinEdge(toSuperviewEdge: .top, withInset: 5.0)
        self.atTime.autoPinEdge(toSuperviewEdge: .left, withInset: 5.0)
        self.atTime.autoPinEdge(toSuperviewEdge: .right, withInset: 5.0)
        self.atTime.autoPinEdge(toSuperviewEdge: .bottom, withInset: 5.0)
    }
    
    override func bind(item: MessageItem, listener: Listener) {
        super.bind(item: item, listener: listener)
        
        let videoItem = item as! MessageVideoItem
        
        if let item = player?.currentItem {
            if let url = (item.asset as? AVURLAsset)?.url {
                if url != URL(string: videoItem.userContent) { // This player item's url is different from videoItem's url
                    setNewPlayerItem(videoItem: videoItem)
                }
            }
        } else { // this player doesn't have playerItem
            setNewPlayerItem(videoItem: videoItem)
        }
        
        if videoItem.isSent {
            self.timeLabelContainer.backgroundColor = UIColor.loginUserChat
        } else {
            self.timeLabelContainer.backgroundColor = UIColor.red
        }
        
        if !videoItem.isTimeVisible {
            self.stackView.removeArrangedSubview(self.timeLabelContainer)
            self.timeLabelContainer.isHidden = true
        } else {
            self.timeLabelContainer.isHidden = false
            self.stackView.addArrangedSubview(self.timeLabelContainer)
            
            self.atTime.text = videoItem.atTime
        }
    }
    
    @objc func onItemClick() {
        super.onClick()
    }
    
    private func setNewPlayerItem(videoItem: MessageVideoItem) {
        player?.replaceCurrentItem(with: AVPlayerItem(url: URL(string: videoItem.userContent)!))
        // do not call player = AVPlayer(...) as we will have to reconfigure the player layer (or we can't see anything)
        
        //player?.seek(to: CMTime(value: CMTimeValue(1.0), timescale: CMTimeScale(1.0)))
        player?.pause()
    }
    
    func isPlaying() -> Bool {
        let item = self.item as! MessageVideoItem
        return item.isPlaying
    }
    
    func startPlaying() {
        let item = self.item as! MessageVideoItem
        item.isPlaying = true
        self.playPauseButton.isHidden = true
        self.player?.play()
    }
    
    func stopPlaying() {
        let item = self.item as! MessageVideoItem
        item.isPlaying = false
        self.playPauseButton.isHidden = false
        self.player?.pause()
    }
}
