//
//  CellConfigurator.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/20/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import Foundation
import UIKit

//protocol CellConfigurator {
//    static var reuseID: String { get }
//    func bind(cell: UIView, listener: Listener)
//    func setIsSent()
//    func changeNewContent(newContent: String)
//}
//
//class TableCellConfigurator<CellType: ConfigurableCell, Item: ConfigurableItem>: CellConfigurator where CellType.Item == Item{
//    static var reuseID: String {
//        return String(describing: CellType.self)
//    }
//
//    let item: Item
//
//    init(item: Item) {
//        self.item = item
//    }
//
//    func bind(cell: UIView, listener: Listener) {
//        (cell as! CellType).bind(item: item, listener: listener)
//    }
//
//    func setIsSent() {
//        item.setIsSent()
//    }
//
//    func changeNewContent(newContent: String) {
//        item.changeNewContent(newContent: newContent)
//    }
//
//}
//
//typealias LoginTextConfigurator = TableCellConfigurator<LoginTextCell, LoginTextItem>
//typealias OthersTextConfigurator = TableCellConfigurator<OthersTextCell, OthersTextItem>
//typealias LoginImageConfigurator = TableCellConfigurator<LoginImageCell, LoginImageItem>
//typealias OthersImageConfigurator = TableCellConfigurator<OthersImageCell, OthersImageItem>
//typealias LoginFileConfigurator = TableCellConfigurator<LoginFileCell, LoginFileItem>
//typealias OthersFileConfigurator = TableCellConfigurator<OthersFileCell, OthersFileItem>
