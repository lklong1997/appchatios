//
//  LoginImageCell.swift
//  MyAppChat
//
//  Created by CPU11814 on 10/1/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import UIKit
import Kingfisher


class LoginImageCell: BaseMessageCell {
    @IBOutlet private weak var contentImage: UIImageView!
    @IBOutlet private weak var atTimeLabel: UILabel!
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var stackView: UIStackView!
    @IBOutlet private weak var timeLabelContainer: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func setupUI() {
        super.setupUI()
        
        self.containerView.backgroundColor = UIColor.clear
        self.containerView.autoPinEdge(toSuperviewEdge: .top, withInset: 8.0)
        self.containerView.autoPinEdge(toSuperviewEdge: .right, withInset: 8.0)
        self.containerView.autoPinEdge(toSuperviewEdge: .bottom, withInset: 8.0)
        self.containerView.autoPinEdge(.left, to: .left, of: containerView.superview!, withOffset: 64.0, relation: .greaterThanOrEqual)
        
        self.stackView.alignment = .leading
        self.stackView.axis = .vertical
        self.stackView.spacing = 8.0
        self.stackView.autoPinEdge(toSuperviewEdge: .top, withInset: 5.0)
        self.stackView.autoPinEdge(toSuperviewEdge: .bottom, withInset: 5.0)
        self.stackView.autoPinEdge(toSuperviewEdge: .left, withInset: 5.0)
        self.stackView.autoPinEdge(toSuperviewEdge: .right, withInset: 0.0)
        
        self.contentImage.autoSetDimensions(to: CGSize(width: 128.0, height: 128.0))
        self.contentImage.layer.cornerRadius = 16.0
        self.contentImage.layer.borderWidth = 0.0
        self.contentImage.clipsToBounds = true
        self.contentImage.contentMode = .scaleAspectFill
        
        self.timeLabelContainer.backgroundColor = UIColor.loginUserChat
        self.timeLabelContainer.layer.cornerRadius = 8.0
        self.timeLabelContainer.layer.borderColor = UIColor.lightGray.cgColor
        self.timeLabelContainer.layer.borderWidth = 1.0
        self.timeLabelContainer.clipsToBounds = true
        
        self.atTimeLabel.textAlignment = .right
        self.atTimeLabel.font = UIFont.systemFont(ofSize: 11.0)
        self.atTimeLabel.textColor = UIColor.lightGray
        self.atTimeLabel.autoPinEdge(toSuperviewEdge: .top, withInset: 5.0)
        self.atTimeLabel.autoPinEdge(toSuperviewEdge: .bottom, withInset: 5.0)
        self.atTimeLabel.autoPinEdge(toSuperviewEdge: .left, withInset: 5.0)
        self.atTimeLabel.autoPinEdge(toSuperviewEdge: .right, withInset: 5.0)
    }
    
    override func bind(item: MessageItem, listener: Listener) {
        super.bind(item: item, listener: listener)
        
        let imageItem = item as! MessageImageItem
        if imageItem.isSent {
            self.timeLabelContainer.backgroundColor = UIColor.loginUserChat
        } else {
            self.timeLabelContainer.backgroundColor = UIColor.red
        }
        
        if !imageItem.isTimeVisible {
            self.stackView.removeArrangedSubview(timeLabelContainer)
            timeLabelContainer.isHidden = true
        } else {
            timeLabelContainer.isHidden = false
            self.stackView.addArrangedSubview(timeLabelContainer)
            
            atTimeLabel.text = imageItem.atTime
        }
        self.contentImage.kf.setImage(with: URL(string: imageItem.userContent),
                                      placeholder: UIImage.imageMessageImage,
                                      options: nil,
                                      progressBlock: nil,
                                      completionHandler: nil)
        
        self.contentImage.isUserInteractionEnabled = true
        self.contentImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onItemClick)))
    }
    
    @objc func onItemClick() {
        super.onClick()
    }
}
