//
//  LoginImageItem.swift
//  MyAppChat
//
//  Created by CPU11814 on 10/1/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

class LoginImageItem: Hashable, ConfigurableItem {
    func hideTimeLabel() {
        isTimeVisible = false
    }
    
    func showTimeLabel() {
        isTimeVisible = true
    }
    
    func hideAvatar() {
        
    }
    
    func showAvatar() {
        
    }
    
    func setIsSent() {
        isSent = true
    }
    
    func changeNewContent(newContent: String) {
        
    }
    
    static func == (lhs: LoginImageItem, rhs: LoginImageItem) -> Bool {
        return lhs.id == rhs.id &&
            lhs.atTime == rhs.atTime &&
            lhs.type == rhs.type &&
            lhs.userContent == rhs.userContent &&
            lhs.userName == rhs.userName &&
            lhs.isTimeVisible == rhs.isTimeVisible &&
            lhs.isSent == rhs.isSent
    }
    
    var hashValue: Int {
        return id.hashValue
    }
    
    let id: String!
    let atTime: String!
    let type: String!
    let userContent: String!
    let userName: String!
    let avatarUrl: String!
    var isTimeVisible: Bool = false
    var isSent: Bool = false
    
    init(id: String!, atTime: String!, type: String!, userContent: String!, userName: String!, avatarUrl: String!) {
        self.id = id
        self.atTime = atTime
        self.type = type
        self.userName = userName
        self.userContent = userContent
        self.avatarUrl = avatarUrl
    }
    
    init(item: LoginImageItem) {
        self.id = item.id
        self.atTime = item.atTime
        self.type = item.type
        self.userContent = item.userContent
        self.userName = item.userName
        self.avatarUrl = item.avatarUrl
        self.isTimeVisible = item.isTimeVisible
    }
}
