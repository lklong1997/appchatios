//
//  OthersImageItem.swift
//  MyAppChat
//
//  Created by CPU11814 on 10/1/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

class OthersImageItem: ConfigurableItem, Hashable {
    var hashValue: Int {
        return id.hashValue
    }
    
    static func == (lhs: OthersImageItem, rhs: OthersImageItem) -> Bool {
        return lhs.id == rhs.id &&
            lhs.atTime == rhs.atTime &&
            lhs.type == rhs.type &&
            lhs.userContent == rhs.userContent &&
            lhs.userName == rhs.userName &&
            lhs.avatarUrl == rhs.avatarUrl &&
            lhs.isTimeVisible == rhs.isTimeVisible &&
            lhs.isAvatarVisible == rhs.isAvatarVisible
    }
    
    func changeNewContent(newContent: String) {
        
    }
    
    func hideTimeLabel() {
        isTimeVisible = false
    }
    
    func showTimeLabel() {
        isTimeVisible = true
    }
    
    func hideAvatar() {
        isAvatarVisible = false
    }
    
    func showAvatar() {
        isAvatarVisible = true
    }
    
    func setIsSent() {
    
    }
    
    let id: String!
    let atTime: String!
    let type: String!
    let userContent: String!
    let userName: String!
    let avatarUrl: String!
    var isTimeVisible = false
    var isAvatarVisible = true

    init(id: String!, atTime: String!, type: String!, userContent: String!, userName: String!, avatarUrl: String!) {
        self.id = id
        self.atTime = atTime
        self.type = type
        self.userName = userName
        self.userContent = userContent
        self.avatarUrl = avatarUrl
    }
    
    init(id: String!, atTime: String!, type: String!, userContent: String!, userName: String!, avatarUrl: String!, isTimeVisible: Bool) {
        self.id = id
        self.atTime = atTime
        self.type = type
        self.userName = userName
        self.userContent = userContent
        self.avatarUrl = avatarUrl
        self.isTimeVisible = isTimeVisible
    }
    
    init(item: OthersImageItem) {
        self.id = item.id
        self.atTime = item.atTime
        self.type = item.type
        self.userName = item.userName
        self.userContent = item.userContent
        self.avatarUrl = item.avatarUrl
        self.isTimeVisible = item.isTimeVisible
    }
}
