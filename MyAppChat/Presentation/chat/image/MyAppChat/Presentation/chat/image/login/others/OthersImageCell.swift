//
//  OthersImageCell.swift
//  MyAppChat
//
//  Created by CPU11814 on 10/1/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import UIKit
import Kingfisher

class OthersImageCell: BaseMessageCell {
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var avatar: UIImageView!
    @IBOutlet private weak var senderLabel: UILabel!
    @IBOutlet private weak var contentImage: UIImageView!
    @IBOutlet private weak var atTimeLabel: UILabel!
    @IBOutlet private weak var stackView: UIStackView!
    @IBOutlet private weak var timeLabelContainer: UIView!
    @IBOutlet private weak var senderLabelContainer: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func setupUI() {
        super.setupUI()
        
        self.avatar.autoSetDimensions(to: CGSize(width: 44.0, height: 44.0))
        self.avatar.autoPinEdge(toSuperviewEdge: .left, withInset: 8.0)
        self.avatar.autoPinEdge(toSuperviewEdge: .top, withInset: 8.0)
        self.avatar.layer.cornerRadius = 22.0
        self.avatar.layer.borderColor = UIColor.lightGray.cgColor
        self.avatar.layer.borderWidth = 1.0
        self.avatar.contentMode = .scaleToFill
        self.avatar.clipsToBounds = true
        
        self.containerView.backgroundColor = UIColor.clear
        self.containerView.autoPinEdge(toSuperviewEdge: .top, withInset: 8.0)
        self.containerView.autoPinEdge(.left, to: .right, of: avatar, withOffset: 8.0)
        self.containerView.autoPinEdge(toSuperviewEdge: .bottom, withInset: 8.0)
        self.containerView.autoPinEdge(.right, to: .right, of: containerView.superview!, withOffset: -64.0, relation: .lessThanOrEqual)
        
        self.stackView.alignment = .leading
        self.stackView.axis = .vertical
        self.stackView.spacing = 8.0
        self.stackView.autoPinEdge(toSuperviewEdge: .top, withInset: 5.0)
        self.stackView.autoPinEdge(toSuperviewEdge: .bottom, withInset: 5.0)
        self.stackView.autoPinEdge(toSuperviewEdge: .left, withInset: 0.0)
        self.stackView.autoPinEdge(toSuperviewEdge: .right, withInset: 5.0)
        
        self.senderLabelContainer.layer.cornerRadius = 8.0
        self.senderLabelContainer.backgroundColor = UIColor.white
        self.senderLabelContainer.clipsToBounds = true
        
        self.senderLabel.autoPinEdge(toSuperviewEdge: .top, withInset: 5.0)
        self.senderLabel.autoPinEdge(toSuperviewEdge: .bottom, withInset: 5.0)
        self.senderLabel.autoPinEdge(toSuperviewEdge: .left, withInset: 8.0)
        self.senderLabel.autoPinEdge(toSuperviewEdge: .right, withInset: 8.0)
        self.senderLabel.numberOfLines = 1
        self.senderLabel.font = UIFont.systemFont(ofSize: 17.0)
        self.senderLabel.textColor = UIColor.purple
        
        self.contentImage.autoSetDimensions(to: CGSize(width: 128.0, height: 128.0))
        self.contentImage.layer.cornerRadius = 16.0
        self.contentImage.layer.borderWidth = 0.0
        self.contentImage.contentMode = .scaleAspectFill
        self.contentImage.clipsToBounds = true
        
        self.timeLabelContainer.layer.cornerRadius = 8.0
        self.timeLabelContainer.backgroundColor = UIColor.lightGray
        self.timeLabelContainer.clipsToBounds = true
        
        self.atTimeLabel.textAlignment = .right
        self.atTimeLabel.font = UIFont.systemFont(ofSize: 11.0)
        self.atTimeLabel.textColor = UIColor.white
        self.atTimeLabel.autoPinEdge(toSuperviewEdge: .top, withInset: 5.0)
        self.atTimeLabel.autoPinEdge(toSuperviewEdge: .bottom, withInset: 5.0)
        self.atTimeLabel.autoPinEdge(toSuperviewEdge: .left, withInset: 5.0)
        self.atTimeLabel.autoPinEdge(toSuperviewEdge: .right, withInset: 5.0)
    }
    
    override func bind(item: MessageItem, listener: Listener) {
        super.bind(item: item, listener: listener)
        
        let imageItem = item as! MessageImageItem
        if imageItem.isAvatarVisible {
            self.avatar.layer.borderColor = UIColor.lightGray.cgColor
            let processor = BlurImageProcessor(blurRadius: 4) >> RoundCornerImageProcessor(cornerRadius: 20)
            self.avatar.kf.setImage(with: URL(string: imageItem.avatarUrl),
                                    placeholder: UIImage.imageAvatar,
                                    options: [.processor(processor)],
                                    progressBlock: nil,
                                    completionHandler: { (image, error, cacheType, imageURL) in
                                        
            })
        }
        else {
            self.avatar.layer.borderColor = UIColor.clear.cgColor
            self.avatar.image = nil
        }
        
        self.senderLabel.text = imageItem.userName
        
        self.contentImage.kf.setImage(with: URL(string: imageItem.userContent),
                                      placeholder: UIImage.imageMessageImage,
                                      options: nil,
                                      progressBlock: nil,
                                      completionHandler: nil)
        self.contentImage.isUserInteractionEnabled = true
        self.contentImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onItemClick)))
        
        if !imageItem.isTimeVisible {
            self.stackView.removeArrangedSubview(timeLabelContainer)
            self.timeLabelContainer.isHidden = true
            
        } else {
            self.timeLabelContainer.isHidden = false
            self.stackView.addArrangedSubview(timeLabelContainer)
            
            self.atTimeLabel.text = imageItem.atTime
        }
    }
    
    @objc func onItemClick() {
        super.onClick()
    }
}
