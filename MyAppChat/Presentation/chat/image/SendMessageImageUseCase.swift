//
//  SendMessageImageUseCase.swift
//  MyAppChat
//
//  Created by CPU11814 on 10/2/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift

class SendMessageImageUseCase: UseCase {
    private let repository: MessageRepository = MessageRepositoryFactory.sharedInstance
    
    public typealias Request = SendMessageImageRequest
    public typealias Response = Message
    
    func execute(request: SendMessageImageRequest) -> Observable<Message> {
        return repository.sendMessageImage(request: request)
    }
}
