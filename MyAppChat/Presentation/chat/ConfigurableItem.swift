//
//  ConfigurableItem.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/21/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

protocol ConfigurableItem {
    func hideTimeLabel()
    func showTimeLabel()
    func hideAvatar()
    func showAvatar()
    func setIsSent()
    func changeNewContent(newContent: String)
}
