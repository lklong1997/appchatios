//
//  LoginAudioCell.swift
//  MyAppChat
//
//  Created by CPU11814 on 11/8/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import PureLayout
import UIKit
import Gifu

class LoginAudioCell: BaseMessageCell {
    @IBOutlet private weak var containerView: UIView!
    
    @IBOutlet private weak var stackView: UIStackView!
    @IBOutlet private weak var audioContainer: UIView!
    @IBOutlet private weak var buttonPlayStop: UIButton!
    @IBOutlet private weak var gif: GIFImageView!
    @IBOutlet private weak var duration: UILabel!
    
    @IBOutlet private weak var timeLabelContainer: UIView!
    @IBOutlet private weak var atTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setupUI() {
        super.setupUI()
        
        self.containerView.backgroundColor = UIColor.clear
        self.containerView.autoPinEdge(toSuperviewEdge: .top, withInset: 8.0)
        self.containerView.autoPinEdge(toSuperviewEdge: .right, withInset: 8.0)
        self.containerView.autoPinEdge(toSuperviewEdge: .bottom, withInset: 8.0)
        self.containerView.autoPinEdge(.left, to: .left, of: containerView.superview!, withOffset: 64.0, relation: .greaterThanOrEqual)
        
        self.stackView.alignment = .leading
        self.stackView.axis = .vertical
        self.stackView.spacing = 8.0
        self.stackView.autoPinEdge(toSuperviewEdge: .top, withInset: 5.0)
        self.stackView.autoPinEdge(toSuperviewEdge: .bottom, withInset: 5.0)
        self.stackView.autoPinEdge(toSuperviewEdge: .left, withInset: 5.0)
        self.stackView.autoPinEdge(toSuperviewEdge: .right, withInset: 0.0)
        
        self.audioContainer.layer.cornerRadius = 8.0
        self.audioContainer.clipsToBounds = true
        
        self.gif.autoSetDimensions(to: CGSize(width: 64.0, height: 24.0))
        self.gif.autoPinEdge(toSuperviewEdge: .top, withInset: 5.0)
        self.gif.autoPinEdge(toSuperviewEdge: .bottom, withInset: 5.0)
        self.gif.layer.cornerRadius = 8.0
        self.gif.clipsToBounds = true
        self.gif.contentMode = .scaleAspectFill
        self.gif.prepareForAnimation(withGIFData: NSDataAsset(name: "Soundwave")!.data) {
        }
        
        self.buttonPlayStop.autoSetDimensions(to: CGSize(width: 44.0, height: 44.0))
        self.buttonPlayStop.setImage(UIImage.imagePlay, for: .normal)
        self.buttonPlayStop.autoPinEdge(toSuperviewEdge: .left, withInset: 5.0)
        self.buttonPlayStop.autoAlignAxis(.horizontal, toSameAxisOf: self.gif)
        self.buttonPlayStop.isUserInteractionEnabled = true
        self.buttonPlayStop.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector(handleTap)))
        
        self.duration.autoSetDimensions(to: CGSize(width: 32.0, height: 32.0))
        self.duration.autoPinEdge(toSuperviewEdge: .right, withInset: 5.0)
        self.duration.autoAlignAxis(.horizontal, toSameAxisOf: self.gif)
        self.duration.numberOfLines = 1
        self.duration.font = UIFont.systemFont(ofSize: 14.0)
        self.duration.textColor = UIColor.textFieldBlue
        self.duration.textAlignment = .center
        
        self.gif.autoPinEdge(.left, to: .right, of: self.buttonPlayStop, withOffset: 5.0)
        self.gif.autoPinEdge(.right, to: .left, of: self.duration, withOffset: -8.0)
        
        self.timeLabelContainer.backgroundColor = UIColor.loginUserChat
        self.timeLabelContainer.layer.cornerRadius = 8.0
        self.timeLabelContainer.layer.borderColor = UIColor.lightGray.cgColor
        self.timeLabelContainer.layer.borderWidth = 1.0
        self.timeLabelContainer.clipsToBounds = true
        
        self.atTime.textAlignment = .right
        self.atTime.font = UIFont.systemFont(ofSize: 11.0)
        self.atTime.textColor = UIColor.lightGray
        self.atTime.autoPinEdge(toSuperviewEdge: .top, withInset: 5.0)
        self.atTime.autoPinEdge(toSuperviewEdge: .bottom, withInset: 5.0)
        self.atTime.autoPinEdge(toSuperviewEdge: .left, withInset: 5.0)
        self.atTime.autoPinEdge(toSuperviewEdge: .right, withInset: 5.0)
    }
    
    override func bind(item: MessageItem, listener: Listener) {
        super.bind(item: item, listener: listener)
        
        let audioItem = item as! MessageAudioItem
        
        if audioItem.isSent {
            self.timeLabelContainer.backgroundColor = UIColor.loginUserChat
        } else {
            self.timeLabelContainer.backgroundColor = UIColor.red
        }
        
        if !audioItem.isTimeVisible {
            self.stackView.removeArrangedSubview(timeLabelContainer)
            timeLabelContainer.isHidden = true
        } else {
            timeLabelContainer.isHidden = false
            self.stackView.addArrangedSubview(timeLabelContainer)
            
            atTime.text = audioItem.atTime
        }
        
        self.duration.text = audioItem.duration
    }
    
    @objc func handleTap() {
        super.onClick()
    }
    
    func startPlaying() {
        self.buttonPlayStop.setImage(UIImage.imagePause, for: .normal)
        self.gif.startAnimatingGIF()
    }
    
    func stopPlaying() {
        self.buttonPlayStop.setImage(UIImage.imagePlay, for: .normal)
        self.gif.stopAnimatingGIF()
    }
}
