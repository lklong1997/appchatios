//
//  ConversationItem.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/17/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

/* This class is used to move from ContactViewController or ConversationViewController to ChatViewController
    It wraps necessary information needed to update on firebase */
class ConversationItemNavigation {
    let conversationID: String!
    let userIDList : [String: String]!
    init(conversationID: String, userIDList: [String : String]) {
        self.conversationID = conversationID
        self.userIDList = userIDList
    }
}
