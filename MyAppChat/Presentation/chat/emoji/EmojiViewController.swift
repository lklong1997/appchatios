//
//  EmojiViewController.swift
//  MyAppChat
//
//  Created by CPU11814 on 10/30/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import UIKit
import PureLayout

private let reuseIdentifier = "cell"

class EmojiViewController:  UICollectionViewController {
    private var emojiIcon = [String]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.collectionView.frame.size = CGSize(width: 128.0, height: 128.0)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        initEmoji()
    }
    
    private func initEmoji() {
        self.emojiIcon.append("a")
        self.emojiIcon.append("ab")
        self.emojiIcon.append("ac")
        self.emojiIcon.append("ad")
        self.emojiIcon.append("ae")
        self.emojiIcon.append("af")
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.emojiIcon.count

    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! EmojiCell
        
        cell.bind(emoji: self.emojiIcon[indexPath.row])
        
        return cell
    }

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
