//
//  EmojiCell.swift
//  MyAppChat
//
//  Created by CPU11814 on 10/30/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import UIKit
import PureLayout

class EmojiCell: UICollectionViewCell {
    @IBOutlet private weak var label: UILabel!
    
    private var emoji: String!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupUI()
    }
    
    private func setupUI() {
        self.superview?.autoSetDimensions(to: CGSize(width: 44.0, height: 44.0))
        
        self.label.autoPinEdgesToSuperviewEdges()
        self.label.textAlignment = .center
    }
    
    public func bind(emoji: String) {
        self.emoji = emoji
        
        self.label.text = emoji
    }
}
