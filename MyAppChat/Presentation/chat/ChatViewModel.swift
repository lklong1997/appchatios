//
//  ChatViewModel.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/7/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import Photos
import FirebaseStorage
import RxSwift
import RxCocoa
import DeepDiff

final class ChatViewModel: ViewModelDelegate {
    private let disposeBag = DisposeBag()
    private weak var displayLogic: ChatDisplayLogic!
    private let chatField = BehaviorRelay<String>(value: "")
    
    private var conversationItemNavigation: ConversationItemNavigation!
    private var loginUser: User!
    
    private let loadNewMessageUseCase = LoadNewMessageUseCase()
    private let loadMessageUseCase = LoadMessageUseCase()
    private let sendMessageUseCase = SendMessageUseCase()
    private let sendMessageImageUseCase = SendMessageImageUseCase()
    private let getUnSentMessagesUseCase = GetUnSentMessagesUseCase()
    private let sendMessageFileUseCase = SendMessageFileUseCase()
    private let downloadFileUseCase = DownloadFileUseCase()
    private let sendLocationUseCase = SendLocationUseCase()
    
    private let textType = FirebaseInjection.textType
    private let imageType = FirebaseInjection.imageType
    private let videoType = FirebaseInjection.videoType
    private let audioType = FirebaseInjection.audioType
    private let fileType = FirebaseInjection.fileType
    private let locationType = FirebaseInjection.locationType
    
    private var baseMessages = [MessageItem]()
    private var sendingMessageIDs = [String]()
    private let dateFormatter = DateFormatter()
    
    // These are used to receive a message from the repository (for showing the message while sending data to firebase)
    private let localImagePublishSubject = PublishSubject<Message>()
    private let localFilePublishSubject = PublishSubject<Message>()
    private let locationSnapshotPublishSubject = PublishSubject<Message>()
    
    private var fetchedMessageIDs = [String]()
    private var handledMessageID = [String]()
    private var unSentMessages = [Message]()
    private let unSentMessagesPublishSubject = PublishSubject<[Message]>()
    
    private let snapshotKey = "Snapshot"
    private let locationKey = "Location"
    private let nameKey = "Name"
    
    init(displayLogic: ChatDisplayLogic, conversationItemNavigation: ConversationItemNavigation, loginUser: User) {
        self.displayLogic = displayLogic
        self.conversationItemNavigation = conversationItemNavigation
        self.loginUser = loginUser
        self.dateFormatter.dateFormat = "HH:mm"
    }
    
    func transform(input: Input) -> Output {
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        
        (input.chatField <-> self.chatField)
            .disposed(by: disposeBag)
        
        // Fetch messages at beginning
        input.trigger
            .flatMap { [unowned self] (_) -> Driver<[Message]> in
                return Observable.deferred { [unowned self] in
                    return Observable.just(LoadMessagesRequest(conversationItem: self.conversationItemNavigation,
                                                               loginUser: self.loginUser,
                                                               unSentMessagesPublishSubject: self.unSentMessagesPublishSubject))
                    }
                    .flatMap { [unowned self] request -> Observable<[Message]> in
                        return self.loadMessageUseCase
                            .execute(request: request)
                    }
                    .do(onNext: { [unowned self] messages in
                        if (self.baseMessages.isEmpty) {
                            // messages is the array of SENT messages with latest message at the beginning
                            
                            DispatchQueue.main.async {
                                messages.forEach { [unowned self] message in
                                    let messageItem = self.convertToMessageItem(message: message, loginUser: self.loginUser)
                                    messageItem.setIsSent()
                                    
                                    self.baseMessages.append(messageItem)
                                    self.fetchedMessageIDs.append(message.id)
                                }
                                
                                for index in stride(from: self.unSentMessages.count - 1, through: 0, by: -1) {
                                    let messageItem = self.convertToMessageItem(message: self.unSentMessages[index], loginUser: self.loginUser)
                                    
                                    self.baseMessages.insert(messageItem, at: 0)
                                    self.fetchedMessageIDs.append(self.unSentMessages[index].id)
                                }
                                
                                self.calculateTimeAvatarAppearance()
                                self.displayLogic.update(newItems: self.baseMessages)
                            }
                        }
                    })
                    .trackError(errorTracker)
                    .asDriverOnErrorJustComplete()
                
            }
            .drive()
            .disposed(by: disposeBag)
        
        input.emojiPublishSubject
            .subscribe({ [unowned self] emoji in
                let currentString = self.chatField.value + emoji.element!
                self.chatField.accept(currentString)
            })
            .disposed(by: disposeBag)
        
        self.unSentMessagesPublishSubject
            .subscribe({ [unowned self] messagesEvent in
                self.unSentMessages.removeAll()
                self.unSentMessages.append(contentsOf: messagesEvent.element!)
            })
            .disposed(by: disposeBag)
        
        //        input.reSendPublishSubject
        //            .subscribe({ [unowned self] resend in
        //                Observable.deferred {
        //                    Observable.just(ReSendMessageRequest())
        //                }
        //
        //            })
        //            .disposed(by: disposeBag)
        
        input.sendTrigger
            .flatMap { [unowned self] () -> Driver<Message> in
                return Observable.deferred { [unowned self] in
                    guard !self.chatField.value.isEmpty else {
                        return Observable.error(ValidateError(message: ""))
                    }
                    
                    self.displayLogic.closeEmojiCollection()
                    self.displayLogic.emptyChatField()
                    let value = self.chatField.value
                    self.chatField.accept("")
                    
                    return Observable.just(SendMessageRequest(sender: self.loginUser.name,
                                                              content: value,
                                                              atTime: CLong(NSDate().timeIntervalSince1970),
                                                              type: self.textType,
                                                              conversationID: self.conversationItemNavigation.conversationID,
                                                              userIDList: self.conversationItemNavigation.userIDList,
                                                              nameOfContent: nil))
                    
                    }
                    .flatMap { [unowned self] request -> Observable<Message> in
                        return self.sendMessageUseCase
                            .execute(request: request)
                    }
                    .do(onNext: { [unowned self] message in
                        // This message is already on firebase (It is already on UITableView)
                        // Change the cell of this message from "isSending" to "isSent"
                        self.handleSentMessage(message: message, shouldUpdateContent: false)
                    })
                    .asDriverOnErrorJustComplete()
            }
            .drive()
            .disposed(by: disposeBag)
        
        // Attach a listener for new message after fetching
        input.publishSubject
            .subscribe({ [unowned self] hasDoneFetching in
                Observable.deferred {
                    Observable.just(LoadNewMessageRequest(conversationItem: self.conversationItemNavigation, loginUser: self.loginUser))
                    }
                    .flatMap { [unowned self] request -> Observable<Message> in
                        self.loadNewMessageUseCase
                            .execute(request: request)                        
                    }
                    .do(onNext: { [unowned self] message in
                        // If this is an image-type message, we have handled it!
                        // Or if this message exists ( after first time fetching, do nothing)
                        if !self.handledMessageID.contains(message.id)  && !self.fetchedMessageIDs.contains(message.id){
                            let messageItem = self.convertToMessageItem(message: message, loginUser: self.loginUser)
                            
                            // When childAdded is called, if the message is sent by other users, it is definitely on firebase.
                            if messageItem.userName != self.loginUser.name {
                                messageItem.setIsSent()
                            }
                            self.baseMessages.insert(messageItem, at: 0)
                            self.calculateTimeLabelAppearance()
                            self.calculateAvatarAppearance()
                            self.displayLogic.updateTableView(newItems: self.baseMessages)
                        }
                    })
                    .asDriverOnErrorJustComplete()
                    .drive()
                    .disposed(by: self.disposeBag)
            })
            .disposed(by: disposeBag)
        
        // After picking an image, process that image and send a new image-type message
        input.pickedImagePublishSubject
            .subscribe({ [unowned self] pickedImageInfo in
                Observable.deferred {
                    let imageFilePath = self.handlePickedImage(imageInfo: pickedImageInfo.element!)
                    
                    return Observable.just(SendMessageImageRequest(sender: self.loginUser.name,
                                                                   content: imageFilePath,
                                                                   atTime: CLong(NSDate().timeIntervalSince1970),
                                                                   type: self.imageType,
                                                                   conversationID: self.conversationItemNavigation.conversationID,
                                                                   userIDList: self.conversationItemNavigation.userIDList,
                                                                   nameOfContent: nil,
                                                                   localImagePublishSubject: self.localImagePublishSubject))
                    }
                    .flatMap { [unowned self] request -> Observable<Message> in
                        return self.sendMessageImageUseCase
                            .execute(request: request)
                    }
                    .do(onNext: { [unowned self] message in
                        self.handleSentMessage(message: message, shouldUpdateContent: false)
                    })
                    .asDriverOnErrorJustComplete()
                    .drive()
                    .disposed(by: self.disposeBag)
            })
            .disposed(by: disposeBag)
        
        // Display a message image with local image path, this will be called inside pickedImagePublishSubject
        self.localImagePublishSubject
            .subscribe({ [unowned self] localImageMessage in
                if let message = localImageMessage.element {
                    self.handledMessageID.append(message.id)
                    
                    let messageItem = self.convertToMessageItem(message: message, loginUser: self.loginUser)
                    self.baseMessages.insert(messageItem, at: 0)
                    self.calculateTimeLabelAppearance()
                    self.calculateAvatarAppearance()
                    self.displayLogic.updateTableView(newItems: self.baseMessages)
                }
            })
            .disposed(by: disposeBag)
        
        // After picking a file, send a new file-type message
        input.pickedFilePublishSubject
            .subscribe({ [unowned self] pickedFileInfo in
                Observable.deferred {
                    let filePath = pickedFileInfo.element![Converter.convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.imageURL)] as! URL
                    
                    return Observable.just(SendMessageFileRequest(sender: self.loginUser.name,
                                                                  content: filePath.absoluteString,
                                                                  atTime: CLong(NSDate().timeIntervalSince1970),
                                                                  type: self.fileType,
                                                                  conversationID: self.conversationItemNavigation.conversationID,
                                                                  userIDList: self.conversationItemNavigation.userIDList,
                                                                  nameOfContent: filePath.lastPathComponent,
                                                                  localFilePublishSubject: self.localFilePublishSubject))
                    }
                    .flatMap { [unowned self] request -> Observable<Message> in
                        return self.sendMessageFileUseCase
                            .execute(request: request)
                    }
                    .do(onNext: { [unowned self] message in
                        self.handleSentMessage(message: message, shouldUpdateContent: true)
                    })
                    .asDriverOnErrorJustComplete()
                    .drive()
                    .disposed(by: self.disposeBag)
            })
            .disposed(by: disposeBag)
        
        // Display a message file before uploading the file to database
        self.localFilePublishSubject
            .subscribe({ [unowned self] localFileMessage in
                if let message = localFileMessage.element {
                    self.handledMessageID.append(message.id)
                    
                    let messageItem = self.convertToMessageItem(message: message, loginUser: self.loginUser)
                    self.baseMessages.insert(messageItem, at: 0)
                    self.calculateTimeLabelAppearance()
                    self.calculateAvatarAppearance()
                    self.displayLogic.updateTableView(newItems: self.baseMessages)
                }
            })
            .disposed(by: disposeBag)
        
        input.downloadFilePublishSubject
            .subscribe({ [unowned self] messageItemEvent in
                if let messageItem = messageItemEvent.element {
                    if let downloadString = messageItem.userContent {
                        Observable.deferred{
                            return Observable.just(DownloadFileRequest(url: downloadString))
                            }
                            .flatMap { [unowned self] request -> Observable<FileDownloadWrapper> in
                                return self.downloadFileUseCase
                                    .execute(request: request)
                            }
                            .do(onNext: { [unowned self] wrapper in
                                wrapper.downloadTask.observe(.progress, handler: { snapshot in
                                    let percentComplete = Double(snapshot.progress!.completedUnitCount) / Double(snapshot.progress!.totalUnitCount)
                                    let rounded = round(100 * percentComplete) / 100
                                    let roundedFloat = CGFloat(rounded)
                                    DispatchQueue.main.async {
                                        self.displayLogic.updateDownloadFileProgress(percent: roundedFloat, messageID: messageItem.id)
                                    }
                                })
                                wrapper.downloadTask.observe(.success, handler: {[unowned self] snapshot in
                                    DispatchQueue.main.async {
                                        self.displayLogic.downloadFileSuccessfully(url: wrapper.filePath)
                                    }
                                })
                            })
                            .asDriverOnErrorJustComplete()
                            .drive()
                            .disposed(by: self.disposeBag)
                    }
                }
            })
            .disposed(by: disposeBag)
        
        input.sendLocationPublishSubject
            .subscribe({ [unowned self] dictionaryEvent in
                if let dictionary = dictionaryEvent.element {
                    Observable.deferred { [unowned self] in
                        let snapshot = dictionary[self.snapshotKey] as! UIImage
                        let location = dictionary[self.locationKey] as! CLLocationCoordinate2D
                        let name = dictionary[self.nameKey] as! String
                        
                        let snapshotFilePath = self.handleLocationSnapshot(snapshot: snapshot)
                        
                        return Observable.just(SendLocationRequest(
                            sender: self.loginUser.name,
                            content: snapshotFilePath,
                            atTime: CLong(NSDate().timeIntervalSince1970),
                            type: self.locationType,
                            conversationID: self.conversationItemNavigation.conversationID,
                            userIDList: self.conversationItemNavigation.userIDList,
                            nameOfContent: name + "#" + String(location.latitude) + "#" + String(location.longitude),
                            locationSnapshotPublishSubject: self.locationSnapshotPublishSubject))
                        }
                        .flatMap {[unowned self] request -> Observable<Message> in
                            return self.sendLocationUseCase
                                .execute(request: request)
                        }
                        .do(onNext: { [unowned self] message in
                            self.handleSentMessage(message: message, shouldUpdateContent: false)
                        })
                        .asDriverOnErrorJustComplete()
                        .drive()
                        .disposed(by: self.disposeBag)
                }
            })
            .disposed(by: disposeBag)
        
        // Display a message location with local snapshot path, this will be called inside sendLocationPublishSubject
        self.locationSnapshotPublishSubject
            .subscribe({ [unowned self] localSnapshotMessage in
                if let message = localSnapshotMessage.element {
                    self.handledMessageID.append(message.id)
                    
                    let messageItem = self.convertToMessageItem(message: message, loginUser: self.loginUser)
                    self.baseMessages.insert(messageItem, at: 0)
                    self.calculateTimeLabelAppearance()
                    self.calculateAvatarAppearance()
                    self.displayLogic.updateTableView(newItems: self.baseMessages)
                }
            })
            .disposed(by: disposeBag)
        
        
        return Output(fetching: activityIndicator.asDriver(),
                      error: errorTracker.asDriver())
    }
    
    private func convertToMessageItem(message: Message, loginUser: User) -> MessageItem {
        let sender = message.userName
        let avatarURL = self.conversationItemNavigation.userIDList[sender!]
        
        let timeInterval = TimeInterval(message.atTime)
        let time = Date(timeIntervalSince1970: timeInterval)
        let atTimeString = dateFormatter.string(from: time)
        
        switch message.type {
        case self.textType:
            if sender == loginUser.name {
                let item = MessageTextItem(messageItemType: MessageItemType.loginText, id: message.id, atTime: atTimeString, type: message.type, userContent: message.userContent, userName: sender, avatarUrl: avatarURL, isTimeVisible: false)
                return item
            } else {
                let item = MessageTextItem(messageItemType: MessageItemType.othersText, id: message.id, atTime: atTimeString, type: message.type, userContent: message.userContent, userName: sender, avatarUrl: avatarURL, isTimeVisible: false)
                return item
            }
            
        case self.imageType:
            let handledImagePath = self.handleImagePath(messageID: message.id, currentImagePath: message.userContent)
            
            if sender == loginUser.name {
                let item = MessageImageItem(messageItemType: MessageItemType.loginImage, id: message.id, atTime: atTimeString, type: message.type, userContent: handledImagePath, userName: sender, avatarUrl: avatarURL, isTimeVisible: false)
                return item
            } else {
                let item = MessageImageItem(messageItemType: MessageItemType.othersImage, id: message.id, atTime: atTimeString, type: message.type, userContent: handledImagePath, userName: sender, avatarUrl: avatarURL, isTimeVisible: false)
                return item
            }
        case self.fileType:
            if sender == loginUser.name {
                let item = MessageFileItem(messageItemType: MessageItemType.loginFile, id: message.id, atTime: atTimeString, type: message.type, userContent: message.userContent, userName: sender, avatarUrl: avatarURL, isTimeVisible: false, nameOfFile: message.nameOfContent!)
                return item
            } else {
                let item = MessageFileItem(messageItemType: MessageItemType.othersFile, id: message.id, atTime: atTimeString, type: message.type, userContent: message.userContent, userName: sender, avatarUrl: avatarURL, isTimeVisible: false, nameOfFile: message.nameOfContent!)
                return item
            }
        case self.locationType:
            let handledImagePath = self.handleImagePath(messageID: message.id, currentImagePath: message.userContent)
            let components = message.nameOfContent!.components(separatedBy: "#")
            var address = ""
            var lat = ""
            var long = ""
            
            if components.count == 3 { // Have address
                address = components[0]
                lat = components[1]
                long = components[2]
            } else if components.count == 2 { // only lat and long
                lat = components[0]
                long = components[1]
            }
            
            if sender == loginUser.name {
                let item = MessageLocationItem(messageItemType: MessageItemType.loginLocation, id: message.id, atTime: atTimeString, type: message.type, userContent: handledImagePath, userName: sender, avatarUrl: avatarURL, isTimeVisible: false, address: address, latitude: lat, longitude: long)
                return item
            }
            else {
                let item = MessageLocationItem(messageItemType: MessageItemType.othersLocation, id: message.id, atTime: atTimeString, type: message.type, userContent: handledImagePath, userName: sender, avatarUrl: avatarURL, isTimeVisible: false, address: address, latitude: lat, longitude: long)
                return item
            }
        case self.audioType:
            let duration = message.nameOfContent!
            
            if sender == loginUser.name {
                let item = MessageAudioItem(messageItemType: MessageItemType.loginAudio, id: message.id, atTime: atTimeString, type: message.type, userContent: message.userContent, userName: sender, avatarUrl: avatarURL, isTimeVisible: false, duration: duration)
                return item
            } else {
                let item = MessageAudioItem(messageItemType: MessageItemType.othersAudio, id: message.id, atTime: atTimeString, type: message.type, userContent: message.userContent, userName: sender, avatarUrl: avatarURL, isTimeVisible: false, duration: duration)
                return item
            }
        case self.videoType:
            if sender == loginUser.name {                
                let item = MessageVideoItem(messageItemType: MessageItemType.loginVideo, id: message.id, atTime: atTimeString, type: message.type, userContent: message.userContent, userName: sender, avatarUrl: avatarURL, isTimeVisible: false)
                return item
            } else {
                let item = MessageVideoItem(messageItemType: MessageItemType.othersVideo, id: message.id, atTime: atTimeString, type: message.type, userContent: message.userContent, userName: sender, avatarUrl: avatarURL, isTimeVisible: false)
                return item
            }

        default:
            fatalError("Unhandled message type.")
        }
    }
    
    // Check if there exists a local image with name is the messageID
    private func handleImagePath(messageID: String, currentImagePath: String) -> String {
        let fileManager = FileManager()
        let documentPath = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first
        
        let localImagePath = documentPath?.appendingPathComponent(messageID + ".jpg")
        if let localImagePathString = localImagePath?.path {
            if fileManager.fileExists(atPath: localImagePathString) {
                return localImagePath!.absoluteString
            }
        }
        return currentImagePath
    }
    
    private func calculateTimeLabelAppearance() {
        baseMessages[0].showTimeLabel()
        
        if baseMessages.count > 1 {
            if baseMessages[0].userName != baseMessages[1].userName {
                baseMessages[1].showTimeLabel()
            }
            else {
                baseMessages[1].hideTimeLabel()
            }
        }
    }
    
    private func calculateAvatarAppearance() {
        if baseMessages.count > 1 {
            if baseMessages[0].userName != baseMessages[1].userName {
                baseMessages[0].showAvatar()
            }
            else {
                baseMessages[0].hideAvatar()
            }
        }
        else {
            baseMessages[0].showAvatar()
        }
    }
    
    private func calculateTimeAvatarAppearance() {
        for index in stride(from: baseMessages.count - 1, through: 0, by: -1) {
            if index < baseMessages.count-1 {
                if baseMessages[index].userName != baseMessages[index + 1].userName {
                    baseMessages[index].showTimeLabel()
                    baseMessages[index].showAvatar()
                    
                    baseMessages[index + 1].showTimeLabel()
                } else {
                    baseMessages[index].showTimeLabel()
                    baseMessages[index].hideAvatar()
                    
                    baseMessages[index + 1].hideTimeLabel()
                }
            } else {
                // This is the last message in our array (at the top of UITableView)
                baseMessages[index].showAvatar()
                baseMessages[index].showTimeLabel()
            }
        }
    }
    
    // Change isSent of this message in this view model, then do the same for the view controller (2 arrays in vm and vc)
    private func handleSentMessage(message: Message, shouldUpdateContent: Bool) {
        let index = self.baseMessages.firstIndex(where: { messageItem in
            messageItem.id == message.id
        })
        self.baseMessages[index!].setIsSent()
        
        var newContent: String = ""
        if shouldUpdateContent {
            newContent = message.userContent
            self.baseMessages[index!].changeNewContent(newContent: newContent)
        }
        
        self.displayLogic.updateSentMessage(newItems: self.baseMessages, indexOfSentMessage: index!, newContent: newContent)
    }
    
    private func handlePickedImage(imageInfo: [String: Any]) -> String {
        let fileManager = FileManager()
        let documentPath = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first
        let imagePath = documentPath?.appendingPathComponent(String(CLong(NSDate().timeIntervalSince1970)) + ".jpg")
        if let pickedImage = imageInfo[Converter.convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            let resizedImage = pickedImage.af_imageAspectScaled(toFill: CGSize(width: 500.0, height: 500.0))
            
            try! resizedImage.jpegData(compressionQuality: 0.8)?.write(to: imagePath!)                        
            
            return String(imagePath!.absoluteString)
        }
        else {
            return ""
        }
    }
    
    private func handleLocationSnapshot(snapshot: UIImage) -> String {
        let fileManager = FileManager()
        let documentPath = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first
        let snapshotPath = documentPath?.appendingPathComponent(String(CLong(NSDate().timeIntervalSince1970)) + ".jpg")
        let resizedSnapshot = snapshot.af_imageAspectScaled(toFill: CGSize(width: 200.0, height: 150.0))
        
        try! resizedSnapshot.jpegData(compressionQuality: 0.8)?.write(to: snapshotPath!)
        
        return String(snapshotPath!.absoluteString)
        
    }
}
extension ChatViewModel {
    public struct Input {
        let trigger: Driver<Void>
        let sendTrigger: Driver<Void>
        let chatField: ControlProperty<String>
        let publishSubject: PublishSubject<Bool>
        let pickedImagePublishSubject: PublishSubject<[String: Any]>
        let pickedFilePublishSubject: PublishSubject<[String: Any]>
        let reSendPublishSubject: PublishSubject<Bool>
        let emojiPublishSubject: PublishSubject<String>
        let downloadFilePublishSubject: PublishSubject<MessageItem>
        let sendLocationPublishSubject: PublishSubject<[String: Any]>
    }
    
    public struct Output {
        let fetching: Driver<Bool>
        let error : Driver<Error>
    }
}
