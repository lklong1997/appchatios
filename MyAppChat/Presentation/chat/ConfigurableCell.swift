//
//  ConfigurableCell.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/20/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import Foundation

protocol ConfigurableCell {
    associatedtype Item
    func bind(item: Item, listener: Listener)
}
