//
//  BaseViewController.swift
//  MyAppChat
//
//  Created by CPU11814 on 8/29/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import UIKit
import MBProgressHUD
import Toast

class BaseViewController: UIViewController {
    open func showToast(message: String) {
        self.view.makeToast(message, duration: 3.0, position: CSToastPositionCenter)
    }
    
    open func showLoading(withStatus show: Bool) {
        if show {
            let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
            hud.mode = .indeterminate
            hud.label.text = "Loading"
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    
    open func showError(error: Error) {
        if let validateError = error as? ValidateError {
            self.showToast(message: validateError.message)
        }
        else {
            self.showToast(message: error.localizedDescription)
        }
    }

}
