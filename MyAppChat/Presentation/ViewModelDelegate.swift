//
//  ViewModelDelegate.swift
//  MyAppChat
//
//  Created by CPU11814 on 8/29/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//


protocol ViewModelDelegate {
    associatedtype Input
    associatedtype Output
    
    func transform(input: Input) -> Output
}
