//
//  LoadContactsUseCase.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/6/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift

class LoadContactsUseCase: UseCase {
    private let repository : UserRepository = UserRepositoryFactory.sharedInstance
    
    public typealias Request = LoadContactRequest
    public typealias Response = [User]
    
    public func execute(request: LoadContactRequest) -> Observable<[User]> {
        return repository.loadContacts(request: request)
    }
}
