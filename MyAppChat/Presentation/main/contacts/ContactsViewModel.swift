//
//  ContactsViewModel.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/5/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift
import RxCocoa


class ContactsViewModel: ViewModelDelegate {
    private let disposeBag = DisposeBag()
    private weak var displayLogic: ContactsDisplayLogic!
    private var loginUser: User!
    private let items = BehaviorRelay<[ContactItem]>(value: [])
    private let loadContactsUseCase = LoadContactsUseCase()
    private let contactClickUseCase = ContactClickUseCase()
    private var didMoveToChat = false
    
    init(displayLogic: ContactsDisplayLogic, user: User) {
        self.displayLogic = displayLogic
        self.loginUser = user
    }
    
    
    func transform(input: Input) -> Output {
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        
        input.trigger
            .flatMap { [unowned self] (_) -> Driver<[User]> in
                return Observable.deferred { [unowned self] in
                    return Observable.just(LoadContactRequest(user: self.loginUser))
                    }
                    .flatMap { [unowned self] (request) -> Observable<[User]> in
                        return self.loadContactsUseCase
                            .execute(request: request)
                            .do(onNext: { (contacts) in
                                self.didMoveToChat = false
                                
                                self.items.accept(contacts.map{ (contact) in
                                    return ContactItem(avatar: contact.avatarUrl ?? "",
                                                       contactName: contact.name,
                                                       conversationIDList: contact.conversationIDList)
                                })
                            })
                        
                    }
                    .trackActivity(activityIndicator)
                    .trackError(errorTracker)
                    .asDriverOnErrorJustComplete()
            }
            .drive()
            .disposed(by: disposeBag)
        
        input.publishSubject    
            .subscribe({contact in
                Observable.deferred {
                    Observable.just(ContactClickRequest(loginUser: self.loginUser, contact: contact.element!))
                    }
                    .flatMap { [unowned self] (request) -> Observable<ConversationItemNavigation> in
                        self.contactClickUseCase
                            .execute(request: request)
                            .do(onNext: { [unowned self] conversationItemNavigation in
                                if !conversationItemNavigation.conversationID.isEmpty && !self.didMoveToChat {
                                    self.didMoveToChat = true
                                    self.displayLogic.goToChat(conversationItemNavigation: conversationItemNavigation, loginUser: self.loginUser)
                                }
                            })
                    }
                    .trackActivity(activityIndicator)
                    .trackError(errorTracker)
                    .asDriverOnErrorJustComplete()
                    .drive()
                    .disposed(by: self.disposeBag)
            })
            
            .disposed(by: disposeBag)
        
        
        return Output(fetching: activityIndicator.asDriver(), error: errorTracker.asDriver(), items: self.items.asDriver())
    }
    
}

extension ContactsViewModel {
    public struct Input {
        let trigger: Driver<Void>
        let publishSubject: PublishSubject<User>
    }
    
    public struct Output {
        let fetching: Driver<Bool>
        let error: Driver<Error>
        let items: Driver<[ContactItem]>
    }
}
