//
//  ContactItem.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/5/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

class ContactItem {
    let avatar: String
    let contactName: String
    let conversationIDList: [String : String]?
    
    init(avatar: String, contactName: String, conversationIDList: [String : String]?) {
        self.avatar = avatar
        self.contactName = contactName
        self.conversationIDList = conversationIDList
    }
}
