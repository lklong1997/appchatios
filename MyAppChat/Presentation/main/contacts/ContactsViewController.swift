//
//  ContactsViewController.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/4/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

protocol ContactsDisplayLogic: class {
    func goToChat(conversationItemNavigation: ConversationItemNavigation, loginUser: User)
}

class ContactsViewController: BaseViewController {
    @IBOutlet private weak var tableView : UITableView!
    
    private var viewModel: ContactsViewModel!
    private let disposeBag = DisposeBag()
    private var items : RxTableViewSectionedReloadDataSource<SectionModel<String, ContactItem>>!
    private let publishSubject = PublishSubject<User>()
    var loginUser: User!
    
    class func newInstance(loginUser: User) -> UIViewController {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "ContactsViewController") as! ContactsViewController
        vc.loginUser = loginUser
        vc.initViewModel()
        return vc
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public func initViewModel() {
        self.viewModel = ContactsViewModel(displayLogic: self, user: self.loginUser)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        setupUI()
        setupLayoutConstraints()
        bindViewModel()
    }
    
    
    private func setupUI() {
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 80
        self.tableView.separatorStyle = .singleLine
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        self.items = RxTableViewSectionedReloadDataSource<SectionModel<String, ContactItem>>(configureCell: { (_, tv, ip, item) -> UITableViewCell in
            let cell = tv.dequeueReusableCell(withIdentifier: "ContactCell", for: ip) as! ContactCell
            cell.bind(item: item)
            return cell
        })
        
        self.tableView.rx.itemSelected.asDriver()
            .drive ( onNext: { [unowned self] indexPath in
                self.tableView.deselectRow(at: indexPath, animated: false)
                let contactItem  = self.items.sectionModels[0].items[indexPath.row]
                let contact = User(name: contactItem.contactName,
                                   password: contactItem.avatar,
                                   avatarUrl: contactItem.avatar,
                                   conversationIDList: contactItem.conversationIDList)
                self.publishSubject.onNext(contact)
            })
            .disposed(by: disposeBag)
    }
    
    private func setupLayoutConstraints() {
        
    }
    
    private func bindViewModel() {
        let viewWillAppear = self.rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        
        let input = ContactsViewModel.Input(trigger: viewWillAppear, publishSubject: self.publishSubject)
        
        let output = viewModel.transform(input: input)
        output.fetching.drive(onNext: { [unowned self] (show) in
            self.showLoading(withStatus: show)
        })
            .disposed(by: disposeBag)
        
        output.error.drive(onNext: { [unowned self] (error) in
            self.showError(error: error)
        })
            .disposed(by: disposeBag)
        
        output.items
            .map { [SectionModel(model: "Items", items: $0)] }
            .drive(self.tableView.rx.items(dataSource: self.items))
            .disposed(by: self.disposeBag)
    }
}
extension ContactsViewController: ContactsDisplayLogic {
    func goToChat(conversationItemNavigation: ConversationItemNavigation, loginUser: User) {
        let vc = ChatViewController.newInstance(conversationItemNavigation: conversationItemNavigation, loginUser: loginUser)
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
