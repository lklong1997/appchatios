//
//  ContactCell.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/5/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import UIKit
import PureLayout
import Kingfisher

class ContactCell: UITableViewCell {
    @IBOutlet private weak var avatar: UIImageView!
    @IBOutlet private weak var contactName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        avatar.autoSetDimensions(to: CGSize(width: 64.0, height: 64.0))
        avatar.autoAlignAxis(toSuperviewAxis: .horizontal)
        avatar.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets(top: 8.0, left: 8.0, bottom: 8.0, right: 0.0), excludingEdge: .right)
        avatar.contentMode = .scaleAspectFill
        avatar.layer.cornerRadius = 32.0
        avatar.layer.borderWidth = 1.0
        avatar.layer.borderColor = UIColor.gray.cgColor
        avatar.clipsToBounds = true
        
        contactName.textAlignment = .left
        contactName.autoAlignAxis(.horizontal, toSameAxisOf: avatar)
        contactName.autoPinEdge(toSuperviewEdge: .right, withInset: 8.0)
        contactName.autoPinEdge(.left, to: .right, of: avatar, withOffset: 16.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func bind(item: ContactItem) {
        let processor = BlurImageProcessor(blurRadius: 4) >> RoundCornerImageProcessor(cornerRadius: 20)
        avatar.kf.setImage(with: URL(string: item.avatar),
                           placeholder: UIImage.imageAvatar,
                           options: [.processor(processor)],
                           progressBlock: nil,
                           completionHandler: { (image, error, cacheType, imageURL) in
                            
        })
        contactName.text = item.contactName
    }
}
