
//
//  ContactClickUseCase.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/10/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift
class ContactClickUseCase: UseCase {
    private let repository : ConversationRepository = ConversationRepositoryFactory.sharedInstance
    
    public typealias Request = ContactClickRequest
    public typealias Response = ConversationItemNavigation
    
    public func execute(request: ContactClickRequest) -> Observable<ConversationItemNavigation> {
        return repository.contactClick(request: request)
    }
}
