//
//  UpdateUserAvatarUseCase.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/18/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift

class UpdateUserAvatarUseCase: UseCase {
    private let repository: UserRepository = UserRepositoryFactory.sharedInstance
    
    public typealias Request = UpdateUserAvatarRequest
    public typealias Response = Bool
    
    func execute(request: UpdateUserAvatarRequest) -> Observable<Bool> {
        return repository.updateUserAvatar(request: request)
    }
}
