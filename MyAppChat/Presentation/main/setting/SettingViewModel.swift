//
//  SettingViewModel.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/17/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift
import RxCocoa

final class SettingViewModel: ViewModelDelegate {
    
    private let disposeBag = DisposeBag()
    private let loginUser : User!
    private weak var displayLogic: SettingDisplayLogic!
    private let updateUserAvatarUseCase = UpdateUserAvatarUseCase()
    private let logoutUseCase = LogoutUseCase()
    
    init(displayLogic: SettingDisplayLogic, loginUser: User) {
        self.displayLogic = displayLogic
        self.loginUser = loginUser
    }
    
    func transform(input: Input) -> Output {
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        
        input.publishSubject
            .subscribe({ [unowned self] avatarUrl in
                Observable.deferred {
                    Observable.just(UpdateUserAvatarRequest(userAvatar: avatarUrl.element!, name: self.loginUser.name))
                    }
                    .flatMap { [unowned self] request -> Observable<Bool> in
                        self.updateUserAvatarUseCase
                            .execute(request: request)
                            .do(onNext: { success in
                                print("Update user avatar success : \(success)")
                                
                            })
                    }
                    .trackActivity(activityIndicator)
                    .trackError(errorTracker)
                    .asDriverOnErrorJustComplete()
                    .drive()
                    .disposed(by: self.disposeBag)
            })
            .disposed(by: disposeBag)
        
        input.logoutTrigger
            .flatMap { [unowned self] (_) -> Driver<Bool> in
                return Observable.deferred {
                    return Observable.just(LogoutRequest())
                    }
                    .flatMap { [unowned self] request -> Observable<Bool> in
                        return self.logoutUseCase
                            .execute(request: request)
                    }
                    .do(onNext: { [unowned self] success in
                        self.displayLogic.gotoLogin()
                    })
                    .asDriverOnErrorJustComplete()
            }
            .drive()
            .disposed(by: disposeBag)
        
        return Output(fetching: activityIndicator.asDriver(), error: errorTracker.asDriver())
        
    }
    
    func updateUserAvatarUrl(avatar: String) {
        
    }
}

extension SettingViewModel {
    public struct Input {
        let publishSubject : PublishSubject<String>
        let logoutTrigger: Driver<Void>
    }
    
    public struct Output {
        let fetching : Driver<Bool>
        let error : Driver<Error>
    }
}
