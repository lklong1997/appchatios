//
//  SettingViewController.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/17/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import Photos
import UIKit
import PureLayout
import RxCocoa
import RxSwift
import Kingfisher
import FirebaseStorage
import AlamofireImage

protocol SettingDisplayLogic: class {
    func gotoLogin()
}

class SettingViewController: BaseViewController {
    @IBOutlet private weak var userAvatar: UIImageView!
    @IBOutlet private weak var userName: UILabel!
    @IBOutlet private weak var logOutButton: UIButton!
    
    class func newInstance(loginUser: User) -> UIViewController{
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "SettingViewController") as! SettingViewController
        vc.loginUser = loginUser
        vc.initViewModel()
        return vc
    }
    
    var loginUser: User!
    private let disposeBag = DisposeBag()
    private var viewModel : SettingViewModel!
    private let storageRef = FirebaseInjection.storageRef
    private let publishSubject = PublishSubject<String>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        setupLayoutConstraints()
        bindViewModel()
    }
    
    private func setupUI() {
        userAvatar.autoSetDimensions(to: CGSize(width: 100.0, height: 100.0))
        userAvatar.contentMode = .scaleToFill
        userAvatar.layer.borderColor = UIColor.lightGray.cgColor
        userAvatar.layer.borderWidth = 1.0
        userAvatar.layer.cornerRadius = 50.0
        userAvatar.clipsToBounds = true
        
        
        userAvatar.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer()
        userAvatar.addGestureRecognizer(tapGesture)
        tapGesture.rx.event.bind(onNext: {recognizer in
            self.avatarClicked()
        }).disposed(by: disposeBag)
        
        userName.textAlignment = .center
        userName.lineBreakMode = .byCharWrapping
        userName.textColor = UIColor.black
        userName.font.withSize(17.0)
        userName.backgroundColor = UIColor.orange
        
        logOutButton.contentEdgeInsets = UIEdgeInsets(top: 8.0, left: 8.0, bottom: 8.0, right: 8.0)
        logOutButton.layer.borderColor = UIColor.lightGray.cgColor
        logOutButton.layer.borderWidth = 1.0
        logOutButton.layer.cornerRadius = 8.0
        logOutButton.backgroundColor = UIColor.textFieldBlue
        logOutButton.clipsToBounds = true
        logOutButton.setTitleColor(UIColor.white, for: .normal)
        logOutButton.setTitle("LOG OUT", for: .normal)
    }
    
    private func setupLayoutConstraints() {
        userAvatar.autoAlignAxis(toSuperviewAxis: .vertical)
        userAvatar.autoPinEdge(toSuperviewMargin: .top)
        
        userName.autoAlignAxis(toSuperviewAxis: .vertical)
        userName.autoPinEdge(.top, to: .bottom, of: userAvatar, withOffset: 16.0)
        userName.autoPinEdge(toSuperviewEdge: .left, withInset: 64.0, relation: .greaterThanOrEqual)
        userName.autoPinEdge(toSuperviewEdge: .right, withInset: 64.0, relation: .greaterThanOrEqual)
        
        logOutButton.autoAlignAxis(toSuperviewAxis: .vertical)
        logOutButton.autoPinEdge(.top, to: .bottom, of: userName, withOffset: 16.0)
    }
    
    private func bindViewModel() {
        userAvatar.kf.setImage(with: URL(string: self.loginUser.avatarUrl ?? ""),
                               placeholder: UIImage.imageSettingAvatar,
                               options: nil,
                               progressBlock: nil,
                               completionHandler: { (image, error, cacheType, imageUrl) in
                                
        })
        userName.text = self.loginUser.name
        
        let input = SettingViewModel.Input(publishSubject: self.publishSubject,
                                           logoutTrigger: logOutButton.rx.tap.asDriver())
        
        let output = self.viewModel.transform(input: input)
        
        output.fetching.drive(onNext: { [unowned self] show in
            self.showLoading(withStatus: show)
        })
            .disposed(by: disposeBag)
        
        output.error.drive(onNext: { [unowned self] error in
            self.showError(error: error)
        })
            .disposed(by: disposeBag)
    }
    
    
    private func avatarClicked() {
        if askForPermission() {
            let alert = UIAlertController(title: "Select Image", message: "", preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { action in
                if UIImagePickerController.isSourceTypeAvailable(.camera) {
                    let imagePicker = UIImagePickerController()
                    imagePicker.delegate = self
                    imagePicker.sourceType = .camera
                    self.present(imagePicker, animated: true, completion: nil)
                } else {
                    self.view.makeToast("Camera is not available")
                }
            }))
            alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { action in
                if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum) {
                    let imagePicker = UIImagePickerController()
                    imagePicker.delegate = self
                    imagePicker.sourceType = .savedPhotosAlbum
                    self.present(imagePicker, animated: true, completion: nil)
                } else {
                    self.view.makeToast("Gallery is not available")
                }
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.sourceRect = self.userAvatar.bounds
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    private func askForPermission() -> Bool {
        var isAllowed = false
        
        let photos = PHPhotoLibrary.authorizationStatus()
        
        if photos == .notDetermined {
            PHPhotoLibrary.requestAuthorization({ status in
                if status == .authorized {
                    isAllowed = true
                }
                else {
                    isAllowed = false
                }
            })
        }
        else if photos == .denied {
            let alert = UIAlertController(title: "Please grant permission", message: "", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Grant permission manually", style: .default, handler: {action in
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!,
                                          options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]),
                                          completionHandler: { success in
                                            isAllowed = true
                })
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
        }
        else {
            isAllowed = true
        }
        
        return isAllowed
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func initViewModel() {
        self.viewModel = SettingViewModel(displayLogic: self, loginUser: self.loginUser)
    }
}

extension SettingViewController: SettingDisplayLogic {
    func gotoLogin() {
        let vc = LoginViewController.newInstance()
        let rootvc = UINavigationController(rootViewController: vc)
        self.navigationController?.present(rootvc, animated: true, completion: nil)
    }
}

extension SettingViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // Local variable inserted by Swift 4.2 migrator.
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        let fileManager = FileManager()
        let documentPath = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first
        let imagePath = documentPath?.appendingPathComponent(String(CLong(NSDate().timeIntervalSince1970)) + ".jpg")
        if let pickedImage = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            let resizedImage = pickedImage.af_imageAspectScaled(toFill: CGSize(width: 100.0, height: 100.0))
            
            try! resizedImage.jpegData(compressionQuality: 0.8)?.write(to: imagePath!)
            
            let filePath = "/\(self.loginUser.name)/\(imagePath!.lastPathComponent)"
            let metaData = StorageMetadata()
            metaData.contentType = "image/jpg"
            
            self.storageRef.child(filePath).putFile(from: imagePath!, metadata: metaData, completion: { [unowned self] (metaDat, error) in
                self.storageRef.child(filePath).downloadURL(completion: { [unowned self] (url, error) in
                    if let error = error {
                        self.showToast(message: "Error occurred while uploading image to database. Please try again.")
                        print("Error when uploading image to firebase \(error)")
                    }
                    else {
                        print("downloadURL: \(url!.absoluteString)")
                        DispatchQueue.main.async {
                            self.userAvatar.image = resizedImage
                        }
                        self.publishSubject.onNext(url!.absoluteString)
                    }
                })
            })
            
            self.dismiss(animated: true, completion: nil)
        }
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}
