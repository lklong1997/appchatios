//
//  LogoutUseCase.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/25/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift

class LogoutUseCase: UseCase {
    private let repository: UserRepository = UserRepositoryFactory.sharedInstance
    
    public typealias Request = LogoutRequest
    public typealias Response = Bool
    
    func execute(request: LogoutRequest) -> Observable<Bool> {
        return repository.logout(request: request)
    }
}
