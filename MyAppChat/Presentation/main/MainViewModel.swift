//
//  MainViewModel.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/4/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift
import RxCocoa

final class MainViewModel {
    private let displayLogic: MainDisplayLogic!
    
    init(displayLogic: MainDisplayLogic) {
        self.displayLogic = displayLogic
    }
}
