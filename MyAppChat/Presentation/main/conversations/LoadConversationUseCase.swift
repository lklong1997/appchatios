//
//  LoadConversationUseCase.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/6/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift

class LoadConversationUseCase: UseCase {
    private let repository : ConversationRepository = ConversationRepositoryFactory.sharedInstance
    
    public typealias Request = LoadConversationRequest
    public typealias Response = [Conversation]
    
    public func execute(request: LoadConversationRequest) -> Observable<[Conversation]> {
        return repository.loadConversation(request: request)
    }
}
