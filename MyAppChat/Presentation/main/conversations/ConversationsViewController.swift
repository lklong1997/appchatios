//
//  ConversationsViewController.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/4/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

protocol ConversationsDisplayLogic: class {
    
}

class ConversationsViewController: BaseViewController {
    @IBOutlet private weak var tableView: UITableView!
    private var viewModel: ConversationsViewModel!
    private let disposeBag = DisposeBag()
    private var items : RxTableViewSectionedReloadDataSource<SectionModel<String, ConversationItem>>!
    var loginUser: User!

    class func newInstance(loginUser: User) -> UIViewController {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "ConversationsViewController") as! ConversationsViewController
        vc.loginUser = loginUser
        vc.initViewModel()
        return vc
    }
    
    public func initViewModel() {
        self.viewModel = ConversationsViewModel(displayLogic: self, user: self.loginUser)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        setupUI()
        setupLayoutConstraints()
        bindViewModel()
    }
    
    private func setupUI() {
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 80.0
        self.tableView.separatorStyle = .singleLine
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        self.items = RxTableViewSectionedReloadDataSource<SectionModel<String, ConversationItem>>(configureCell:
            { (tableViewSectionDataSource, tableView, indexPath, item) -> UITableViewCell in
                switch item {
                case .privateItem(let privateItem):
                    let cell = tableView.dequeueReusableCell(withIdentifier: "PrivateCell") as! PrivateCell
                    cell.bind(item: privateItem)
                    return cell
                }
        })
        self.tableView.rx.itemSelected.asDriver()
            .drive(onNext: { [unowned self] indexPath in
                self.tableView.deselectRow(at: indexPath, animated: false)
                let conversationItem = self.items.sectionModels[0].items[indexPath.row]
                switch conversationItem {
                case .privateItem(let privateItem) :
                    let conversationItemNavigation = ConversationItemNavigation(conversationID: privateItem.conversationKey,
                                                                                userIDList: privateItem.userIDList)
                    self.goToChatViewController(conversationItemNavigation: conversationItemNavigation)
                    break
                }
            })
            .disposed(by: disposeBag)
        
    }
    
    private func setupLayoutConstraints() {
        
    }
    
    private func goToChatViewController(conversationItemNavigation: ConversationItemNavigation) {
        let vc = ChatViewController.newInstance(conversationItemNavigation: conversationItemNavigation, loginUser: self.loginUser)
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func bindViewModel() {
        let viewWillAppear = self.rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        
        let input = ConversationsViewModel.Input(trigger: viewWillAppear)
        
        let output = viewModel.transform(input: input)
        output.fetching.drive(onNext: { [unowned self] (show) in
            self.showLoading(withStatus: show)
        }).disposed(by: disposeBag)
        
        output.error.drive(onNext: { [unowned self] (error) in
            self.showError(error: error)
        }).disposed(by: disposeBag)
        
        output.items
            .map { [SectionModel(model: "Items", items: $0)] }
            .drive(self.tableView.rx.items(dataSource: self.items))
            .disposed(by: disposeBag)
    }
}

extension ConversationsViewController: ConversationsDisplayLogic {
    
}
