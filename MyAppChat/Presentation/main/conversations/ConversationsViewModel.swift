//
//  ConversationsViewModel.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/6/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift
import RxCocoa

class ConversationsViewModel: ViewModelDelegate {
    private weak var displayLogic: ConversationsDisplayLogic!
    private var loginUser: User!
    private let disposeBag = DisposeBag()
    private let items = BehaviorRelay<[ConversationItem]>(value: [])
    private let loadConversationUseCase = LoadConversationUseCase()
    
    private let typePrivate = FirebaseInjection.privateType
    private let typeGroup = FirebaseInjection.groupType
    
    init(displayLogic: ConversationsDisplayLogic, user: User) {
        self.displayLogic = displayLogic
        self.loginUser = user
    }
    
    func transform(input: Input) -> Output {
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        
        input.trigger
            .flatMap { [unowned self] (_) -> Driver<[Conversation]> in
                return Observable.deferred { [unowned self] in
                    return Observable.just(LoadConversationRequest(user: self.loginUser))
                    }
                    .flatMap { [unowned self] (request) -> Observable<[Conversation]> in
                        return self.loadConversationUseCase
                            .execute(request: request)
                    }
                    .do(onNext: { [unowned self] (conversations) in
                        self.items.accept(conversations.map { (conversation) in
                            return self.convertToConversationItem(conversation: conversation, loginUser: self.loginUser)
                        })
                    })
                    //.trackActivity(activityIndicator)
                    //.trackError(errorTracker)
                    .asDriverOnErrorJustComplete()
            }
            .drive()
            .disposed(by: disposeBag)
        
        return Output(fetching: activityIndicator.asDriver(), error: errorTracker.asDriver(), items: items.asDriver())
    }
    
    private func convertToConversationItem(conversation: Conversation, loginUser: User) -> ConversationItem {
        var conversationItem : ConversationItem        
        
        var avatarUrl = ""
        for key in conversation.userIDlist.keys {
            if key != loginUser.name { // Get the avatar url of other user
                avatarUrl = conversation.userIDlist[key]!
            }
        }
        
        let type = conversation.type
        
        var conversationName : String!
        for name in conversation.conversationName {
            if name != loginUser.name { // Private message: we just need to display the name of the one we are chatting with
                conversationName = name
            }
        }
        
        let latestMessage = conversation.latestMessage
        var latestMessageTimeString: String?
        if let latestMessageTime = conversation.newestTime {
            latestMessageTimeString = Converter.calculateLatestMessageTimeString(latestMessageTime: latestMessageTime)
        }
        let latestMessageType = conversation.latestMessageType
        
        //let sender = value[self.senderNode] as? String
        
        switch type {
        case self.typePrivate:
            conversationItem = ConversationItem.privateItem(PrivateItem(conversationKey: conversation.id!, avatarUrl: avatarUrl, conversationName: conversationName, latestMessage: latestMessage ?? "", latestMessageTime: latestMessageTimeString ?? nil, latestMessageType: latestMessageType ?? "", userIDList: conversation.userIDlist))
            return conversationItem
        default:
            break
        }
        fatalError("Unhandled conversation type.")
    }
}

extension ConversationsViewModel {
    public struct Input {
        let trigger: Driver<Void>
    }
    
    public struct Output {
        let fetching : Driver<Bool>
        let error: Driver<Error>
        let items: Driver<[ConversationItem]>
    }
}
