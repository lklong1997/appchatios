//
//  ConversationItem.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/6/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//
import Foundation

enum  ConversationItem {
    case privateItem(PrivateItem)
}
