//
//  PrivateCell.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/6/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import UIKit

class PrivateCell: UITableViewCell {
    @IBOutlet private weak var avatar: UIImageView!
    @IBOutlet private weak var labelSender: UILabel!
    @IBOutlet private weak var labelLatestMessage: UILabel!
    @IBOutlet private weak var labelLatestTime: UILabel!
    
    private let textType = FirebaseInjection.textType
    private let imageType = FirebaseInjection.imageType
    private let audioType = FirebaseInjection.audioType
    private let videoType = FirebaseInjection.videoType
    private let fileType = FirebaseInjection.fileType
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        avatar.autoSetDimensions(to: CGSize(width: 64.0, height: 64.0))
        avatar.autoPinEdge(toSuperviewEdge: .left, withInset: 8.0)
        avatar.autoPinEdge(toSuperviewEdge: .top, withInset: 8.0)
        avatar.autoPinEdge(toSuperviewEdge: .bottom, withInset: 8.0)
        avatar.contentMode = .scaleAspectFill
        avatar.layer.borderWidth = 1.0
        avatar.layer.borderColor = UIColor.gray.cgColor
        avatar.layer.cornerRadius = 32.0
        avatar.clipsToBounds = true
        
        labelLatestTime.numberOfLines = 1
        labelLatestTime.font.withSize(11.0)
        labelLatestTime.textAlignment = .right
        labelLatestTime.autoSetDimensions(to: CGSize(width: 128.0, height: 13.0))
        labelLatestTime.textColor = UIColor.lightGray
        labelLatestTime.autoPinEdge(.top, to: .top, of: avatar, withOffset: 0.0)
        labelLatestTime.autoPinEdge(toSuperviewEdge: .right, withInset: 8.0)
        
        labelSender.numberOfLines = 1
        labelSender.font.withSize(17.0)
        labelSender.textColor = UIColor.black
        labelSender.autoAlignAxis(.horizontal, toSameAxisOf: avatar, withOffset: -16.0)
        labelSender.autoPinEdge(.left, to: .right, of: avatar, withOffset: 16.0)
        labelSender.autoPinEdge(.right, to: .left, of: labelLatestTime, withOffset: 32.0, relation: .lessThanOrEqual)
        
        labelLatestMessage.lineBreakMode = .byTruncatingTail
        labelLatestMessage.numberOfLines = 1
        labelLatestMessage.font.withSize(17.0)
        labelLatestMessage.textColor = UIColor.lightGray
        labelLatestMessage.autoAlignAxis(.horizontal, toSameAxisOf: avatar, withOffset: 16.0)
        labelLatestMessage.autoPinEdge(.left, to: .right, of: avatar, withOffset: 16.0)
        labelLatestMessage.autoPinEdge(.right, to: .left, of: labelLatestTime, withOffset: -32.0, relation: .lessThanOrEqual)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    public func bind(item: PrivateItem) {
        avatar.kf.setImage(with: URL(string: item.avatarUrl),
                           placeholder: UIImage.imageAvatar,
                           options: nil,
                           progressBlock: nil,
                           completionHandler: { (image, error, cacheType, url) in
                            
        })
        labelSender.text = item.conversationName
        
        let messageType = item.latestMessageType
        if messageType == self.textType {
            labelLatestMessage.text = item.latestMessage
        } else if messageType == self.imageType || messageType == self.fileType {
            labelLatestMessage.text = "Sent an attachment."
        }
        
        if let time = item.latestMessageTime {
            labelLatestTime.text = time
        }
    }
}
