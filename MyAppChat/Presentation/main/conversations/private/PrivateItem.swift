//
//  PrivateItem.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/6/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

class PrivateItem {
    let conversationKey: String
    let avatarUrl: String
    let conversationName: String
    let latestMessage: String
    let latestMessageTime: String?
    let latestMessageType: String
    let userIDList: [String : String]
    init(conversationKey: String, avatarUrl: String, conversationName: String, latestMessage: String, latestMessageTime: String?, latestMessageType: String,  userIDList: [String : String] ) {
        self.conversationKey = conversationKey
        self.avatarUrl = avatarUrl
        self.conversationName = conversationName
        self.latestMessage = latestMessage
        self.latestMessageTime = latestMessageTime
        self.latestMessageType = latestMessageType
        self.userIDList = userIDList
    }
}
