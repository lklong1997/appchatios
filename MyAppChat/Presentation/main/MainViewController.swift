//
//  MainViewController.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/4/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import PureLayout

protocol MainDisplayLogic: class {
    
}


class MainViewController: UITabBarController {
    class func newInstance(user: User) -> UIViewController {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        vc.loginUser = user
        return vc
    }
    
    var loginUser: User!
    private var viewModel: MainViewModel!
    private let disposeBag = DisposeBag()
    private let blue = UIColor.textFieldBlue
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        viewModel = MainViewModel(displayLogic: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isTranslucent = false
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {        
        super.viewDidLoad()
        
        let contactsViewController = ContactsViewController.newInstance(loginUser: loginUser)
        contactsViewController.tabBarItem = UITabBarItem(title: "Contacts", image: UIImage.imageUser, tag: 0)
        let vc = UINavigationController(rootViewController: contactsViewController)
        
        let conversationsViewController = ConversationsViewController.newInstance(loginUser: loginUser)
        conversationsViewController.tabBarItem = UITabBarItem(title: "Conversations", image: UIImage.imageConversation, tag: 1)
        let vc2 = UINavigationController(rootViewController: conversationsViewController)
        
        let settingViewController = SettingViewController.newInstance(loginUser: loginUser)
        settingViewController.tabBarItem = UITabBarItem(title: "Setting", image: UIImage.imageSetting, tag: 2)
        let vc3 = UINavigationController(rootViewController: settingViewController)
        
        let tabBarList = [vc, vc2, vc3]
        viewControllers = tabBarList
        
        setupUI()
        setupLayoutConstraints()
        bindViewModel()
    }
    
    private func setupUI() {
        
    }
    
    private func setupLayoutConstraints() {
        
    }
    
    private func bindViewModel() {
        
    }
}

extension MainViewController: MainDisplayLogic {
    
}
