//
//  GoogleMapViewModel.swift
//  MyAppChat
//
//  Created by CPU11814 on 10/29/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift
import RxCocoa

class GoogleMapViewModel {
    private weak var displayLogic: GoogleMapDisplayLogic!
    private let loginUser: User
    
    init(displayLogic: GoogleMapDisplayLogic, loginUser: User) {
        self.displayLogic = displayLogic
        self.loginUser = loginUser
    }
}
