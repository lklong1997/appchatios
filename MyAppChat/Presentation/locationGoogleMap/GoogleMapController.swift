//
//  GoogleMapController.swift
//  MyAppChat
//
//  Created by CPU11814 on 10/26/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import RxCocoa
import RxSwift

class GoogleMapController: BaseViewController {
    @IBOutlet private weak var mapView: GMSMapView!
    public weak var delegate: LocationViewControllerDelegate!

    private var loginUser: User!
    private let locationManager = CLLocationManager()
    private let zoom: Float = 15
    private var currentLocationMarker = GMSMarker()
    private var currentPlace: GMSPlace?
    
    private var searchController: UISearchController!
    private var resultsViewController: GMSAutocompleteResultsViewController!
    private let searchRadius = 500
    
    let widthOfScreenshot: CGFloat = 200
    let heightOfScreenshot: CGFloat = 150
    
    class func newInstance(loginUser: User) -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "GoogleMapController") as! GoogleMapController
        vc.loginUser = loginUser
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        if askForPermission() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
            
            mapView.isMyLocationEnabled = true
            mapView.settings.myLocationButton = true
        }
    }
    
    @objc func sendLocation() {
        let projection = self.mapView.projection
        let currentPoint = projection.point(for: self.currentLocationMarker.position)
        
        var startX: CGFloat = 0
        var startY: CGFloat = 0
        
        let halfWidth = widthOfScreenshot / 2
        let halfHeight = heightOfScreenshot / 2
        
        if currentPoint.x >= halfWidth {
            startX = -(currentPoint.x - halfWidth)
        } else {
            startX = 0
        }
        
        if currentPoint.y >= halfHeight {
            startY = -(currentPoint.y - halfHeight)
        } else {
            startY = 0
        }
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: widthOfScreenshot, height: heightOfScreenshot), false, 0)
        self.mapView.drawHierarchy(in: CGRect(x: startX, y: startY, width: mapView.bounds.size.width, height: mapView.bounds.size.height), afterScreenUpdates: true)
        
        let locationSnapshot = UIGraphicsGetImageFromCurrentImageContext()

        var name: String?
        if currentPlace != nil {
            name = currentPlace?.formattedAddress
        }
        self.delegate.didFinishLocationVC(snapshot: locationSnapshot!, location: self.currentLocationMarker.position, name: name ?? "")
    }
    
    private func setupUI() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Send", style: .done, target: self, action: #selector(sendLocation))
        
        self.mapView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        self.mapView.autoPinEdge(toSuperviewEdge: .left)
        self.mapView.autoPinEdge(toSuperviewEdge: .right)
        self.mapView.autoPinEdge(toSuperviewEdge: .bottom)        
        
        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController.delegate = self

        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController.searchResultsUpdater = resultsViewController
        searchController.searchBar.placeholder = "Search location"

        let filter = GMSAutocompleteFilter()
        resultsViewController.autocompleteFilter = filter
        
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    
    private func askForPermission() -> Bool {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .authorizedAlways, .authorizedWhenInUse:
                return true
            case .notDetermined:
                locationManager.requestAlwaysAuthorization()
                locationManager.requestWhenInUseAuthorization()
                return true
            case .denied:
                let alert = UIAlertController(title: "Please grant permission", message: "", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Grant permission manually", style: .default, handler: {action in
                    UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!,
                                              options: Converter.convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]),
                                              completionHandler: { success in
                                                
                    })
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                present(alert, animated: true, completion: nil)
            default:
                return false
            }
        }
        return false
    }
    
    private func changeCurrentMarkerLocationAndAnimate(coordinate: CLLocationCoordinate2D) {
        self.currentLocationMarker.position = coordinate
        mapView.animate(toLocation: coordinate)
    }
}

extension GoogleMapController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        
        self.currentLocationMarker.position = location.coordinate
        mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: self.zoom, bearing: 0, viewingAngle: 0)
        self.currentLocationMarker.map = self.mapView
        
        self.mapView.delegate = self
    }
}

extension GoogleMapController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        changeCurrentMarkerLocationAndAnimate(coordinate: coordinate)
    }
}

extension GoogleMapController: GMSAutocompleteResultsViewControllerDelegate {
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController, didAutocompleteWith place: GMSPlace) {
        currentPlace = place
        changeCurrentMarkerLocationAndAnimate(coordinate: place.coordinate)
        self.searchController.isActive = false
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController, didFailAutocompleteWithError error: Error) {
        self.searchController.isActive = false
    }
    
}
