//
//  LocationSearchTable.swift
//  MyAppChat
//
//  Created by CPU11814 on 10/26/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import UIKit
import MapKit

class LocationSearchTable: UITableViewController {
    private var matchingLocation = [MKMapItem]()
    var mapView: MKMapView!
    private weak var displayLogic: LocationDisplayLogic!

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return matchingLocation.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        let location = matchingLocation[indexPath.row].placemark
        cell.textLabel?.text = location.name
        cell.detailTextLabel?.text = location.title
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)        
        self.displayLogic.clickedLocation(location: matchingLocation[indexPath.row].placemark)
    }
    
    func setDisplayLogic(displayLogic: LocationDisplayLogic) {
        self.displayLogic = displayLogic
    }
}

extension LocationSearchTable: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        guard let mapView = mapView, let searchBarText = searchController.searchBar.text else { return }
        let request = MKLocalSearch.Request()
        request.naturalLanguageQuery = searchBarText
        request.region = mapView.region
        let search = MKLocalSearch(request: request)
        search.start(completionHandler: { response, error in
            if error != nil {
                return
            }
            self.matchingLocation = response!.mapItems
            self.tableView.reloadData()
        })
    }
}

