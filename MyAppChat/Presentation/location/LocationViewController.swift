//
//  LocationViewController.swift
//  MyAppChat
//
//  Created by CPU11814 on 10/23/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import MapKit

protocol LocationDisplayLogic: class {
    func clickedLocation(location: MKPlacemark)
}

class LocationViewController: BaseViewController {
    @IBOutlet private weak var mapView: MKMapView!
    
    private var loginUser: User!
    private var viewModel: LocationViewModel!
    private let disposeBag = DisposeBag()
    
    private let regionRadius: CLLocationDistance = 500
    private let locationManager = CLLocationManager()
    private var currentLocation: CLLocation!
    private var annotation = MKPointAnnotation()
    
    private var searchController: UISearchController!
    
    class func newInstance(user: User) -> UIViewController {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "LocationViewController") as! LocationViewController
        vc.loginUser = user
        vc.initViewModel()
        return vc
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        bindViewModel()
        if askForPermission() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    private func initViewModel() {
        self.viewModel = LocationViewModel(loginUser: self.loginUser, displayLogic: self)
    }
    
    private func setupUI() {
        self.mapView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        self.mapView.autoPinEdge(toSuperviewEdge: .left)
        self.mapView.autoPinEdge(toSuperviewEdge: .right)
        self.mapView.autoPinEdge(toSuperviewEdge: .bottom)

        let locationSearchTable = storyboard?.instantiateViewController(withIdentifier: "LocationSearchTable") as! LocationSearchTable
        searchController = UISearchController(searchResultsController: locationSearchTable)
        searchController.searchResultsUpdater = locationSearchTable
        searchController.obscuresBackgroundDuringPresentation = true
        searchController.searchBar.placeholder = "Search location"
        locationSearchTable.mapView = self.mapView
        locationSearchTable.setDisplayLogic(displayLogic: self)
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    
    private func bindViewModel() {
        let viewWillAppear = self.rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        
        let input = LocationViewModel.Input(trigger: viewWillAppear)
        
        let output = viewModel.transform(input: input)
        
        output.fetching.drive(onNext: { [unowned self] success in
            self.showLoading(withStatus: success)
        })
            .disposed(by: disposeBag)
        
        output.error.drive(onNext: { [unowned self] error in
            self.showError(error: error)
        })
            .disposed(by: disposeBag)
    }
    
    private func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: self.regionRadius, longitudinalMeters: self.regionRadius)
        self.mapView.setRegion(coordinateRegion, animated: true)
    }
    
    private func askForPermission() -> Bool {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .authorizedAlways, .authorizedWhenInUse:
                return true
            case .notDetermined:
                locationManager.requestAlwaysAuthorization()
                locationManager.requestWhenInUseAuthorization()
                return true
            case .denied:
                let alert = UIAlertController(title: "Please grant permission", message: "", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Grant permission manually", style: .default, handler: {action in
                    UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!,
                                              options: Converter.convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]),
                                              completionHandler: { success in
                                                
                    })
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                present(alert, animated: true, completion: nil)
            default:
                return false
            }
        }
        return false
    }
    
    @objc func handleTap(recognizer: UITapGestureRecognizer) {
        let location = recognizer.location(in: mapView)
        let coordinate = mapView.convert(location, toCoordinateFrom: mapView)
        changeCoordinateOfCurrentAnnotation(latitude: coordinate.latitude, longitude: coordinate.longitude)
    }
    
    private func changeCoordinateOfCurrentAnnotation(latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        annotation.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        centerMapOnLocation(location: CLLocation(latitude: annotation.coordinate.latitude, longitude: annotation.coordinate.longitude))
    }
}

extension LocationViewController: LocationDisplayLogic {
    func clickedLocation(location: MKPlacemark) {
        self.searchController.searchBar.text = ""
        self.searchController.isActive = false
        self.changeCoordinateOfCurrentAnnotation(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
    }
}

extension LocationViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        annotation.coordinate = CLLocationCoordinate2D(latitude: locations.last!.coordinate.latitude, longitude: locations.last!.coordinate.longitude)
        self.mapView.addAnnotation(annotation)
        
        centerMapOnLocation(location: CLLocation(latitude: annotation.coordinate.latitude, longitude: annotation.coordinate.longitude))
        
        mapView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap(recognizer:))))
    }
}
