//
//  LocationViewModel.swift
//  MyAppChat
//
//  Created by CPU11814 on 10/23/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift
import RxCocoa

class LocationViewModel: ViewModelDelegate {
    
    private var loginUser: User!
    private weak var displayLogic: LocationDisplayLogic!
    
    init(loginUser: User, displayLogic: LocationDisplayLogic) {
        self.loginUser = loginUser
        self.displayLogic = displayLogic
    }
    
    func transform(input: Input) -> Output {
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
                
        
        return Output(fetching: activityIndicator.asDriver(), error: errorTracker.asDriver())
    }
}

extension LocationViewModel {
    public struct Input {
        let trigger: Driver<Void>
    }
    
    public struct Output {
        let fetching: Driver<Bool>
        let error: Driver<Error>
    }
}
