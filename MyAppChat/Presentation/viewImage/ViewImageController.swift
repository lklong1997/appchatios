//
//  ViewImageController.swift
//  MyAppChat
//
//  Created by CPU11814 on 10/19/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol ViewImageDisplayLogic: class {
    
}

class ViewImageController: BaseViewController {
    @IBOutlet private weak var photo: UIImageView!
    @IBOutlet private weak var scrollView: UIScrollView!
    
    private var viewModel: ViewImageViewModel!
    private var imageURL: String!
    private let disposeBag = DisposeBag()
    private var navigationBarHeight: CGFloat!
    private var initialCenter: CGPoint!
    
    class func newInstance(messageID: String, url: String) -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ViewImageController") as! ViewImageController
        vc.imageURL = url
        vc.initViewModel()
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        bindViewModel()
    }
    
    func initViewModel() {
        viewModel = ViewImageViewModel(displayLogic: self, imageURL: self.imageURL)
    }
    
    private func setupUI() {
        self.view.backgroundColor = UIColor.black
        
        self.scrollView.delegate = self
        self.scrollView.autoPinEdge(.top, to: .top, of: self.view)
        self.scrollView.autoPinEdge(.bottom, to: .bottom, of: self.view)
        self.scrollView.autoPinEdge(.left, to: .left, of: self.view)
        self.scrollView.autoPinEdge(.right, to: .right, of: self.view)
        self.scrollView.minimumZoomScale = 1.0
        self.scrollView.maximumZoomScale = 6.0
        
        self.photo.backgroundColor = UIColor.orange
        self.photo.contentMode = .scaleAspectFill
        self.photo.autoAlignAxis(.horizontal, toSameAxisOf: self.view)
        self.photo.autoPinEdge(.left, to: .left, of: self.view)
        self.photo.autoPinEdge(.right, to: .right, of: self.view)
        self.photo.autoPinEdge(.bottom, to: .bottom, of: self.view, withOffset: -64.0, relation: .lessThanOrEqual)
        self.photo.autoPinEdge(.top, to: .top, of: self.view, withOffset: 64.0, relation: .greaterThanOrEqual)
        self.photo.kf.setImage(with: URL(string: self.imageURL),
                               placeholder: UIImage.imageMessageImage,
                               options: nil,
                               progressBlock: nil,
                               completionHandler: nil)
        self.photo.isUserInteractionEnabled = true
        self.photo.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(handlePan(recognizer:))))
    }
    
    private var dragStartPositionRelativeToCenter : CGPoint?
    private var initialAlpha: CGFloat = 1.0
    
    @objc func handlePan(recognizer: UIPanGestureRecognizer) {
        if recognizer.state == UIGestureRecognizer.State.began {
            self.initialCenter = self.photo.center

            let locationInView = recognizer.location(in: self.view)
            dragStartPositionRelativeToCenter = CGPoint(x: photo.center.x, y: locationInView.y - photo.center.y)
            
            return
        }
        
        if recognizer.state == UIGestureRecognizer.State.ended {
            dragStartPositionRelativeToCenter = nil
            
            if (photo.frame.maxY < self.view.frame.midY + 0) {
                UIView.animate(withDuration: 0.5,
                               animations: { [unowned self] in
                                self.photo.center = CGPoint(x: self.photo.center.x, y: self.photo.center.y - self.photo.frame.height)
                                
                    },
                               completion: {[unowned self] complete in
                                self.dismiss(animated: false, completion: nil)
                })
            } else if (photo.frame.minY > self.view.frame.midY + 0) {
                UIView.animate(withDuration: 0.5,
                               animations: { [unowned self] in
                                self.photo.center = CGPoint(x: self.photo.center.x, y: self.photo.center.y + self.photo.frame.height)
                                
                    },
                               completion: {[unowned self] complete in
                                self.dismiss(animated: false, completion: nil)
                })
            } else { // Reset ?
                UIView.animate(withDuration: 0.5,
                               animations: { [unowned self] in
                                self.initialAlpha = 1.0
                                self.view.backgroundColor = UIColor.black
                                self.photo.center = self.initialCenter
                                
                    },
                               completion: nil)}
            return
        }
        
        let locationInView = recognizer.location(in: self.scrollView)
        
        UIView.animate(withDuration: 0.1, animations: { [unowned self] in
            let oldY = self.photo.center.y
            self.photo.center = CGPoint(x: self.photo.center.x,
                                        y: locationInView.y - self.dragStartPositionRelativeToCenter!.y)
            if recognizer.velocity(in: self.view).y > 0 { // dragging photo to bottom
                if self.photo.center.y >= self.initialCenter.y { // if the image is at bottom half of screen
                    self.initialAlpha -= abs((self.photo.center.y - oldY) / 1000)
                } else { // if the image is at top half of screen
                    self.initialAlpha += abs((self.photo.center.y - oldY) / 1000)
                    if self.initialAlpha > 1 {
                        self.initialAlpha = 1
                    }
                }
            } else { // dragging photo to top
                if self.photo.center.y >= self.initialCenter.y { // if the image is at bottom half of screen
                    self.initialAlpha += abs((self.photo.center.y - oldY) / 1000)
                    if self.initialAlpha > 1 {
                        self.initialAlpha = 1
                    }
                } else { // if the image is at top half of screen
                    self.initialAlpha -= abs((self.photo.center.y - oldY) / 1000)
                }
            }
            
            self.view.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: self.initialAlpha)
        })
    }
    
    private func bindViewModel() {
        let viewWillAppear = self.rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        
        let input = ViewImageViewModel.Input(trigger: viewWillAppear)
        
        let output = viewModel.transform(input: input)
        
        output.fetching.drive(onNext: { [unowned self] (show) in
            self.showLoading(withStatus: show)
        })
            .disposed(by: disposeBag)
        
        output.error.drive(onNext: { [unowned self] (error) in
            self.showError(error: error)
        })
            .disposed(by: disposeBag)
    }
}

extension ViewImageController: ViewImageDisplayLogic {
    
}

extension ViewImageController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return photo
    }
}
