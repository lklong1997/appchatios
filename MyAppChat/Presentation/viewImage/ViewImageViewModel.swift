//
//  ViewImageViewModel.swift
//  MyAppChat
//
//  Created by CPU11814 on 10/19/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift
import RxCocoa

class ViewImageViewModel: ViewModelDelegate {
    private let imageURL: String
    private weak var displayLogic: ViewImageDisplayLogic!
    
    init(displayLogic: ViewImageDisplayLogic, imageURL: String) {
        self.displayLogic = displayLogic
        self.imageURL = imageURL
    }
    
    func transform(input: ViewImageViewModel.Input) -> ViewImageViewModel.Output {
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        
        
        
        return Output(fetching: activityIndicator.asDriver(), error: errorTracker.asDriver())
    }
    
}

extension ViewImageViewModel {
    public struct Input {
        let trigger: Driver<Void>
    }
    
    public struct Output {
        let fetching: Driver<Bool>
        let error: Driver<Error>
    }
}


