//
//  SignUpUseCase.swift
//  MyAppChat
//
//  Created by CPU11814 on 8/31/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift

class SignUpUseCase : UseCase {
    private let repository : UserRepository = UserRepositoryFactory.sharedInstance
    typealias Request = SignUpRequest
    
    typealias Response = User
    
    public func execute(request: SignUpRequest) -> Observable<User> {
        return repository.signUp(request: request)
    }
    
}
