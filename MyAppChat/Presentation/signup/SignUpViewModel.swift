//
//  SignUpViewModel.swift
//  MyAppChat
//
//  Created by CPU11814 on 8/30/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift
import RxCocoa

final class SignUpViewModel: ViewModelDelegate {
    private weak var displayLogic: SignUpDisplayLogic?
    private let username = BehaviorRelay<String>(value: "")
    private let password = BehaviorRelay<String>(value: "")
    private let reenterPassword = BehaviorRelay<String>(value: "")
    private let disposeBag = DisposeBag()
    private let signUpUseCase = SignUpUseCase()
    
    init(displayLogic: SignUpDisplayLogic) {
        self.displayLogic = displayLogic
    }
    
    func transform(input: Input) -> Output {
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        (input.username <-> username)
            .disposed(by: disposeBag)
        (input.password <-> password)
            .disposed(by: disposeBag)
        (input.reenterPassword <-> reenterPassword)
            .disposed(by: disposeBag)
        
        input.signupTrigger
            .flatMap { [unowned self] (_)  -> Driver<User> in
                return Observable.deferred { [unowned self ]() -> Observable<SignUpRequest> in
                    if self.username.value.isEmpty {
                        return Observable.error(ValidateError(message: "Please enter username"))
                    }
                    if self.password.value.isEmpty {
                        return Observable.error(ValidateError(message: "Please enter password"))
                    }
                    if self.reenterPassword.value.isEmpty {
                        return Observable.error(ValidateError(message: "Please re-enter password"))
                    }
                    if self.password.value != self.reenterPassword.value {
                        return Observable.error(ValidateError(message: "Please re-confirm password"))
                    }
                    
                    return Observable.just(SignUpRequest(username: self.username.value, password: self.password.value))
                    }
                    .flatMap { [unowned self] (request) -> Observable<User> in
                        return self.signUpUseCase
                            .execute(request: request)
                    }
                    
                    .trackError(errorTracker)
                    .asDriverOnErrorJustComplete()
            }
            .drive(onNext: { (user) in
                self.displayLogic?.goToMain(user: user)
            }).disposed(by: disposeBag)
        
        
        return Output(fetching: activityIndicator.asDriver(), error: errorTracker.asDriver())
    }
}

extension SignUpViewModel {
    public struct Input {
        let signupTrigger: Driver<Void>
        let username: ControlProperty<String>
        let password: ControlProperty<String>
        let reenterPassword: ControlProperty<String>
    }
    
    public struct Output{
        let fetching: Driver<Bool>
        let error : Driver<Error>
    }
}
