//
//  SignUpViewController.swift
//  MyAppChat
//
//  Created by CPU11814 on 8/30/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import PureLayout

protocol SignUpDisplayLogic: class {
    func goToMain(user: User)
}

class SignUpViewController: BaseViewController {
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var usernameTextField: UITextField!
    @IBOutlet private weak var passwordTextField: UITextField!
    @IBOutlet private weak var reenterTextField: UITextField!
    @IBOutlet private weak var signupButton: UIButton!
    private var viewModel : SignUpViewModel!
    private let disposeBag = DisposeBag()
    
    class func newInstance() -> UIViewController {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "SignUpViewController")
        return vc
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        viewModel = SignUpViewModel(displayLogic: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        setupLayoutConstraints()
        bindViewModel()
        
    }
    
    private func setupUI() {
        let color = UIColor.textFieldBlue
        
        usernameTextField.layer.borderColor = color.cgColor
        usernameTextField.layer.borderWidth = 1.0
        usernameTextField.layer.cornerRadius = 8.0
        usernameTextField.autoSetDimensions(to: CGSize(width: 256.0, height: 32.0))
        
        passwordTextField.layer.borderColor = color.cgColor
        passwordTextField.layer.borderWidth = 1.0
        passwordTextField.layer.cornerRadius = 8.0
        passwordTextField.autoSetDimensions(to: CGSize(width: 256.0, height: 32.0))
        
        reenterTextField.layer.borderColor = color.cgColor
        reenterTextField.layer.borderWidth = 1.0
        reenterTextField.layer.cornerRadius = 8.0
        reenterTextField.autoSetDimensions(to: CGSize(width: 256.0, height: 32.0))
        
        signupButton.contentEdgeInsets = UIEdgeInsets(top: 8.0, left: 8.0, bottom: 8.0, right: 8.0)
        signupButton.layer.borderWidth = 1.0
        signupButton.layer.borderColor = UIColor.lightGray.cgColor
        signupButton.layer.cornerRadius = 8.0
        signupButton.clipsToBounds = true
        signupButton.backgroundColor = color
        signupButton.setTitleColor(UIColor.white, for: .normal)
    }
    
    private func setupLayoutConstraints() {
        containerView.autoCenterInSuperview()
        
        usernameTextField.autoPinEdge(toSuperviewEdge: .left)
        usernameTextField.autoPinEdge(toSuperviewEdge: .top)
        usernameTextField.autoPinEdge(toSuperviewEdge: .right)
        
        passwordTextField.autoPinEdge(.top, to: .bottom, of: usernameTextField, withOffset: 8.0)
        passwordTextField.autoPinEdge(toSuperviewEdge: .left)
        passwordTextField.autoPinEdge(toSuperviewEdge: .right)
        
        reenterTextField.autoPinEdge(.top, to: .bottom, of: passwordTextField, withOffset: 8.0)
        reenterTextField.autoPinEdge(toSuperviewEdge: .left)
        reenterTextField.autoPinEdge(toSuperviewEdge: .right)
        reenterTextField.autoPinEdge(toSuperviewEdge: .bottom)
        
        signupButton.autoAlignAxis(.vertical, toSameAxisOf: containerView, withOffset: 0.0)
        signupButton.autoPinEdge(.top, to: .bottom, of: containerView, withOffset: 16.0)
    }
    
    private func bindViewModel() {
        let input = SignUpViewModel.Input(
                                          signupTrigger: signupButton.rx.tap.asDriver(),
                                          username: usernameTextField.rx.text.orEmpty,
                                          password: passwordTextField.rx.text.orEmpty,
                                          reenterPassword: reenterTextField.rx.text.orEmpty)
        let output = viewModel.transform(input: input)
        
        output.fetching.drive(onNext: { [unowned self] (show) in
            self.showLoading(withStatus: show)
        }).disposed(by: disposeBag)
        output.error.drive(onNext: {[unowned self ] (error) in
            self.showError(error: error)
        }).disposed(by: disposeBag)
    }
}

extension SignUpViewController: SignUpDisplayLogic {
    func goToMain(user: User) {
        let vc = MainViewController.newInstance(user: user)
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
}
