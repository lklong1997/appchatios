//
//  MessageRemoteDataSource.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/12/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift
import FirebaseDatabase

protocol MessageRemoteDataSource {
    func loadMessage(request: LoadMessagesRequest) -> Observable<[Message]>
    
    func sendMessage(request: SendMessageRequest, messageID: String) -> Observable<Message>
    
    func loadNewMessage(request: LoadNewMessageRequest) -> Observable<Message>
    
    func sendMessageImage(request: SendMessageImageRequest) -> Observable<Message>
    
    func sendMessageFile(request: SendMessageFileRequest) -> Observable<Message>
    
    func downloadFile(request: DownloadFileRequest) -> Observable<FileDownloadWrapper>
    
    func sendMessageLocation(request: SendLocationRequest) -> Observable<Message>
}

class MessageRemoteDataSourceFactory {
    public static let sharedInstance = MessageRemoteDataSourceImplementation()      
}
