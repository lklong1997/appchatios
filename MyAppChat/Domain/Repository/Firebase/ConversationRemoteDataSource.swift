//
//  ConversationRemoteDataSource.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/7/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift

protocol ConversationRemoteDataSource {
    func loadConversation(request: LoadConversationRequest) -> Observable<[Conversation]>
    
    func contactClick(request: ContactClickRequest) -> Observable<ConversationItemNavigation>
    
}

class ConversationRemoteDataSourceFactory {
    public static let sharedInstance = ConversationRemoteDataSourceImplementation()
}
