//
//  UserRemoteDataSource.swift
//  MyAppChat
//
//  Created by CPU11814 on 8/29/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift

protocol UserRemoteDataSource {
    func getLoginUser() -> User 
    
    func loadContacts(request: LoadContactRequest) -> Observable<[User]>
    
    func login(request: LoginRequest) -> Observable<User>
    
    func signUp(request: SignUpRequest) -> Observable<User>
    
    func updateUserAvatar(request: UpdateUserAvatarRequest) -> Observable<Bool>
}

class UserRemoteDataSourceFactory {
    public static let sharedInstance: UserRemoteDataSource = UserRemoteDataSourceImplementation()
}
