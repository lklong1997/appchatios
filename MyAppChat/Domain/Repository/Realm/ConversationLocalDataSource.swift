//
//  ConversationLocalDataSource.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/7/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift

protocol ConversationLocalDataSource {
    func persistConversations(conversations: [Conversation]) -> Observable<[Conversation]>
    
    func contactClick(request: ContactClickRequest) -> Observable<ConversationItemNavigation>
}

class ConversationLocalDataSourceFactory {
    public static let sharedInstance = ConversationLocalDataSourceImplementation()
}
