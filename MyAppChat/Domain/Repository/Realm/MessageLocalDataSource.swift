//
//  MessageLocalDataSource.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/12/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift

protocol MessageLocalDataSource {    
    func changeImageFilePath(message: Message, imagePath: String) -> Observable<Message>
    
    func persistSentMessage(message: Message, conversationID: String, isSent: Bool) -> Observable<Message>
    
    func persistSendingMessage(request: PersistSendingMessageRequest, messageID: String) -> Observable<Message>
    
    func persistNewMessage(message: Message, request: LoadNewMessageRequest) -> Observable<Message>
    
    func persistMessages(messages: [Message], conversationID: String) -> Observable<[Message]>
    
    func loadMessages(request: LoadMessagesRequest) -> Observable<[Message]>
    
    func changeSendingStateOfMessage(message: Message, conversationID: String, isSent: Bool) -> Observable<Message>
    
    func getUnSentMessages() -> [Message]
}

class MessageLocalDataSourceFactory {
    public static let sharedInstance = MessageLocalDataSourceImplementation()
}
