//
//  UserLocalDataSource.swift
//  MyAppChat
//
//  Created by CPU11814 on 8/30/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift

protocol UserLocalDataSource {
    func saveUser(user: User) -> Observable<User>
    
    func loadUser(request: LoadUserFromLocalRequest) -> Observable<User>
    
    func logout(request: LogoutRequest) -> Observable<Bool>
    
    func persistContacts(contacts: [User]) -> Observable<[User]>
    
    func getLoginUser() -> User
}

class UserLocalDataSourceFactory {
    public static let sharedInstance : UserLocalDataSource = UserLocalDataSourceImplementation()
}
