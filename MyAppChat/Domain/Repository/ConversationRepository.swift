//
//  ConversationRepository.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/7/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift

protocol ConversationRepository {
    func loadConversation(request: LoadConversationRequest) -> Observable<[Conversation]>
    
    func contactClick(request: ContactClickRequest) -> Observable<ConversationItemNavigation>
    
}

class ConversationRepositoryFactory {
    public static let sharedInstance = ConversationRepositoryImplementation(remoteDataSource: ConversationRemoteDataSourceFactory.sharedInstance,
                                                                            localDataSource: ConversationLocalDataSourceFactory.sharedInstance)
}
