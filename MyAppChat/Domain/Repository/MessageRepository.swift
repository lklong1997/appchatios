//
//  MessageRepository.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/12/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift

protocol MessageRepository {
    func loadMessage(request: LoadMessagesRequest) -> Observable<[Message]>
    
    func sendMessage(request: SendMessageRequest) -> Observable<Message>
    
    func loadNewMessage(request: LoadNewMessageRequest) -> Observable<Message>
    
    func sendMessageImage(request: SendMessageImageRequest) -> Observable<Message>
    
    func sendMessageFile(request: SendMessageFileRequest) -> Observable<Message>
    
    func reSendMessage(request: ReSendMessageRequest) -> Observable<Message>
    
    func getUnSentMessages(request: GetUnSentMessagesRequest) -> Observable<[Message]>
    
    func downloadFile(request: DownloadFileRequest) -> Observable<FileDownloadWrapper>
    
    func sendMessageLocation(request: SendLocationRequest) -> Observable<Message>
}

class MessageRepositoryFactory {
    public static let sharedInstance = MessageRepositoryImplementation(remoteDataSource: MessageRemoteDataSourceFactory.sharedInstance,
                                                                       localDataSource: MessageLocalDataSourceFactory.sharedInstance)
}
