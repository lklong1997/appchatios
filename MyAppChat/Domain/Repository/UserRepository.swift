//
//  UserRepository.swift
//  MyAppChat
//
//  Created by CPU11814 on 8/29/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift

protocol UserRepository {
    func loadContacts(request: LoadContactRequest) -> Observable<[User]>
    
    func login(request: LoginRequest) -> Observable<User>
    
    func signUp(request: SignUpRequest) -> Observable<User>
    
    func updateUserAvatar(request: UpdateUserAvatarRequest) -> Observable<Bool>
    
    func loadUserFromLocal(request: LoadUserFromLocalRequest) -> Observable<User>
    
    func logout(request: LogoutRequest) -> Observable<Bool>
}

class UserRepositoryFactory {
    public static let sharedInstance : UserRepository = UserRepositoryImplementation(remoteSource: UserRemoteDataSourceFactory.sharedInstance, localSource: UserLocalDataSourceFactory.sharedInstance)
}
