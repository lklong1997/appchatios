//
//  LoadContactRequest.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/6/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

struct LoadContactRequest {
    let user: User
}
