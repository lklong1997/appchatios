//
//  ContactClickRequest.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/10/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

struct ContactClickRequest {
    let loginUser: User
    let contact: User
}
