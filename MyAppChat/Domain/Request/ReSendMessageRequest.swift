//
//  ReSendMessageRequest.swift
//  MyAppChat
//
//  Created by CPU11814 on 10/8/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift

struct ReSendMessageRequest {
    let sender: String
    let content: String
    let atTime: CLong
    let type: String
    let conversationID: String
    let userIDList: [String: String] // "userA" : userA-avatarURL
    let nameOfContent: String?
    let localImagePublishSubject: PublishSubject<Message>
}
