//
//  AttachChildAddedListenerRequest.swift
//  MyAppChat
//
//  Created by CPU11814 on 10/9/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

struct LoadNewMessageRequest {
    let conversationItem: ConversationItemNavigation
    let loginUser: User
}
