//
//  SendMessageFileRequest.swift
//  MyAppChat
//
//  Created by CPU11814 on 10/17/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift

struct SendMessageFileRequest {
    let sender: String
    let content: String
    let atTime: CLong
    let type: String
    let conversationID: String
    let userIDList: [String: String] // "userA" : userA-avatarURL
    let nameOfContent: String?
    let localFilePublishSubject: PublishSubject<Message>
}
