//
//  DownloadFileRequest.swift
//  MyAppChat
//
//  Created by CPU11814 on 11/2/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//
import Foundation

struct DownloadFileRequest {
    let url: String
}
