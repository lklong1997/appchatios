//
//  LoginRequest.swift
//  MyAppChat
//
//  Created by CPU11814 on 8/30/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import Foundation

struct LoginRequest {
    let username: String
    let password: String
}
