//
//  LoadMessageRequest.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/12/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift

struct LoadMessagesRequest {
    let conversationItem: ConversationItemNavigation
    let loginUser: User
    let unSentMessagesPublishSubject: PublishSubject<[Message]>
}
