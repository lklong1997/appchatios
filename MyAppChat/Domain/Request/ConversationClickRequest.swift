//
//  ConversationClickRequest.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/14/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

struct ConversationClickRequest {
    let conversationID: String
}
