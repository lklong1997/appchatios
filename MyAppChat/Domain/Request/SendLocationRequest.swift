//
//  SendLocationRequest.swift
//  MyAppChat
//
//  Created by CPU11814 on 11/5/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//
import UIKit
import MapKit
import RxSwift

struct SendLocationRequest {
    let sender: String
    let content: String
    let atTime: CLong
    let type: String
    let conversationID: String
    let userIDList: [String: String] // "userA" : userA-avatarURL
    let nameOfContent: String
    let locationSnapshotPublishSubject: PublishSubject<Message>
}
