//
//  UpdateUserAvatarRequest.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/18/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

struct UpdateUserAvatarRequest {
    let userAvatar: String
    let name: String
}
