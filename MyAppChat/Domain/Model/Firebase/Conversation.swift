//
//  File.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/6/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

public struct Conversation {
    let id: String?
    let conversationName: [String]!
    let newestTime: CLong?
    let type: String!
    let userIDlist: [String : String]! // Each user will have his/her avatar url associated with
    let latestMessage: String?
    let latestMessageType: String?
}
