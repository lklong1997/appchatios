//
//  Message.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/6/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

public struct Message {
    let id: String!
    let atTime: CLong!
    let type: String!
    let userContent: String!
    let userName: String!
    let nameOfContent: String? // when sending a file, this will the the file's name
    
    func compareWith(_ message: Message) -> Bool {        
        return self.atTime >= message.atTime
    }
}
