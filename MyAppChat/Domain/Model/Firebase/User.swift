//
//  User.swift
//  MyAppChat
//
//  Created by CPU11814 on 8/29/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//


public struct User {
    let name: String
    let password: String
    let avatarUrl: String?
    let conversationIDList: [String : String]?
}
