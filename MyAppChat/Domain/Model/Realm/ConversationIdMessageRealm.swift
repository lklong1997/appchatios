//
//  ConversationIdMessageRealm.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/27/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RealmSwift

class ConversationIdMessageRealm: Object {
    @objc dynamic var id: String = ""
    let messages = List<MessageRealm>()
    
    override public static func primaryKey() -> String {
        return "id"
    }
    
    static func from(conversationID: String, messages: [Message], isSent: Bool) -> ConversationIdMessageRealm {
        let conversationIdMessageRealm = ConversationIdMessageRealm()
        
        conversationIdMessageRealm.id = conversationID
        conversationIdMessageRealm.messages.append(objectsIn: messages.map { message in
            let messageRealm = MessageRealm.from(message: message)
            messageRealm.isSent = isSent
            return messageRealm
        })
        
        return conversationIdMessageRealm
    }
    
    static func fromOneMessage(conversationID: String, message: Message) -> ConversationIdMessageRealm {
        let conversationIdMessageRealm = ConversationIdMessageRealm()
        
        conversationIdMessageRealm.id = conversationID
        conversationIdMessageRealm.messages.append(MessageRealm.from(message: message))
        
        return conversationIdMessageRealm
    }
}
