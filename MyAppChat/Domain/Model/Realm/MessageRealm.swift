//
//  MessageRealm.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/24/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RealmSwift

class MessageRealm: Object {
    @objc dynamic var id: String = ""
    @objc dynamic var type: String = ""
    @objc dynamic var userContent: String = ""
    @objc dynamic var userName: String = ""
    @objc dynamic var isSent: Bool = false
    @objc dynamic var nameOfContent: String?
    let atTime = RealmOptional<CLong>()
    
    override public static func primaryKey() -> String {
        return "id"
    }
    
    static func from(message: Message) -> MessageRealm {
        let messageRealm = MessageRealm()
        
        messageRealm.id = message.id
        messageRealm.type = message.type
        messageRealm.userContent = message.userContent
        messageRealm.userName = message.userName
        messageRealm.atTime.value = message.atTime
        messageRealm.nameOfContent = message.nameOfContent
        
        return messageRealm
    }
    
    public func convert() -> Message {
        let id = self.id
        let type = self.type
        let userContent = self.userContent
        let userName = self.userName
        let atTime = self.atTime.value!
        let nameOfContent = self.nameOfContent
     
        return Message(id: id, atTime: atTime, type: type, userContent: userContent, userName: userName, nameOfContent: nameOfContent)
    }
}
