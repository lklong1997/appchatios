//
//  MessageImageItem.swift
//  MyAppChat
//
//  Created by CPU11814 on 10/1/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

enum MessageImageItem: Hashable {
    case login(LoginImageItem)
    case others(OthersImageItem)
    
    var hashValue: Int {
        switch self {
        case .login(let loginImageItem):
            return loginImageItem.hashValue
        case .others(let othersImageItem):
            return othersImageItem.hashValue
        }
    }
    
    static func == (lhs: MessageImageItem, rhs: MessageImageItem) -> Bool {
        switch (lhs, rhs) {
        case (.login(let lhsValue), .login(let rhsValue)):
            return lhsValue == rhsValue
        case (.others(let lhsValue), .others(let rhsValue)):
            return lhsValue == rhsValue
        default:
            return false
        }
    }
    
    func showTimeLabel() {
        switch self {
        case .login(let loginImageItem):
            loginImageItem.showTimeLabel()
        case .others(let othersImageItem):
            othersImageItem.showTimeLabel()
        }
    }
    
    func hideTimeLabel() {
        switch self {
        case .login(let loginImageItem):
            loginImageItem.hideTimeLabel()
        case .others(let othersImageItem):
            othersImageItem.hideTimeLabel()
        }
    }
    
    func showAvatar() {
        switch self {
        case .login(let loginImageItem):
            loginImageItem.showAvatar()
        case .others(let othersImageItem):
            othersImageItem.showAvatar()
        }
    }
    
    func hideAvatar() {
        switch self {
        case .login(let loginImageItem):
            loginImageItem.hideAvatar()
        case .others(let othersImageItem):
            othersImageItem.hideAvatar()
        }
    }
    
    func sender() -> String {
        switch self {
        case .login(let loginImageItem):
            return loginImageItem.userName
        case .others(let othersImageItem):
            return othersImageItem.userName
        }
    }
    
    func content() -> String {
        switch self {
        case .login(let loginImageItem):
            return loginImageItem.userContent
        case .others(let othersImageItem):
            return othersImageItem.userContent
        }
    }
    
    func id() -> String {
        switch self {
        case .login(let loginImageItem):
            return loginImageItem.id
        case .others(let othersImageItem):
            return othersImageItem.id
        }
    }
    
    func setIsSent() {
        switch self {
        case .login(let loginImageItem):
            loginImageItem.isSent = true
        default:
            return
        }
    }
    
    func changeNewContent(newContent: String) {
        
    }

}
