//
//  LoggedInUserRealm.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/25/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RealmSwift

class LoggedInUserRealm: Object {
    @objc dynamic var name: String = ""
    @objc dynamic var password: String = ""
    @objc dynamic var avatarUrl: String? = nil
    let conversationIdRealmList = List<ConversationIdRealm>()
    
    override public static func primaryKey() -> String? {
        return "name"
    }
    
    static func from(user: User) -> LoggedInUserRealm {
        let userRealm = LoggedInUserRealm()
        userRealm.name = user.name
        userRealm.password = user.password
        userRealm.avatarUrl = user.avatarUrl
        if let conversationIDList = user.conversationIDList {
            for key in conversationIDList.keys {
                userRealm.conversationIdRealmList.append(ConversationIdRealm.from(key: key, conversationID: conversationIDList[key]!))
            }
        }
        
        return userRealm
    }
    
    func convert() -> User {
        var conversationIDList = [String: String]()
        for i in 0..<conversationIdRealmList.count {
            conversationIDList.updateValue(conversationIdRealmList[i].conversationID, forKey: conversationIdRealmList[i].key)
        }
        
        return User(name: self.name, password: self.password, avatarUrl: self.avatarUrl, conversationIDList: conversationIDList)
    }
}
