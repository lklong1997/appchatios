//
//  ConversationRealm.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/24/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RealmSwift

class ConversationRealm: Object {
    @objc dynamic var id: String = ""
    let conversationName = List<String>()
    let newestTime = RealmOptional<CLong>()
    @objc dynamic var type: String = ""
    let userIdAvatarRealmList = List<UserIdAvatarRealm>() // Each user will have his/her avatar url associated with
    @objc dynamic var latestMessage: String? = nil
    @objc dynamic var latestMessageType: String? = nil
    
    override public static func primaryKey() -> String {
        return "id"
    }
    
    static func from(conversation: Conversation) -> ConversationRealm {
        let conversationRealm = ConversationRealm()
        
        conversationRealm.id = conversation.id!
        for converName in conversation.conversationName {
            conversationRealm.conversationName.append(converName)
        }
    
        conversationRealm.type = conversation.type
        conversationRealm.latestMessage = conversation.latestMessage
        conversationRealm.latestMessageType = conversation.latestMessageType
        conversationRealm.newestTime.value = conversation.newestTime
        for userId in conversation.userIDlist.keys {
            conversationRealm.userIdAvatarRealmList.append(UserIdAvatarRealm.from(userID: userId, userAvatarUrl: conversation.userIDlist[userId]!))
        }
        
        return conversationRealm
    }
    
    func getUserIdAvatarList() -> [String: String] {
        var dictionary = [String: String]()
        
        self.userIdAvatarRealmList.forEach { userIdAvatarRealm in
            dictionary.updateValue(userIdAvatarRealm.userAvatarUrl, forKey: userIdAvatarRealm.userID)
        }
        
        return dictionary
    }
}
