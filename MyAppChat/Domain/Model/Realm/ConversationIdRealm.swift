//
//  ConversationIdRealm.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/25/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RealmSwift

class ConversationIdRealm: Object {
    @objc dynamic var key: String = ""
    @objc dynamic var conversationID: String = ""
    
    override public static func primaryKey() -> String {
        return "key"
    }
    
    static func from(key: String, conversationID: String) -> ConversationIdRealm {
        let conversationIdRealm = ConversationIdRealm()
        conversationIdRealm.key = key
        conversationIdRealm.conversationID = conversationID
    
        return conversationIdRealm
    }
}
