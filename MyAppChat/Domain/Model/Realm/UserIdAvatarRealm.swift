//
//  UserIdAvatarRealm.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/26/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RealmSwift

class UserIdAvatarRealm: Object {
    @objc dynamic var userID: String = ""
    @objc dynamic var userAvatarUrl: String = ""
    
    override public static func primaryKey() -> String {
        return "userID"
    }
    
    static func from(userID: String, userAvatarUrl: String) -> UserIdAvatarRealm {
        let userIdAvatarRealm = UserIdAvatarRealm()
        
        userIdAvatarRealm.userID = userID
        userIdAvatarRealm.userAvatarUrl = userAvatarUrl
        
        return userIdAvatarRealm
    }
    
    
}
