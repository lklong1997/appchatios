//
//  ConversationRepositoryImplementation.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/7/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift

class ConversationRepositoryImplementation: ConversationRepository {
    private let remoteDataSource: ConversationRemoteDataSource
    private let localDataSource: ConversationLocalDataSource
    
    init(remoteDataSource: ConversationRemoteDataSource, localDataSource: ConversationLocalDataSource) {
        self.remoteDataSource = remoteDataSource
        self.localDataSource = localDataSource
    }
    
    func loadConversation(request: LoadConversationRequest) -> Observable<[Conversation]> {
        return self.remoteDataSource
            .loadConversation(request: request)
            .flatMap { [unowned self] conversations -> Observable<[Conversation]> in
                return self.localDataSource
                    .persistConversations(conversations: conversations)
        }
    }
    
    func contactClick(request: ContactClickRequest) -> Observable<ConversationItemNavigation> {        
        return self.remoteDataSource
            .contactClick(request: request)
    }
    
    
    
}
