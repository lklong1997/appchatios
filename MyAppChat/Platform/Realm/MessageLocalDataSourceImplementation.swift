//
//  MessageLocalDataSourceImplementation.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/12/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift
import RealmSwift

class MessageLocalDataSourceImplementation: MessageLocalDataSource {
    private var unSentMessages = [Message]()
    private var messageIdsAfterFetching = [String]()
    
    func persistSendingMessage(request: PersistSendingMessageRequest, messageID: String) -> Observable<Message> {
        return Observable.deferred {
            let realm = try Realm()
            let message = Message(id: messageID, atTime: request.atTime, type: request.type, userContent: request.content, userName: request.sender, nameOfContent: request.nameOfContent)
            try realm.write {
                let conversationID = request.conversationID
                if let conversationIdMessageRealm = realm.object(ofType: ConversationIdMessageRealm.self, forPrimaryKey: conversationID) {
                    let messageRealm = MessageRealm.from(message: message)
                    messageRealm.isSent = false
                    conversationIdMessageRealm.messages.append(messageRealm)
                }
            }
            return Observable.just(message)
        }
    }
    
    func persistNewMessage(message : Message, request: LoadNewMessageRequest) -> Observable<Message> {
        return Observable.deferred {            
            let realm = try Realm()
            try realm.write {
                let conversationID = request.conversationItem.conversationID!
                if let conversationIdMessageRealm = realm.object(ofType: ConversationIdMessageRealm.self, forPrimaryKey: conversationID) {
                    // If there is no MessageRealm object with message id, create new one by append it to conversationIdMessageRealm's messages
                    if realm.object(ofType: MessageRealm.self, forPrimaryKey: message.id) == nil {
                        let messageRealm = MessageRealm.from(message: message)
                        
                        if message.userName == request.loginUser.name {
                            // If this message is sent by login user, there's a chance that this is "false" alarm by Firebase
                            // Which means this message may not on the Firebase database yet
                            
                            //                            messageRealm.isSent = false
                            //                          conversationIdMessageRealm.messages.append(messageRealm)
                        } else {
                            // If we receive this message by Firebase child added listener, this messsage is already on Firebase
                            // Because it is sent by other users
                            messageRealm.isSent = true
                            conversationIdMessageRealm.messages.append(messageRealm)
                        }
                    }
                }
                else {                    
                    realm.add(ConversationIdMessageRealm.fromOneMessage(conversationID: conversationID, message: message))
                }
            }
            
            return Observable.just(message)
        }
    }
    
    // This may be called several times, so we need to check if unSent is alreay exist or not
    // When fetching, we have merged messages from firebase with local, so all messages that are going to be saved in local will have isSent = true
    func persistMessages(messages: [Message], conversationID: String) -> Observable<[Message]> {
        return Observable.deferred {
            let realm = try Realm()
            try realm.write {
                if realm.object(ofType: ConversationIdMessageRealm.self, forPrimaryKey: conversationID) != nil {
                    let conversationIdMessageRealm = ConversationIdMessageRealm.from(conversationID: conversationID, messages: messages, isSent: true)
                    
                    for unSent in self.unSentMessages {
                        if !messages.contains(where: { message in
                            return message.id.elementsEqual(unSent.id)
                        }) {
                            conversationIdMessageRealm.messages.append(MessageRealm.from(message: unSent))
                        }
                    }
                    realm.add(conversationIdMessageRealm, update: true)
                }
                else { // If there is no ConversationIdMessageRealm with this conversationID, create new one with all messages
                    let conversationIdMessageRealm = ConversationIdMessageRealm.from(conversationID: conversationID, messages: messages, isSent: true)
                    conversationIdMessageRealm.messages.append(objectsIn: self.unSentMessages.map { MessageRealm.from(message: $0)})
                    realm.add(conversationIdMessageRealm, update: true)
                }
            }
            return Observable.just(messages)
        }
    }
    
    func loadMessages(request: LoadMessagesRequest) -> Observable<[Message]> {
        return Observable.deferred {
            let realm = try Realm()
            
            var messages = [Message]()
            self.unSentMessages.removeAll()
            
            if let conversationIdMessageRealm = realm.object(ofType: ConversationIdMessageRealm.self, forPrimaryKey: request.conversationItem.conversationID) {
                for messageRealm in conversationIdMessageRealm.messages {
                    if messageRealm.isSent {
                        self.messageIdsAfterFetching.append(messageRealm.id)
                        messages.append(messageRealm.convert())
                    } else {
                        self.unSentMessages.append(messageRealm.convert())
                    }
                }
                
                // Sort unsent messages with descending order(latest unsent message at the beginning)
                self.unSentMessages.sort(by: {
                    return $0.compareWith($1)
                })
                return Observable.just(messages)
            }
            // If there is no messages of this conversation in Realm, return empty array (as we are loading from both local and remote)
            return Observable.just([])
        }
    }
    
    func changeSendingStateOfMessage(message: Message, conversationID: String, isSent: Bool) -> Observable<Message> {
        return Observable.deferred {
            let realm = try Realm()
            try realm.write {
                let messageRealm = realm.object(ofType: MessageRealm.self, forPrimaryKey: message.id)
                messageRealm?.isSent = isSent
                realm.add(messageRealm!, update: true)
            }
            
            return Observable.just(message)
        }
    }
    
    // For image type message
    func persistSentMessage(message: Message, conversationID: String, isSent: Bool) -> Observable<Message> {
        return Observable.deferred {
            let realm = try Realm()
            try realm.write {
                let conversationID = conversationID
                if let conversationIdMessageRealm = realm.object(ofType: ConversationIdMessageRealm.self, forPrimaryKey: conversationID) {
                    let messageRealm = MessageRealm.from(message: message)
                    messageRealm.isSent = true
                    conversationIdMessageRealm.messages.append(messageRealm)
                }
            }
            
            return Observable.just(message)
        }
    }
    
    func changeImageFilePath(message: Message, imagePath: String) -> Observable<Message> {
        return Observable.deferred {
            do {
                let fileManager = FileManager.default
                let documentPath = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first
                
                let currentPath = documentPath?.appendingPathComponent(URL(string: imagePath)!.lastPathComponent)
                
                let destinationPath = documentPath?.appendingPathComponent(message.id + ".jpg")
                
                try fileManager.moveItem(at: currentPath!, to: destinationPath!)                
            } catch let error {
                print("Error while renaming file: \(error)")
                return Observable.error(ValidateError(message: error.localizedDescription))
            }
            
            return Observable.just(message)
        }
    }
    
    func getUnSentMessages() -> [Message] {
        return self.unSentMessages
    }
}
