//
//  UserLocalDataSourceImplementation.swift
//  MyAppChat
//
//  Created by CPU11814 on 8/30/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift
import RealmSwift

class UserLocalDataSourceImplementation: UserLocalDataSource {
    private var loginUser : User?
    
    func getLoginUser() -> User {
        return self.loginUser!
    }
    
    func saveUser(user: User) -> Observable<User> {
        return Observable.deferred {
            let realm = try Realm()
            do {
                try realm.write {
                    realm.add(LoggedInUserRealm.from(user: user), update: true)
                }
                self.loginUser = user
            } catch let error as NSError {
                print("Error while save login user to local: \(error)")                
            }
            return Observable.just(user)
        }
    }
    
    func loadUser(request: LoadUserFromLocalRequest) -> Observable<User> {
        return Observable.deferred { [unowned self] in
            let realm = try Realm()
            if let loggedInUser = realm.objects(LoggedInUserRealm.self).first {
                let user = loggedInUser.convert()
                self.loginUser = user
                return Observable.just(user)
            }
            return Observable.error(ValidateError(message: "No local user."))
        }
    }
    
    func logout(request: LogoutRequest) -> Observable<Bool> {
        return Observable.deferred {
            let realm = try Realm()
            do {
                try realm.write {
                    realm.deleteAll()
                }
            } catch let error as NSError {
                print("Error when log out: \(error)")
                return Observable.just(false)
            }
            return Observable.just(true)
        }
    }
    
    func persistContacts(contacts: [User]) -> Observable<[User]> {
        return Observable.deferred {
            let realm = try Realm()
            do {
                try realm.write {
                    realm.add(contacts.map { UserRealm.from(user: $0)}, update: true)
                }
            } catch let error as NSError {
                print("Error while persisting contacts to local: \(error)")
                return Observable.error(ValidateError(message: "Error while persisting contacts to local."))
            }
            return Observable.just(contacts)
            
        }
    }
}
