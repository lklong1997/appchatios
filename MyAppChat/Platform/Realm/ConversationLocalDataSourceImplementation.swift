//
//  ConversationLocalDataSourceImplementation.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/7/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift
import RealmSwift

class ConversationLocalDataSourceImplementation: ConversationLocalDataSource {
    private var userRepository: UserLocalDataSource = UserLocalDataSourceFactory.sharedInstance
    
    func persistConversations(conversations: [Conversation]) -> Observable<[Conversation]> {
        return Observable.deferred {
            do {
                let realm = try Realm()
                try realm.write {
                    realm.add(conversations.map { ConversationRealm.from(conversation: $0)}, update: true)
                }
            }
            catch let error as NSError {
                print("Error while persisting conversations to local: \(error)")
                return Observable.error(ValidateError(message: "Error while persisting conversations to local."))
            }
            return Observable.just(conversations)
        }
    }
    
    func contactClick(request: ContactClickRequest) -> Observable<ConversationItemNavigation> {
        return Observable.deferred { [unowned self] in
            let realm = try Realm()
            let loginUser = self.userRepository.getLoginUser()
            var isEqualToLoginUser = false
            var isEqualToContact = false
            
            let loginUserRealm = realm.object(ofType: LoggedInUserRealm.self, forPrimaryKey: loginUser.name)
            if let conversationIdRealmList = loginUserRealm?.conversationIdRealmList {
                for conversationIdRealm in conversationIdRealmList {
                    let conversationRealm = realm.object(ofType: ConversationRealm.self, forPrimaryKey: conversationIdRealm.conversationID)!
                    let userIdAvatarRealmList = conversationRealm.userIdAvatarRealmList
                    
                    for userIdAvatarRealm in userIdAvatarRealmList {
                        if userIdAvatarRealm.userID.elementsEqual(request.loginUser.name) {
                            isEqualToLoginUser = true
                        } else if userIdAvatarRealm.userID.elementsEqual(request.contact.name) {
                            isEqualToContact = true
                        }
                    }
                    
                    if isEqualToContact && isEqualToLoginUser {
                        return Observable.just(ConversationItemNavigation(conversationID: conversationIdRealm.conversationID,
                                                          userIDList: conversationRealm.getUserIdAvatarList()))
                    }
                }
            }
            return Observable.error(ValidateError(message: "No conversation between login user and this contact."))
        }
    }
}
