//
//  MessageRemoteDataSourceImplementation.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/12/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift
import Firebase
import FirebaseDatabase

class MessageRemoteDataSourceImplementation: MessageRemoteDataSource {
    private var databaseRef: DatabaseReference!
    private var storageRef: StorageReference!
    
    private let messageNode = FirebaseInjection.message
    private let conversationNode = FirebaseInjection.conversation
    
    private let textType = FirebaseInjection.textType
    private let imageType = FirebaseInjection.imageType
    private let audioType = FirebaseInjection.audioType
    private let videoType = FirebaseInjection.videoType
    
    private let storageImage = FirebaseInjection.storageImage
    private let storageFile = FirebaseInjection.storageFile
    private let storageAudio = FirebaseInjection.storageAudio
    private let storageVideo = FirebaseInjection.storageVideo
    private let storageLocation = FirebaseInjection.storageLocation
    
    private let sender = FirebaseInjection.sender
    private let content = FirebaseInjection.content
    private let atTime = FirebaseInjection.atTime
    private let typeNode = FirebaseInjection.type
    private let nameOfContent = FirebaseInjection.nameOfContent
    
    private let latestMessageNode = FirebaseInjection.latestMessage
    private let latestMessageTypeNode = FirebaseInjection.latestMessageType
    
    private let userRepository : UserRemoteDataSource = UserRemoteDataSourceFactory.sharedInstance
    private var newestMessageKeyAfterFetching: String!
    private var messageIdsAfterFetching = [String]()
    private var childAddedObserverHandle: UInt = 0
    
    // Fetch messages when start up
    func loadMessage(request: LoadMessagesRequest) -> Observable<[Message]> {
        self.databaseRef = FirebaseInjection.databaseRef.child(messageNode).child(request.conversationItem.conversationID)
        
        return Observable.create { [unowned self] observer -> Disposable in
            self.databaseRef.observeSingleEvent(of: DataEventType.value, with: { [unowned self] (snapshot) in
                var items = [Message]()
                if snapshot.exists() {
                    let dateFormater = DateFormatter()
                    dateFormater.dateFormat = "HH:mm"
                    
                    for child in snapshot.children {
                        let childSnapshot = child as! DataSnapshot
                        let key = childSnapshot.key
                        let value = childSnapshot.value as! [String: AnyObject]
                        self.newestMessageKeyAfterFetching = key
                        self.messageIdsAfterFetching.append(key)
                        
                        let type = value[self.typeNode] as! String
                        let sender = value[self.sender] as! String
                        let content = value[self.content] as! String
                        let atTime = value[self.atTime] as! CLong
                        let nameOfContent = value[self.nameOfContent] as? String
                        
                        let message = Message(id: key, atTime: atTime, type: type, userContent: content, userName: sender, nameOfContent: nameOfContent)
                        items.append(message)
                    }
                    observer.onNext(items)                    
                } else {  
                    observer.onNext(items)
                }
            })
            return Disposables.create()
        }
    }
    
    // This may be called several times due to our implementation when fetching messages in messageRepository, so we have to use a variable to store the handle of child added listener.
    // Only listen for new message after fetching
    func loadNewMessage(request: LoadNewMessageRequest) -> Observable<Message> {
        self.databaseRef = FirebaseInjection.databaseRef.child(messageNode).child(request.conversationItem.conversationID)
        // Remove any observer that is attached before, so that the trigger will be called one time.
        self.databaseRef.removeObserver(withHandle: self.childAddedObserverHandle)
        var query = databaseRef.queryOrderedByKey()
        
        // This will be nill if this conversation doesn't have any message
        if newestMessageKeyAfterFetching != nil {
            query = databaseRef.queryOrderedByKey().queryStarting(atValue: newestMessageKeyAfterFetching)
        }
        
        return Observable.create { [unowned self] observer -> Disposable in
            self.childAddedObserverHandle = query.observe(DataEventType.childAdded, with: { (snapshot) in
                if snapshot.exists() {
                    let key = snapshot.key
                    if self.messageIdsAfterFetching.contains(key) {
                        return
                    }
                    let value = snapshot.value as! [String: AnyObject]
                    
                    let type = value[self.typeNode] as! String
                    let sender = value[self.sender] as! String
                    let content = value[self.content] as! String
                    let atTime = value[self.atTime] as! CLong
                    let nameOfContent = value[self.nameOfContent] as? String
                    
                    let message = Message(id: key, atTime: atTime, type: type, userContent: content, userName: sender, nameOfContent: nameOfContent)
                    observer.onNext(message)
                }
            })
            return Disposables.create {
                self.databaseRef = FirebaseInjection.databaseRef.child(self.messageNode).child(request.conversationItem.conversationID)
                self.databaseRef.removeObserver(withHandle: self.childAddedObserverHandle)
            }
        }
    }
    
    func sendMessage(request: SendMessageRequest, messageID: String) -> Observable<Message> {
        self.databaseRef = FirebaseInjection.databaseRef
        
        return Observable.create { [unowned self] observer -> Disposable in
            let value = [self.sender: request.sender,
                         self.content: request.content,
                         self.atTime: request.atTime,
                         self.typeNode: request.type] as [String : Any]
            
            var updates = ["/\(self.messageNode)/\(request.conversationID)/\(messageID)": value] as [String: Any]
            
            // Iterates through each user id
            for key in request.userIDList.keys {
                // Update the Conversation/UserID/ConversationID/ node for each user id
                updates.updateValue(request.atTime, forKey: "/\(self.conversationNode)/\(key)/\(request.conversationID)/\(self.atTime)")
                updates.updateValue(request.content, forKey: "/\(self.conversationNode)/\(key)/\(request.conversationID)/\(self.latestMessageNode)")
                updates.updateValue(request.type, forKey: "/\(self.conversationNode)/\(key)/\(request.conversationID)/\(self.latestMessageTypeNode)")
                updates.updateValue(request.sender, forKey: "/\(self.conversationNode)/\(key)/\(request.conversationID)/\(self.sender)")
            }
            
            self.databaseRef.updateChildValues(updates, withCompletionBlock: { (error, ref) in
                if let error = error {		    
                    observer.onError(error)
                }
                else {
                    let message = Message(id: messageID, atTime: request.atTime, type: request.type, userContent: request.content, userName: request.sender, nameOfContent: request.nameOfContent)
                    observer.onNext(message)
                }
            })
            return Disposables.create()
        }
    }
    
    func sendMessageImage(request: SendMessageImageRequest) -> Observable<Message> {
        guard !request.content.isEmpty else {
            return Observable.error(ValidateError(message: "Empty image path."))
        }
        
        self.storageRef = FirebaseInjection.storageRef
        let key = self.databaseRef.child(self.messageNode).child(request.conversationID).childByAutoId().key
        let message = Message(id: key, atTime: request.atTime, type: request.type, userContent: request.content, userName: request.sender, nameOfContent: request.nameOfContent)
        // Push a message with local image path to viewModel to display 
        request.localImagePublishSubject.onNext(message)
        
        return Observable.create { [unowned self] observer -> Disposable in
            let imageUrl = URL(string: request.content)
            
            let filePath = "/\(request.conversationID)/\(request.sender)/\(self.storageImage)/\(imageUrl!.lastPathComponent)"
            let metaData = StorageMetadata()
            metaData.contentType = "image/jpg"
            
            self.storageRef.child(filePath).putFile(from: imageUrl!, metadata: metaData, completion: { [unowned self] (metaDat, error) in
                self.storageRef.child(filePath).downloadURL(completion: { [unowned self] (url, error) in
                    if let error = error {
                        print("Error when uploading image to firebase \(error)")
                        observer.onError(ValidateError(message: "Error when uploading image to firebase storage. \(error)"))
                    }
                    else {
                        print("downloadURL: \(url!.absoluteString)")
                        let value = [self.sender: request.sender,
                                     self.content: url!.absoluteString,
                                     self.atTime: request.atTime,
                                     self.typeNode: request.type] as [String : Any]
                        
                        var updates = ["/\(self.messageNode)/\(request.conversationID)/\(key)": value] as [String: Any]
                        
                        for key in request.userIDList.keys {
                            updates.updateValue(request.atTime, forKey: "/\(self.conversationNode)/\(key)/\(request.conversationID)/\(self.atTime)")
                            updates.updateValue(url!.absoluteString, forKey: "/\(self.conversationNode)/\(key)/\(request.conversationID)/\(self.latestMessageNode)")
                            updates.updateValue(request.type, forKey: "/\(self.conversationNode)/\(key)/\(request.conversationID)/\(self.latestMessageTypeNode)")
                            updates.updateValue(request.sender, forKey: "/\(self.conversationNode)/\(key)/\(request.conversationID)/\(self.sender)")
                        }
                        
                        self.databaseRef = FirebaseInjection.databaseRef
                        self.databaseRef.updateChildValues(updates, withCompletionBlock: { (error, ref) in
                            if let error = error {
                                observer.onError(error)
                            } else {
                                let message = Message(id: key, atTime: request.atTime, type: request.type, userContent: url!.absoluteString, userName: request.sender, nameOfContent: request.nameOfContent)
                                observer.onNext(message)
                            }
                        })
                        
                    }
                })                
            })
            
            
            return Disposables.create()
        }
    }
    
    func sendMessageFile(request: SendMessageFileRequest) -> Observable<Message> {
        guard !request.content.isEmpty else {
            return Observable.error(ValidateError(message: "Empty file path."))
        }
        
        self.storageRef = FirebaseInjection.storageRef
        let key = self.databaseRef.child(self.messageNode).child(request.conversationID).childByAutoId().key
        
        let filePath = request.content
        let nameOfFile = request.nameOfContent!
        
        let message = Message(id: key, atTime: request.atTime, type: request.type, userContent: filePath, userName: request.sender, nameOfContent: nameOfFile)
        // Push a message with local file path to viewModel to display
        request.localFilePublishSubject.onNext(message)
        
        return Observable.create { [unowned self] observer -> Disposable in
            let fileUrl = URL(string: request.content)
            
            let firebaseFilePath = "/\(request.conversationID)/\(request.sender)/\(self.storageFile)/\(fileUrl!.lastPathComponent)"
            
            self.storageRef.child(firebaseFilePath).putFile(from: fileUrl!, metadata: nil, completion: { [unowned self] (metaDat, error) in
                self.storageRef.child(firebaseFilePath).downloadURL(completion: { [unowned self] (url, error) in
                    if let error = error {
                        print("Error when uploading file to firebase \(error)")
                        observer.onError(ValidateError(message: "Error when uploading file to firebase storage. \(error)"))
                    }
                    else {
                        print("downloadURL: \(url!.absoluteString)")
                        let downloadUrl = url!.absoluteString
                        
                        let value = [self.sender: request.sender,
                                     self.content: downloadUrl,
                                     self.atTime: request.atTime,
                                     self.typeNode: request.type,
                                     self.nameOfContent: nameOfFile] as [String : Any]
                        
                        var updates = ["/\(self.messageNode)/\(request.conversationID)/\(key)": value] as [String: Any]
                        
                        for key in request.userIDList.keys {
                            updates.updateValue(request.atTime, forKey: "/\(self.conversationNode)/\(key)/\(request.conversationID)/\(self.atTime)")
                            updates.updateValue(nameOfFile, forKey: "/\(self.conversationNode)/\(key)/\(request.conversationID)/\(self.latestMessageNode)")
                            updates.updateValue(request.type, forKey: "/\(self.conversationNode)/\(key)/\(request.conversationID)/\(self.latestMessageTypeNode)")
                            updates.updateValue(request.sender, forKey: "/\(self.conversationNode)/\(key)/\(request.conversationID)/\(self.sender)")
                        }
                        
                        self.databaseRef = FirebaseInjection.databaseRef
                        self.databaseRef.updateChildValues(updates, withCompletionBlock: { (error, ref) in
                            if let error = error {
                                observer.onError(error)
                            } else {
                                let message = Message(id: key, atTime: request.atTime, type: request.type, userContent: downloadUrl, userName: request.sender, nameOfContent: nameOfFile)
                                observer.onNext(message)
                            }
                        })
                        
                    }
                })
            })
            return Disposables.create()
        }
    }
    
    func downloadFile(request: DownloadFileRequest) -> Observable<FileDownloadWrapper> {
        return Observable.create({ observer -> Disposable in
            let httpReference = Storage.storage().reference(forURL: request.url)            
            
            let fileManager = FileManager.default
            let documentPath = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first
            
            if let path = documentPath?.appendingPathComponent(URL(string: request.url)!.lastPathComponent) {
                let downloadTask = httpReference.write(toFile: path)
                let fileDownloadWrapper = FileDownloadWrapper(downloadTask: downloadTask, filePath: path)
                observer.onNext(fileDownloadWrapper)
            }
            
            return Disposables.create()
        })
    }
    
    func sendMessageLocation(request: SendLocationRequest) -> Observable<Message> {
        guard !request.content.isEmpty else {
            return Observable.error(ValidateError(message: "Empty snapshot path."))
        }
        
        self.storageRef = FirebaseInjection.storageRef
        let key = self.databaseRef.child(self.messageNode).child(request.conversationID).childByAutoId().key
        let message = Message(id: key, atTime: request.atTime, type: request.type, userContent: request.content, userName: request.sender, nameOfContent: request.nameOfContent)
        // Push a message with local location snapshot to viewModel to display
        request.locationSnapshotPublishSubject.onNext(message)
        
        return Observable.create { [unowned self] observer -> Disposable in
            let snapshotUrl = URL(string: request.content)
            
            let filePath = "/\(request.conversationID)/\(request.sender)/\(self.storageLocation)/\(snapshotUrl!.lastPathComponent)"
            let metaData = StorageMetadata()
            metaData.contentType = "image/jpg"
            
            self.storageRef.child(filePath).putFile(from: snapshotUrl!, metadata: metaData, completion: { [unowned self] (metaDat, error) in
                self.storageRef.child(filePath).downloadURL(completion: { [unowned self] (url, error) in
                    if let error = error {
                        print("Error when uploading snapshot to firebase \(error)")
                        observer.onError(ValidateError(message: "Error when uploading snapshot to firebase storage. \(error)"))
                    }
                    else {
                        print("downloadURL: \(url!.absoluteString)")
                        let value = [self.sender: request.sender,
                                     self.content: url!.absoluteString,
                                     self.atTime: request.atTime,
                                     self.typeNode: request.type,
                                     self.nameOfContent: request.nameOfContent] as [String : Any]
                        
                        var updates = ["/\(self.messageNode)/\(request.conversationID)/\(key)": value] as [String: Any]
                        
                        for key in request.userIDList.keys {
                            updates.updateValue(request.atTime, forKey: "/\(self.conversationNode)/\(key)/\(request.conversationID)/\(self.atTime)")
                            updates.updateValue(url!.absoluteString, forKey: "/\(self.conversationNode)/\(key)/\(request.conversationID)/\(self.latestMessageNode)")
                            updates.updateValue(request.type, forKey: "/\(self.conversationNode)/\(key)/\(request.conversationID)/\(self.latestMessageTypeNode)")
                            updates.updateValue(request.sender, forKey: "/\(self.conversationNode)/\(key)/\(request.conversationID)/\(self.sender)")
                        }
                        
                        self.databaseRef = FirebaseInjection.databaseRef
                        self.databaseRef.updateChildValues(updates, withCompletionBlock: { (error, ref) in
                            if let error = error {
                                observer.onError(error)
                            } else {
                                let message = Message(id: key, atTime: request.atTime, type: request.type, userContent: url!.absoluteString, userName: request.sender, nameOfContent: request.nameOfContent)
                                observer.onNext(message)
                            }
                        })
                        
                    }
                })
            })
                        
            return Disposables.create()
        }
    }
}
