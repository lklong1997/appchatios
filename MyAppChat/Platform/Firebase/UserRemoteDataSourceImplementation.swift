//
//  FirebaseDatabase.swift
//  MyAppChat
//
//  Created by CPU11814 on 8/29/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift
import Firebase
import FirebaseDatabase

class UserRemoteDataSourceImplementation: UserRemoteDataSource {
    private var databaseRef: DatabaseReference!
    private let userNameNode = FirebaseInjection.name
    private let userPasswordNode = FirebaseInjection.password
    private let userAvatarURLNode = FirebaseInjection.avatarUrl
    private let userConversationIDListNode = FirebaseInjection.conversationIDList
    private let userNode = FirebaseInjection.user
    private let conversationNode = FirebaseInjection.conversation
    private let userIDListNode = FirebaseInjection.userIDList
    
    private var loginUser : User!
    
    func getLoginUser() -> User {
        return self.loginUser
    }
    
    func login(request: LoginRequest) -> Observable<User> {
        self.databaseRef = FirebaseInjection.databaseRef.child(userNode).child(request.username)
        
        return Observable.create { [unowned self] (observer) -> Disposable in
            let observerHandle = self.databaseRef.observe(DataEventType.value, with: { (snapshot) in
                guard snapshot.exists() else {
                    return observer.onError(ValidateError(message: "Username or password is wrong"))
                }
                let value = snapshot.value as! [String: AnyObject]
                
                let userName = value[self.userNameNode] as! String
                let userPassword = value[self.userPasswordNode] as! String
                let avatarUrl = value[self.userAvatarURLNode] as? String
                let conversationIDList = value[self.userConversationIDListNode] as? [String : String]
                
                
                if userName == request.username && userPassword == request.password {
                    observer.onNext(User(name: userName, password: userPassword, avatarUrl: avatarUrl, conversationIDList: conversationIDList))
                    observer.onCompleted()
                }
                observer.onError(ValidateError(message: "Password is incorrect"))
            })
            return Disposables.create {
                self.databaseRef.removeObserver(withHandle: observerHandle)
            }
        }
    }
    
    func signUp(request: SignUpRequest) -> Observable<User> {
        databaseRef = FirebaseInjection.databaseRef.child(userNode)
        
        let value = [userNameNode: request.username,
                     userPasswordNode: request.password]
        let key = request.username
        return Observable.create{ [unowned self] (observer) -> Disposable in
            self.databaseRef.child(request.username).observeSingleEvent(of: DataEventType.value, with: { (snapshot) in
                if snapshot.exists() {
                    observer.onError(ValidateError(message: "Username already exists."))
                }
                else {
                    self.databaseRef.child(key).setValue(value) {
                        (error: Error?, ref: DatabaseReference) in
                        if let error = error {
                            print("Data could not be saved \(error)")
                            observer.onError(error)
                        }
                        else {
                            observer.onNext(User(name: request.username, password: request.password, avatarUrl: "", conversationIDList: [:]))
                        }
                    }
                }
            })
            return Disposables.create()
        }
    }
    
    func loadContacts(request: LoadContactRequest) -> Observable<[User]> {
        databaseRef = FirebaseInjection.databaseRef.child(userNode)
        
        return Observable.create { [unowned self] (observer) -> Disposable in
            let observerHandle = self.databaseRef.observe(DataEventType.value, with: { (snapshot) in
                var users = [User]()
                for child in snapshot.children {
                    let childSnapshot = child as! DataSnapshot
                    let value = childSnapshot.value as! [String: AnyObject]
                    
                    let userName = value[self.userNameNode] as! String
                    if userName != request.user.name {
                        let userPassword = value[self.userPasswordNode] as! String
                        let userConversationIDList = value[self.userConversationIDListNode] as? [String : String]
                        let userAvatarUrl = value[self.userAvatarURLNode] as? String
                        
                        users.append(User(name: userName, password: userPassword, avatarUrl: userAvatarUrl, conversationIDList: userConversationIDList))
                    }
                    else {
                        self.loginUser = User(name: userName, password: "", avatarUrl: value[self.userAvatarURLNode] as? String, conversationIDList: value[self.userConversationIDListNode] as? [String : String])
                    }
                }
                
                observer.onNext(users)
            })
            return Disposables.create {
                self.databaseRef.removeObserver(withHandle: observerHandle)
            }
        }
    }
    
    func updateUserAvatar(request: UpdateUserAvatarRequest) -> Observable<Bool> {
        databaseRef = FirebaseInjection.databaseRef
        
        return Observable.create { [unowned self] (observer) -> Disposable in
            // Update user avatar at /User/user_id/userAvatar
            var updates = ["/\(self.userNode)/\(request.name)/\(self.userAvatarURLNode)": request.userAvatar]
            
            let observerHandle = self.databaseRef.child(self.conversationNode).child(request.name).observe(.value, with: ({ snapshot in
                if snapshot.exists() {
                    var converID_userID_pairDict = [String: [String]]()
                    
                    // Iterate through each conversation at the node /Conversation/login_user_id/conversation_id
                    for child in snapshot.children {
                        let childSnapshot = child as! DataSnapshot
                        
                        let conversationID = childSnapshot.key
                        let value = childSnapshot.value as! [String: AnyObject]
                        let loginUserPositionToUpdate = "/\(self.conversationNode)/\(request.name)/\(conversationID)/\(self.userIDListNode)/\(request.name)"
                        updates.updateValue(request.userAvatar, forKey: loginUserPositionToUpdate)
                        
                        let userIDList = value[self.userIDListNode] as! [String : String]
                        var needToUpdateUserIDList = [String]()
                        for userID in userIDList.keys {
                            if userID != request.name {
                                // If this userID is not our login user's id, add that userID to the list for later update
                                needToUpdateUserIDList.append(userID)
                            }
                        }
                        
                        converID_userID_pairDict.updateValue(needToUpdateUserIDList, forKey: conversationID)
                    }
                    
                    // We will update login user's avatar url for each of other user's conversation -- /Conversation/userID/conversation_id/userIDList/login_user_id
                    for conversationID in converID_userID_pairDict.keys {
                        for otherUsersID in converID_userID_pairDict[conversationID]! {
                            let positionToUpdate = "/\(self.conversationNode)/\(otherUsersID)/\(conversationID)/\(self.userIDListNode)/\(request.name)"
                            updates.updateValue(request.userAvatar, forKey: positionToUpdate)
                        }
                    }
                    
                    self.databaseRef.updateChildValues(updates, withCompletionBlock: { (error, ref) in
                        if let error = error {
                            observer.onError(error)
                        }
                        else {
                            observer.onNext(true)
                            observer.onCompleted()
                        }
                    })
                }
            }))
            
            return Disposables.create {
                self.databaseRef.child(self.conversationNode).child(request.name).removeObserver(withHandle: observerHandle)
            }
        }
    }
}
