//
//  ConversationRemoteDataSourceImplementation.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/7/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift
import Firebase
import FirebaseDatabase

class ConversationRemoteDataSourceImplementation: ConversationRemoteDataSource {
    private var databaseRef : DatabaseReference!
    
    private let userNode = FirebaseInjection.user
    private let conversationIDList = FirebaseInjection.conversationIDList
    
    private let conversationNode = FirebaseInjection.conversation
    private let atTimeNode = FirebaseInjection.atTime
    private let typeNode = FirebaseInjection.type
    private let userIDListNode = FirebaseInjection.userIDList
    private let conversationNameNode = FirebaseInjection.conversationName
    private let latestMessageNode = FirebaseInjection.latestMessage
    private let senderNode = FirebaseInjection.sender

    private let latestMessageTypeNode = FirebaseInjection.latestMessageType
    private let typePrivate = FirebaseInjection.privateType
    private let typeGroup = FirebaseInjection.groupType
    
    private let textType = FirebaseInjection.textType
    private let imageType = FirebaseInjection.imageType
    private let audioType = FirebaseInjection.audioType
    private let videoTYpe = FirebaseInjection.videoType
    
    private let userRepository : UserRemoteDataSource = UserRemoteDataSourceFactory.sharedInstance
    
    func loadConversation(request: LoadConversationRequest) -> Observable<[Conversation]> {
        self.databaseRef = FirebaseInjection.databaseRef.child(conversationNode).child(request.user.name)
        
        return Observable.create { [unowned self] observer -> Disposable in
            let observerHandle = self.databaseRef.observe(DataEventType.value, with: { (snapshot) in
                if snapshot.exists() {
                    var items = [Conversation]()
                    
                    // Iterate through each child of Conversation/userID/converID Node
                    for child in snapshot.children {
                        let childSnapShot = child as! DataSnapshot
                        
                        let key = childSnapShot.key
                        let value = childSnapShot.value as! [String: AnyObject]
                        
                        let type = value[self.typeNode] as! String
                        
                        let userIDList = value[self.userIDListNode] as! [String: String]
                        
                        let conversationNameList = value[self.conversationNameNode] as! [String]

                        
                        let latestMessage = value[self.latestMessageNode] as? String
                        let latestMessageTime = value[self.atTimeNode] as? CLong
                        let latestMessageType = value[self.latestMessageTypeNode] as? String
                        
                        //let sender = value[self.senderNode] as? String
                        
                        items.append(Conversation(id: key, conversationName: conversationNameList, newestTime: latestMessageTime, type: type, userIDlist: userIDList, latestMessage: latestMessage, latestMessageType: latestMessageType))
                    }
                    observer.onNext(items)
                }
            })
            return Disposables.create {
                self.databaseRef.child(self.conversationNode).removeObserver(withHandle: observerHandle)
            }
        }
    }
    
    func contactClick(request: ContactClickRequest) -> Observable<ConversationItemNavigation> {
        return Observable.create { [unowned self] (observer) -> Disposable in
            self.databaseRef = FirebaseInjection.databaseRef.child(self.conversationNode)
            
            var doesHaveConver = false
            
            let observerHandle = self.databaseRef.child(request.loginUser.name).observe(DataEventType.value, with: { (snapshot) in
                if snapshot.exists() {
                    for child in snapshot.children {
                        let childSnapShot = child as! DataSnapshot
                        let value = childSnapShot.value as! [String: AnyObject]
                        
                        // userIDList is sth like:  user_a: user_a_avatar
                        let userIDList = value[self.userIDListNode] as! [String : String]
                        if userIDList.keys.contains(request.loginUser.name) && userIDList.keys.contains(request.contact.name) {
                            // Now we gets the right conversation ID
                            doesHaveConver = true
                            
                            let id = childSnapShot.key
        
                            observer.onNext(ConversationItemNavigation(conversationID: id, userIDList: userIDList))
                            observer.onCompleted()
                        }
                        
                    }
                    if !doesHaveConver {
                        self.createNewConversation(observer: observer, request: request)
                    }
                }
                else {
                    self.createNewConversation(observer: observer, request: request)
                }
            })
            
            return Disposables.create {
                self.databaseRef.child(request.loginUser.name).removeObserver(withHandle: observerHandle)
            }
        }
        
    }
    
    private func createNewConversation(observer: AnyObserver<ConversationItemNavigation>, request: ContactClickRequest) {
        // Create new conversation between them
        self.databaseRef = FirebaseInjection.databaseRef
        let key = self.databaseRef.child(conversationNode).childByAutoId().key
        
        let loginUserKey = self.databaseRef.child(userNode).child(request.loginUser.name).childByAutoId().key
        let otherKey = self.databaseRef.child(userNode).child(request.contact.name).childByAutoId().key
        
        let conversationName = [request.loginUser.name, request.contact.name]
        let conversationType = self.typePrivate
        let userIDList = [request.loginUser.name: request.loginUser.avatarUrl ?? "",
                          request.contact.name: request.contact.avatarUrl ?? ""]
        
        
        let value = [self.conversationNameNode: conversationName,
                     self.atTimeNode: nil,
                     self.typeNode: conversationType,
                     self.userIDListNode: userIDList,
                     self.latestMessageNode: nil] as [String : Any?]
        let updates = ["/\(self.conversationNode)/\(request.loginUser.name)/\(key)": value,
                       "/\(self.conversationNode)/\(request.contact.name)/\(key)": value,
                       "/\(self.userNode)/\(request.loginUser.name)/\(self.conversationIDList)/\(loginUserKey)": key,
                       "/\(self.userNode)/\(request.contact.name)/\(self.conversationIDList)/\(otherKey)": key] as [String: Any]
        self.databaseRef.updateChildValues(updates, withCompletionBlock: { (error, ref) in
            if let error = error {
                print("Data could not be saved \(error)")
                observer.onError(error)
            }
            else {                            
                observer.onNext(ConversationItemNavigation(conversationID: key, userIDList: userIDList))
                observer.onCompleted()
            }
        })
    }
}
