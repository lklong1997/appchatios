//
//  MessageRepositoryImplementation.swift
//  MyAppChat
//
//  Created by CPU11814 on 9/12/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift
import FirebaseDatabase

class MessageRepositoryImplementation: MessageRepository {
    private var databaseRef: DatabaseReference!
    private let messageNode = FirebaseInjection.message
    
    private var remoteDataSource : MessageRemoteDataSource!
    private var localDataSource: MessageLocalDataSource!
    
    private let textType = FirebaseInjection.textType
    private let imageType = FirebaseInjection.imageType
    private let videoType = FirebaseInjection.videoType
    private let audioType = FirebaseInjection.audioType
    
    init(remoteDataSource: MessageRemoteDataSource, localDataSource: MessageLocalDataSource) {
        self.remoteDataSource = remoteDataSource
        self.localDataSource = localDataSource
    }
    
    func loadMessage(request: LoadMessagesRequest) -> Observable<[Message]> {
        let local = self.localDataSource.loadMessages(request: request)
        
        let remote = Observable
            .just([])
            .concat(self.remoteDataSource.loadMessage(request: request))
        
        let final = Observable
            .combineLatest(local, remote) { [unowned self]  in
                return self.mergeMessage($0, $1)
        }
        
        return final
            .flatMap { [unowned self ] messages in
                return Observable.deferred {
                    request.unSentMessagesPublishSubject.onNext(self.localDataSource.getUnSentMessages())
                    
                    return self.localDataSource
                        .persistMessages(messages: messages, conversationID: request.conversationItem.conversationID)
                }
        }
    }
    
    func sendMessage(request: SendMessageRequest) -> Observable<Message> {
        self.databaseRef = FirebaseInjection.databaseRef
        
        return Observable.deferred { [unowned self] in
            let key = self.databaseRef.child(self.messageNode).child(request.conversationID).childByAutoId().key
            
            return Observable.just(PersistSendingMessageRequest(sender: request.sender,
                                                                content: request.content,
                                                                atTime: request.atTime,
                                                                type: request.type,
                                                                conversationID: request.conversationID,
                                                                userIDList: request.userIDList,
                                                                nameOfContent: request.nameOfContent))
                .flatMap { [unowned self] persistRequest -> Observable<Message> in
                    return self.localDataSource
                        .persistSendingMessage(request: persistRequest, messageID: key)
                        .flatMap { [unowned self] message -> Observable<Message> in
                            return self.remoteDataSource
                                .sendMessage(request: request, messageID: key)
                                .flatMap { [unowned self] message -> Observable<Message> in
                                    return self.localDataSource
                                        .changeSendingStateOfMessage(message: message, conversationID: request.conversationID, isSent: true)
                            }
                    }
            }
        }
    }
    
    func sendMessageImage(request: SendMessageImageRequest) -> Observable<Message> {
        return self.remoteDataSource
            .sendMessageImage(request: request)
            // Receive a completion block here
            .flatMap { [unowned self] message -> Observable<Message> in
                return self.localDataSource
                    .persistSentMessage(message: message, conversationID: request.conversationID, isSent: true)
                    .flatMap { [unowned self] message -> Observable<Message> in
                        return self.localDataSource
                            .changeImageFilePath(message: message, imagePath: request.content)
                }
        }
    }
    
    func sendMessageFile(request: SendMessageFileRequest) -> Observable<Message> {
        return self.remoteDataSource
            .sendMessageFile(request: request)
            .flatMap { [unowned self] message -> Observable<Message> in
                return self.localDataSource
                    .persistSentMessage(message: message, conversationID: request.conversationID, isSent: true)                    
        }
    }
    // Attach a childAdded listener for this conversation
    // It may give "false" alarm when no connection but childAdded is still triggered when login user sends a new message
    func loadNewMessage(request: LoadNewMessageRequest) -> Observable<Message> {
        return self.remoteDataSource
            .loadNewMessage(request: request)
            .flatMap { [unowned self] message -> Observable<Message> in
                return self.localDataSource
                    .persistNewMessage(message: message, request: request)
        }
    }
    
    func reSendMessage(request: ReSendMessageRequest) -> Observable<Message> {
        if request.type == self.textType {
            let sendMessageRequest = SendMessageRequest(sender: request.sender,
                                                        content: request.content,
                                                        atTime: request.atTime,
                                                        type: request.type,
                                                        conversationID: request.conversationID,
                                                        userIDList: request.userIDList,
                                                        nameOfContent: request.nameOfContent)
            return self.sendMessage(request: sendMessageRequest)
        } else {
            let sendMessageImageRequset = SendMessageImageRequest(sender: request.sender,
                                                                  content: request.content,
                                                                  atTime: request.atTime,
                                                                  type: request.type,
                                                                  conversationID: request.conversationID,
                                                                  userIDList: request.userIDList,
                                                                  nameOfContent: request.nameOfContent,
                                                                  localImagePublishSubject: request.localImagePublishSubject)
            return self.sendMessageImage(request: sendMessageImageRequset)
        }
    }
    
    func getUnSentMessages(request: GetUnSentMessagesRequest) -> Observable<[Message]> {
        return Observable.just(self.localDataSource
            .getUnSentMessages())
    }
    
    func downloadFile(request: DownloadFileRequest) -> Observable<FileDownloadWrapper> {
        return self.remoteDataSource
            .downloadFile(request: request)
    }
    
    func sendMessageLocation(request: SendLocationRequest) -> Observable<Message> {
        return self.remoteDataSource
            .sendMessageLocation(request: request)
            // Receive completion block here
            .flatMap { [unowned self] message -> Observable<Message> in
                return self.localDataSource
                    .persistSentMessage(message: message, conversationID: request.conversationID, isSent: true)
                    .flatMap { [unowned self] message -> Observable<Message> in
                        return self.localDataSource
                            .changeImageFilePath(message: message, imagePath: request.content)
                    }
        }
    }
    
    private func mergeMessage(_ local: [Message], _ remote: [Message]) -> [Message]{
        var messages = local
        
        remote.forEach { remoteMessage in
            let index = messages.firstIndex(where: {
                return remoteMessage.id.elementsEqual($0.id)
            })
            if index != nil {
                messages[index!] = remoteMessage
            }
            else {
                messages.append(remoteMessage)
            }
        }
        
        // Sort the array into descending order (newest message at beginning)
        let finalMessages = messages.sorted(by: {
            return $0.compareWith($1)
        })
        return finalMessages
    }
}
