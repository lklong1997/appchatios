//
//  UserRepositoryImplementation.swift
//  MyAppChat
//
//  Created by CPU11814 on 8/29/18.
//  Copyright © 2018 LyKimLong. All rights reserved.
//

import RxSwift

class UserRepositoryImplementation: UserRepository {
    private let remoteSource : UserRemoteDataSource
    private let localSource: UserLocalDataSource
    
    init(remoteSource : UserRemoteDataSource, localSource: UserLocalDataSource) {
        self.remoteSource = remoteSource
        self.localSource = localSource
    }
    
    func login(request: LoginRequest) -> Observable<User> {
        return Observable.deferred { [unowned self] in
            return self.remoteSource
                .login(request: request)
                .flatMap { [unowned self] (user) -> Observable<User> in
                    return self.localSource
                        .saveUser(user: user)
            }
        }
    }
    
    func signUp(request: SignUpRequest) -> Observable<User> {
        return Observable.deferred{ [unowned self] in
            return self.remoteSource
                .signUp(request: request)
                .flatMap{ [unowned self] (user) -> Observable<User> in
                    return self.localSource
                        .saveUser(user: user)
                    
            }
        }
    }
    
    func loadContacts(request: LoadContactRequest) -> Observable<[User]> {
        return Observable.deferred { [unowned self] in
            return self.remoteSource
                .loadContacts(request: request)
                .flatMap { [unowned self] contacts -> Observable<[User]> in
                    return self.localSource
                        .persistContacts(contacts: contacts)
            }
        }
    }
    
    func updateUserAvatar(request: UpdateUserAvatarRequest) -> Observable<Bool> {
        return Observable.deferred { [unowned self] in
            return self.remoteSource
                .updateUserAvatar(request: request)
        }
    }
    
    func loadUserFromLocal(request: LoadUserFromLocalRequest) -> Observable<User> {
        return Observable.deferred { [unowned self] in
            return self.localSource
                .loadUser(request: request)
        }
    }
    
    func logout(request: LogoutRequest) -> Observable<Bool> {
        return Observable.deferred { [unowned self] in
            return self.localSource
                .logout(request: request)
        }
    }
}
